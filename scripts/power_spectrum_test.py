#!/usr/bin/env python
import sys
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy


def radial_profile(image, centre=None):
    if centre is None:
        centre = (image.shape[0] // 2, image.shape[1] // 2)
    x, y = numpy.indices((image.shape))
    r = numpy.sqrt((x - centre[0]) ** 2 + (y - centre[1]) ** 2)
    r = r.astype(numpy.int)
    return numpy.bincount(r.ravel(), image.ravel()) / numpy.bincount(r.ravel())


def main(file_name):
    fits_file = fits.open(file_name)
    # pylint: disable=no-member
    image = fits_file[0].data[0]
    cellsize = abs(fits_file[0].header["CDELT1"]) * numpy.pi / 180.0
    spectrum = numpy.abs(numpy.fft.fftshift(numpy.fft.fft2(image)))
    profile = radial_profile(spectrum**2)
    # Delta_u = 1 / (N * Delta_theta)
    cellsize_uv = 1.0 / (image.shape[0] * cellsize)
    lambda_max = cellsize_uv * len(profile)
    lambda_axis = numpy.linspace(cellsize_uv, lambda_max, len(profile))
    theta_axis = 180.0 / (numpy.pi * lambda_axis)
    plt.subplot(121)
    plt.imshow(spectrum)
    plt.gca().set_title("Amplitude(FFT(residual image))")
    plt.subplot(122)
    plt.plot(theta_axis, profile)
    plt.gca().set_title("Power spectrum of residual image")
    plt.gca().set_xlabel("Degrees")
    plt.gca().set_xscale("log")
    plt.gca().set_yscale("log")
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise RuntimeError(
            "Need path of input FITS image file on command line."
        )
    main(sys.argv[1])
