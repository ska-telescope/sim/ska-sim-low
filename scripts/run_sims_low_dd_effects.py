#!/usr/bin/env python3
"""
Run simulations for SKA1-LOW direction-dependent effects.
https://confluence.skatelescope.org/display/SE/Simulations+with+Direction-Dependent+Effects
https://jira.skatelescope.org/browse/SIM-489
"""

import copy
import os.path

from astropy.io import fits
import numpy
import oskar

from .utils import get_start_time


def bright_sources():
    """
    Returns a list of bright A-team sources.
    Does not include the Galactic Centre!
    """
    # For A: data from the Molonglo Southern 4 Jy sample (VizieR).
    # Others from GLEAM reference paper, Hurley-Walker et al. (2017), Table 2.
    return numpy.array(
        (
            [
                50.67375,
                -37.20833,
                528,
                0,
                0,
                0,
                178e6,
                -0.51,
                0,
                0,
                0,
                0,
            ],  # For
            [
                201.36667,
                -43.01917,
                1370,
                0,
                0,
                0,
                200e6,
                -0.50,
                0,
                0,
                0,
                0,
            ],  # Cen
            [
                139.52500,
                -12.09556,
                280,
                0,
                0,
                0,
                200e6,
                -0.96,
                0,
                0,
                0,
                0,
            ],  # Hyd
            [
                79.95833,
                -45.77889,
                390,
                0,
                0,
                0,
                200e6,
                -0.99,
                0,
                0,
                0,
                0,
            ],  # Pic
            [
                252.78333,
                4.99250,
                377,
                0,
                0,
                0,
                200e6,
                -1.07,
                0,
                0,
                0,
                0,
            ],  # Her
            [
                187.70417,
                12.39111,
                861,
                0,
                0,
                0,
                200e6,
                -0.86,
                0,
                0,
                0,
                0,
            ],  # Vir
            [
                83.63333,
                22.01444,
                1340,
                0,
                0,
                0,
                200e6,
                -0.22,
                0,
                0,
                0,
                0,
            ],  # Tau
            [
                299.86667,
                40.73389,
                7920,
                0,
                0,
                0,
                200e6,
                -0.78,
                0,
                0,
                0,
                0,
            ],  # Cyg
            [
                350.86667,
                58.81167,
                11900,
                0,
                0,
                0,
                200e6,
                -0.41,
                0,
                0,
                0,
                0,
            ],  # Cas
        )
    )


def main():
    """Main function."""
    # Load GLEAM catalogue data as a sky model.
    sky_dir = "./"
    gleam = fits.getdata(sky_dir + "GLEAM_EGC.fits", 1)
    gleam_sky_array = numpy.column_stack(
        (gleam["RAJ2000"], gleam["DEJ2000"], gleam["peak_flux_wide"])
    )

    # Define common base settings.
    tel_dir = "./"
    tel_model = "SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm"
    common_settings = {
        "simulator/max_sources_per_chunk": 65536,
        "simulator/write_status_to_log_file": True,
        "observation/start_frequency_hz": 125e6,  # First channel at 125 MHz.
        "observation/frequency_inc_hz": 5e6,  # Channels spaced every 5 MHz.
        "observation/num_channels": 11,
        "telescope/input_directory": tel_dir + tel_model,
        "interferometer/channel_bandwidth_hz": 100e3,  # 100 kHz-wide channels.
        "interferometer/time_average_sec": 1.0,
        "interferometer/max_time_samples_per_block": 4,
    }

    # Define observations.
    observations = {
        "short": {
            "observation/length": 5 * 60,
            "observation/num_time_steps": 300,
            "telescope/external_tec_screen/input_fits_file": "screen_short_300_1.0.fits",
        },
        "medium": {
            "observation/length": 30 * 60,
            "observation/num_time_steps": 300,
            "telescope/external_tec_screen/input_fits_file": "screen_medium_300_6.0.fits",
        },
        "long": {
            "observation/length": 4 * 60 * 60,
            "observation/num_time_steps": 240,
            "telescope/external_tec_screen/input_fits_file": "screen_long_240_60.0.fits",
        },
    }

    # Define fields.
    fields = {
        "EoR0": {
            "observation/phase_centre_ra_deg": 0.0,
            "observation/phase_centre_dec_deg": -27.0,
        },
        "EoR1": {
            "observation/phase_centre_ra_deg": 60.0,
            "observation/phase_centre_dec_deg": -30.0,
        },
        "EoR2": {
            "observation/phase_centre_ra_deg": 170.0,
            "observation/phase_centre_dec_deg": -10.0,
        },
    }

    # Define ionosphere settings.
    ionosphere = {
        "ionosphere_on": {"telescope/ionosphere_screen_type": "External"},
        "ionosphere_off": {"telescope/ionosphere_screen_type": "None"},
    }

    # Define sky model components.
    sky_models = {
        "A-team": oskar.Sky.from_array(bright_sources()),
        "GLEAM": oskar.Sky.from_array(gleam_sky_array),
    }

    # Loop over observations.
    for obs_name, obs_params in observations.items():
        # Copy the base settings dictionary.
        current_settings = copy.deepcopy(common_settings)

        # Update current settings with observation parameters.
        current_settings.update(obs_params)

        # Loop over fields.
        for field_name, field_params in fields.items():
            # Update current settings with field parameters.
            current_settings.update(field_params)

            # Update current settings with start time.
            ra0_deg = current_settings["observation/phase_centre_ra_deg"]
            length_sec = current_settings["observation/length"]
            start_time = get_start_time(ra0_deg, length_sec)
            current_settings["observation/start_time_utc"] = start_time

            # Loop over ionospheric screen on/off.
            for iono_name, iono_params in ionosphere.items():
                # Update current settings with ionosphere parameters.
                current_settings.update(iono_params)

                # Loop over sky model components.
                for sky_name, sky_model in sky_models.items():
                    # Update output MS filename based on current parameters.
                    ms_name = "SKA_LOW_SIM"
                    ms_name += "_" + obs_name
                    ms_name += "_" + field_name
                    ms_name += "_" + iono_name
                    ms_name += "_" + sky_name
                    ms_name += ".MS"

                    # Check if the MS already exists (if so, skip).
                    if os.path.isdir(ms_name):
                        continue

                    # Create the settings tree.
                    settings = oskar.SettingsTree("oskar_sim_interferometer")
                    settings.from_dict(current_settings)
                    settings["interferometer/ms_filename"] = ms_name

                    # Set up the simulator and run it.
                    sim = oskar.Interferometer(settings=settings)
                    sim.set_sky_model(sky_model)
                    sim.run()


if __name__ == "__main__":
    main()
