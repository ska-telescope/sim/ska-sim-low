#!/usr/bin/env python3
"""
Generate an animation by stepping through visibility time samples.
"""
import argparse
import copy

import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import animation
import matplotlib.pyplot as plt
import numpy
import oskar


# pylint: disable=too-many-instance-attributes
class Plotter:
    """Generate an animation by stepping through visibility time samples."""

    def __init__(self):
        """Constructor."""
        self._artists = ()
        self._axes = None
        self._base_settings = {}
        self._fig = None
        self._ms_list = []
        self._ms_names = []
        self._num_frames = 0
        self._title = ""

    def animate(
        self, imager_settings, ms_names, title="", fps=10, filename="out.mp4"
    ):
        """Function to generate the animation.

        Args:
            imager_settings (dict): Base settings for OSKAR imager.
            ms_names (list[str]): List of Measurement Sets to image.
            title (str): Main figure title.
            fps (int): Frames-per-second.
            filename (str): Name of output MP4 file.
        """
        # Store arguments.
        self._base_settings = imager_settings
        self._ms_names = ms_names
        self._title = title
        self._ms_list.clear()

        # Work out the number of frames to generate.
        num_images = len(self._ms_names)
        self._num_frames = 0
        for i in range(num_images):
            ms = oskar.MeasurementSet.open(self._ms_names[i], readonly=True)
            num_rows = ms.num_rows
            num_stations = ms.num_stations
            num_baselines = (num_stations * (num_stations - 1)) // 2
            self._num_frames = max(self._num_frames, num_rows // num_baselines)
            self._ms_list.append(ms)

        # Create the plot panels.
        num_cols = num_images
        if num_cols > 4:
            num_cols = 4
        num_rows = (num_images + num_cols - 1) // num_cols
        panel_size = 8
        if num_images > 1:
            panel_size = 5
        if num_images > 3:
            panel_size = 4
        fig_size = (num_cols * panel_size, num_rows * panel_size)
        fig, axes = plt.subplots(
            nrows=num_rows, ncols=num_cols, squeeze=False, figsize=fig_size
        )
        self._fig = fig
        self._axes = axes.flatten()

        # Call the animate function.
        anim = animation.FuncAnimation(
            self._fig,
            self._animate_func,
            init_func=self._init_func,
            frames=range(0, self._num_frames),
            interval=1000.0 / fps,
            blit=False,
        )

        # Save animation.
        anim.save(filename, writer="ffmpeg", bitrate=3500)
        plt.close(fig=fig)

    def _init_func(self):
        """Internal initialisation function called by FuncAnimation."""
        # Create an empty image.
        imsize = self._base_settings["image/size"]
        zeros = numpy.zeros((imsize, imsize))
        zeros[0, 0] = 1

        # Create list of matplotlib artists that must be updated each frame.
        artists = []

        # Iterate plot panels.
        for i in range(len(self._axes)):
            ax = self._axes[i]
            im = ax.imshow(zeros, aspect="equal", cmap="gnuplot2")
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            cbar = plt.colorbar(im, cax=cax)
            ax.invert_yaxis()
            ax.axes.xaxis.set_visible(False)
            ax.axes.yaxis.set_visible(False)
            if i < len(self._ms_names):
                ax.set_title(self._ms_names[i])
            else:
                cbar.set_ticks([])
                cbar.set_ticklabels([])
            artists.append(im)

        # Set figure title.
        self._fig.suptitle(self._title, fontsize=16, y=0.95)

        # Return tuple of artists to update.
        self._artists = tuple(artists)
        return self._artists

    def _animate_func(self, frame):
        """Internal function called per frame by FuncAnimation.

        Args:
            frame (int): Frame index.
        """
        # Iterate plot panels.
        num_panels = len(self._ms_list)
        for i in range(num_panels):
            # Read the visibility meta data.
            freq_start_hz = self._ms_list[i].freq_start_hz
            freq_inc_hz = self._ms_list[i].freq_inc_hz
            num_channels = self._ms_list[i].num_channels
            num_stations = self._ms_list[i].num_stations
            num_rows = self._ms_list[i].num_rows
            num_baselines = (num_stations * (num_stations - 1)) // 2

            # Read the visibility data and coordinates.
            start_row = frame * num_baselines
            if start_row >= num_rows or start_row + num_baselines > num_rows:
                continue
            (u, v, w) = self._ms_list[i].read_coords(start_row, num_baselines)
            vis = self._ms_list[i].read_column(
                "DATA", start_row, num_baselines
            )
            num_pols = vis.shape[-1]

            # Create settings for the imager.
            params = copy.deepcopy(self._base_settings)
            settings = oskar.SettingsTree("oskar_imager")
            settings.from_dict(params)

            # Make the image for this frame.
            print(
                "Generating frame %d/%d, panel %d/%d"
                % (frame + 1, self._num_frames, i + 1, num_panels)
            )
            imager = oskar.Imager(settings=settings)
            imager.set_vis_frequency(freq_start_hz, freq_inc_hz, num_channels)
            imager.update(
                u, v, w, vis, end_channel=num_channels - 1, num_pols=num_pols
            )
            data = imager.finalise(return_images=1)

            # Update the plot panel and colourbar.
            self._artists[i].set_data(data["images"][0])
            self._artists[i].autoscale()


def main():
    """Main function."""
    parser = argparse.ArgumentParser(
        description="Make an animation from one or more Measurement Sets",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "ms_names", metavar="MS", nargs="+", help="Measurement Set path(s)"
    )
    parser.add_argument(
        "--fov_deg",
        type=float,
        default=0.5,
        help="Field of view to image, in degrees",
    )
    parser.add_argument(
        "--size", type=int, default=256, help="Image side length, in pixels"
    )
    parser.add_argument(
        "--fps", type=int, default=10, help="Frames per second in output"
    )
    parser.add_argument("--out", default="out.mp4", help="Output filename")
    parser.add_argument("--title", default="", help="Overall figure title")
    args = parser.parse_args()

    # Imager settings.
    imager_settings = {"image/fov_deg": args.fov_deg, "image/size": args.size}

    # Make animation.
    plotter = Plotter()
    plotter.animate(
        imager_settings, args.ms_names, args.title, args.fps, args.out
    )


if __name__ == "__main__":
    main()
