#!/usr/bin/env python3

import os
import oskar

common_params = {
    "simulator/double_precision": False,
    "simulator/max_sources_per_chunk": 8192,
    "simulator/keep_log_file": True,
    "simulator/write_status_to_log_file": True,
    "sky/healpix_fits/file": "haslam_nside_128.fits",
    "sky/healpix_fits/min_abs_val": 30,
    "observation/mode": "Drift scan",
    "observation/start_frequency_hz": 100e6,
    "observation/start_time_utc": "2000-01-01 09:30:00.0",
    "observation/length": "24:00:00",
    "observation/num_time_steps": 48,
    "telescope/allow_station_beam_duplication": True,
}

models = {
    "dipoles": {
        "telescope/input_directory": "single_station_dipoles.tm",
        "telescope/pol_mode": "Scalar",
    },
    "SKALA4": {
        "telescope/input_directory": "single_station_SKALA4.tm",
        "telescope/aperture_array/element_pattern/swap_xy": True,
    },
    "SKALA4_with_gains": {
        "telescope/input_directory": "single_station_SKALA4_with_gains.tm",
        "telescope/aperture_array/element_pattern/swap_xy": True,
    },
}

# Loop over telescope models.
for model_name, model_params in models.items():
    # Create settings.
    settings = oskar.SettingsTree("oskar_sim_interferometer")
    settings.from_dict(common_params)
    settings.from_dict(model_params)

    # Set output file name (skip if already done).
    out_name = "drift_scan_%s.ms" % model_name
    if os.path.exists(out_name):
        continue
    settings["interferometer/ms_filename"] = out_name

    # Run simulation.
    sim = oskar.Interferometer(settings=settings)
    print('Running model "%s"...' % model_name)
    sim.run()
