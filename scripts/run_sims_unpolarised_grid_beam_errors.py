#!/usr/bin/env python3

"""Run polarisation simulations with beamforming errors."""

import glob
import json
import logging
import os
import sys

import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy
import oskar  # Requires OSKAR 2.8 / oskarpy 0.2

from .utils import get_start_time

LOG = logging.getLogger()


class Simulator(oskar.Interferometer):
    """Simulates and images visibilities concurrently.

    Inherits oskar.Interferometer to image each block in the process_block()
    method.
    """

    def __init__(
        self, imager=None, precision=None, settings=None, ref_vis=None
    ):
        """Creates the simulator, storing a handle to the imager.

        Args:
            imager (Optional[oskar.Imager]):
                Imager to use.
            precision (Optional[str]):
                Either 'double' or 'single' to specify the numerical
                precision of the simulation. Default 'double'.
            settings (Optional[oskar.SettingsTree]):
                Optional settings to use to set up the simulator.
            ref_vis (Optional[str]):
                Pathname of reference visibility file.
        """
        oskar.Interferometer.__init__(self, precision, settings)
        self._imager = imager
        self._return_images = 0
        self._ref_vis = ref_vis
        self._ref_handle = None
        self._ref_hdr = None
        self._ref_block = None
        if ref_vis:
            (self._ref_hdr, self._ref_handle) = oskar.VisHeader.read(ref_vis)
            self._ref_block = oskar.VisBlock.create_from_header(self._ref_hdr)

    def finalise(self):
        """Called automatically by the base class at the end of run()."""
        oskar.Interferometer.finalise(self)
        if self._imager and not self.coords_only:
            return self._imager.finalise(return_images=self._return_images)

    def process_block(self, block, block_index):
        """Writes the visibility block to any open file(s), and images it.

        Args:
            block (oskar.VisBlock): A handle to the block to be processed.
            block_index (int):      The index of the visibility block.
        """
        if not self.coords_only:
            if self._ref_vis:
                # Read the reference visibility data block.
                self._ref_block.read(
                    self._ref_hdr, self._ref_handle, block_index
                )

                # Subtract reference block from this one.
                block.cross_correlations()[
                    :
                ] -= self._ref_block.cross_correlations()[:]

            # Write (modified) block to any open files.
            self.write_block(block, block_index)

        # Update imager with modified block.
        if self._imager:
            self._imager.update_from_block(self.vis_header(), block)

    # pylint: disable=arguments-differ
    def run(self, return_images=0):
        """Runs the interferometer simulator and imager.

        Args:
            return_images (int): Number of images to return.
        """
        # Save flags for use in finalise().
        self._return_images = return_images

        # Check if imaging with uniform weighting or W-projection.
        need_coords_first = False
        if self._imager:
            if (
                self._imager.weighting == "Uniform"
                or self._imager.algorithm == "W-projection"
            ):
                need_coords_first = True

        # Simulate coordinates first, if required.
        if need_coords_first:
            self.set_coords_only(True)
            oskar.Interferometer.run(self)
            self.set_coords_only(False)

        # Simulate and image the visibilities.
        return oskar.Interferometer.run(self)

    def set_coords_only(self, value):
        """Calls set_coords_only() on interferometer and imager objects."""
        oskar.Interferometer.set_coords_only(self, value)
        if self._imager:
            self._imager.set_coords_only(value)


def plot_panel(ax, x, y, z, title, freq_MHz):
    """Plots a single Stokes parameter panel."""
    sc = ax.scatter(x, y, c=z, s=30, cmap="plasma")
    cb = plt.colorbar(sc, format="%.2e")
    cb.update_ticks()
    ax.set_title(title + " ({:.0f} MHz)".format(freq_MHz))
    ax.axis("equal")
    # ax.autoscale(enable=True, axis="both", tight=True)


def make_plots(prefix, ra0, dec0, freq_MHz, sky_pos, results):
    """Plot selected results."""
    ra = numpy.array([])
    dec = numpy.array([])
    stokes_i = numpy.array([])
    stokes_q = numpy.array([])
    stokes_u = numpy.array([])
    stokes_v = numpy.array([])
    num_pos = sky_pos.shape[0]

    # Loop over source positions.
    for s in range(num_pos):
        key = "%s_%.1f_%.1f_%.0f_MHz" % (
            prefix,
            sky_pos[s, 0],
            sky_pos[s, 1],
            freq_MHz,
        )
        if key in results:
            ra = numpy.append(ra, numpy.radians(sky_pos[s, 0]))
            dec = numpy.append(dec, numpy.radians(sky_pos[s, 1]))
            stokes_i = numpy.append(stokes_i, results[key]["i"])
            stokes_q = numpy.append(stokes_q, results[key]["q"])
            stokes_u = numpy.append(stokes_u, results[key]["u"])
            stokes_v = numpy.append(stokes_v, results[key]["v"])

    # Convert to direction cosines for plotting.
    x = numpy.cos(dec) * numpy.sin(ra - ra0)
    y = numpy.cos(numpy.radians(dec0)) * numpy.sin(dec) - numpy.sin(
        numpy.radians(dec0)
    ) * numpy.cos(dec) * numpy.cos(ra - ra0)

    # Sub-plots, one for each Stokes parameter.
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221)
    plot_panel(ax, x, y, stokes_i, "Measured Stokes I", freq_MHz)
    ax = fig.add_subplot(222)
    plot_panel(ax, x, y, stokes_q, "Measured Stokes Q", freq_MHz)
    ax = fig.add_subplot(223)
    plot_panel(ax, x, y, stokes_u, "Measured Stokes U", freq_MHz)
    ax = fig.add_subplot(224)
    plot_panel(ax, x, y, stokes_v, "Measured Stokes V", freq_MHz)

    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(
        labelcolor="none", top="off", bottom="off", left="off", right="off"
    )
    ax.set_xlabel("x direction cosine", labelpad=20)
    ax.set_ylabel("y direction cosine", labelpad=40)
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])

    # Save and close.
    fig.tight_layout()
    plt.savefig("%s_%03.0f_MHz.png" % (prefix, freq_MHz))
    plt.close("all")


def run_set(
    prefix, axis_freq, ra0_deg, dec0_deg, base_settings, sky_pos, results
):
    """Run a set of simulations."""
    settings = oskar.SettingsTree("oskar_sim_interferometer")
    settings.from_dict(base_settings)
    tel_no_errors = oskar.Telescope(settings=settings)
    tel_errors = oskar.Telescope(settings=settings)
    tel_errors.override_element_cable_length_errors(0.015, seed=1, feed=0)
    tel_errors.override_element_cable_length_errors(0.015, seed=2, feed=1)
    num_pos = sky_pos.shape[0]
    for freq_MHz in axis_freq:
        frequency_hz = freq_MHz * 1e6
        settings["observation/start_frequency_hz"] = frequency_hz
        for s in range(num_pos):
            ra_deg = sky_pos[s, 0]
            dec_deg = sky_pos[s, 1]

            # Check if already done.
            key = "%s_%.1f_%.1f_%.0f_MHz" % (prefix, ra_deg, dec_deg, freq_MHz)
            if key in results:
                continue

            # Set up "reference" and "corrupted" telescope models.
            tel_no_errors.set_phase_centre(ra_deg, dec_deg)
            tel_errors.set_phase_centre(ra_deg, dec_deg)

            # Set up unpolarised single-source sky model.
            settings["observation/phase_centre_ra_deg"] = ra_deg
            settings["observation/phase_centre_dec_deg"] = dec_deg
            sky = oskar.Sky.from_array([ra_deg, dec_deg, 1.0])

            # Run reference simulation.
            ref_vis = key + "_ref.vis"
            settings["interferometer/oskar_vis_filename"] = ref_vis
            sim = Simulator(settings=settings)
            sim.set_sky_model(sky)
            sim.set_telescope_model(tel_no_errors)
            sim.run()
            del sim

            # Set up imager for this source position.
            fov_ref_frequency_hz = 140e6
            fov_ref_deg = 0.05
            fov_deg = fov_ref_deg * (fov_ref_frequency_hz / frequency_hz)
            imager = oskar.Imager()
            imager.set(fov_deg=fov_deg, image_size=256, image_type="Stokes")
            # if abs(ra_deg - 11.014) < 0.1 and abs(dec_deg - -15.991) < 0.1:
            #    imager.output_root = key

            # Run simulation and make images of all four polarisations.
            settings["interferometer/oskar_vis_filename"] = ""
            LOG.info(
                "Running simulation for source at (%.3f, %.3f)",
                ra_deg,
                dec_deg,
            )
            sim = Simulator(imager, settings=settings, ref_vis=ref_vis)
            sim.set_sky_model(sky)
            sim.set_telescope_model(tel_errors)
            output = sim.run(return_images=4)
            images = output["images"]

            # Store image peak values in results.
            results[key] = {
                "i": max(images[0].min(), images[0].max(), key=abs),
                "q": max(images[1].min(), images[1].max(), key=abs),
                "u": max(images[2].min(), images[2].max(), key=abs),
                "v": max(images[3].min(), images[3].max(), key=abs),
            }
            del images, sim, imager
            os.remove(ref_vis)

            # Update saved result set.
            with open("results.json", "w") as output_file:
                json.dump(results, output_file, indent=4)

        # Update plots.
        make_plots(prefix, ra0_deg, dec0_deg, freq_MHz, sky_pos, results)


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    ra0_deg = 0.0
    dec0_deg = -26.824
    obs_length_sec = 1.0
    base_settings = {
        "simulator/use_gpus": True,
        "simulator/max_sources_per_chunk": 2,
        "observation": {
            "start_time_utc": get_start_time(ra0_deg, obs_length_sec),
            "length": obs_length_sec,
            "num_time_steps": 1,
        },
        "telescope": {
            "input_directory": "SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm",
            "normalise_beams_at_phase_centre": False,
            "aperture_array/array_pattern/normalise": True,
            "aperture_array/element_pattern/normalise": True,
            # "aperture_array/element_pattern/enable_numerical": False
        },
        "interferometer/max_time_samples_per_block": 4,
    }

    # Generate source grid.
    sky_grid = oskar.Sky.generate_grid(ra0_deg, dec0_deg, 15, 80.0)
    sky_pos = sky_grid.to_array()

    # Load any saved result sets.
    results = {}
    for json_file in glob.glob("*results.json"):
        with open(json_file, "r") as input_file:
            results.update(json.load(input_file))

    # Run set.
    axis_freq = [50, 70, 110, 137, 160, 230, 320]
    # axis_freq = [230]
    prefix = "pol_scan_beam_errors_stokes_i_skala4"
    run_set(
        prefix, axis_freq, ra0_deg, dec0_deg, base_settings, sky_pos, results
    )


if __name__ == "__main__":
    main()
