#!/usr/bin/env python3

import os

from astropy.io import fits
import numpy
import oskar

# Simulation parameters.
ra0_deg = 0.0
dec0_deg = -55.0
num_channels = 10000
sim_params = {
    "simulator/max_sources_per_chunk": 150,
    "observation/phase_centre_ra_deg": ra0_deg,
    "observation/phase_centre_dec_deg": dec0_deg,
    "observation/start_frequency_hz": 1.0e08,
    "observation/num_channels": num_channels,
    "observation/frequency_inc_hz": 1.0e04,
    "observation/start_time_utc": "2000-01-01 09:00:00.0",
    "observation/length": 600.0,
    "observation/num_time_steps": 666,
    "telescope/normalise_beams_at_phase_centre": False,
    "telescope/station_type": "Gaussian beam",
    "telescope/gaussian_beam/fwhm_deg": 4.5,
    "telescope/gaussian_beam/ref_freq_hz": 1.0e08,
    "interferometer/channel_bandwidth_hz": 1.0e04,
    "interferometer/time_average_sec": 0.9,
    "interferometer/max_time_samples_per_block": 4,
    "interferometer/max_channels_per_block": 10000,
}

# Imaging parameters.
img_params = {
    "image/fov_deg": 6.5,
    "image/size": 8192,
    "image/fft/use_gpu": True,
    "image/fft/grid_on_gpu": True,
}

# Create a simple 2-source sky model.
sky_array = numpy.array([[2.0, -53.5, 30.0], [359.0, -56.0, 10.0]])
simple_sky = oskar.Sky.from_array(sky_array)

# Create a filtered GLEAM-based sky model.
data = fits.getdata("GLEAM_EGC.fits", 1)
sky_array = numpy.column_stack(
    (data["RAJ2000"], data["DEJ2000"], data["peak_flux_wide"])
)
gleam_sky = oskar.Sky.from_array(sky_array)
gleam_sky.filter_by_radius(0, 3.5, ra0_deg, dec0_deg)
gleam_sky.filter_by_flux(0.2, 1000)
# gleam_sky.to_ds9_regions("gleam.reg")

# Define dictionary of sky models.
sky_models = {"2_sources": simple_sky, "GLEAM": gleam_sky}

# Define dictionary of telescope models.
tel_models = {
    "small_psi": {"telescope/input_directory": "PSI-LOW_5_stations_1_km.tm"},
    "large_psi": {"telescope/input_directory": "PSI-LOW_8_stations_1_km.tm"},
    "small_psi_with_error": {
        "telescope/input_directory": "PSI-LOW_5_stations_1_km_error.tm"
    },
    "large_psi_with_error": {
        "telescope/input_directory": "PSI-LOW_8_stations_1_km_error.tm"
    },
}

# Loop over sky models.
for sky_model_name, sky_model in sky_models.items():
    # Loop over telescope models.
    for tel_model_name, tel_model_params in tel_models.items():
        # Construct MS file name.
        tel_model_path = tel_model_params["telescope/input_directory"]
        tel_model_root = os.path.splitext(tel_model_path)[0]
        ms_name = "%s_%s_%d_channels.ms" % (
            tel_model_root,
            sky_model_name,
            num_channels,
        )

        # Run simulation if not done.
        if not os.path.exists(ms_name):
            settings = oskar.SettingsTree("oskar_sim_interferometer")
            settings.from_dict(sim_params)
            settings.from_dict(tel_model_params)
            settings["interferometer/ms_filename"] = ms_name
            sim = oskar.Interferometer(settings=settings)
            sim.set_sky_model(sky_model)
            sim.run()

        # Construct FITS file root name.
        fits_root = os.path.splitext(ms_name)[0]

        # Run imager if not done.
        if not os.path.exists("%s_I.fits" % fits_root):
            settings = oskar.SettingsTree("oskar_imager")
            settings.from_dict(img_params)
            settings["image/input_vis_data"] = ms_name
            settings["image/root_path"] = fits_root
            img = oskar.Imager(settings=settings)
            img.run()
