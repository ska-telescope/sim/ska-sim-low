#!/usr/bin/env python3

"""Run polarisation simulations with beamforming."""

import glob
import json
import logging
import sys

import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy
import oskar

from .utils import get_start_time

LOG = logging.getLogger()


class Simulator(oskar.Interferometer):
    """Simulates and images visibilities concurrently.

    Inherits oskar.Interferometer to image each block in the process_block()
    method.
    """

    def __init__(self, imager, precision=None, settings=None):
        """Creates the simulator, storing a handle to the imager.

        Args:
            imager (oskar.Imager):
                Imager to use.
            precision (Optional[str]):
                Either 'double' or 'single' to specify the numerical
                precision of the simulation. Default 'double'.
            settings (Optional[oskar.SettingsTree]):
                Optional settings to use to set up the simulator.
        """
        oskar.Interferometer.__init__(self, precision, settings)
        self._imager = imager
        self._return_images = 0

    def finalise(self):
        """Called automatically by the base class at the end of run()."""
        oskar.Interferometer.finalise(self)
        if not self.coords_only:
            return self._imager.finalise(return_images=self._return_images)

    def process_block(self, block, block_index):
        """Writes the visibility block to any open file(s), and images it.

        Args:
            block (oskar.VisBlock): A handle to the block to be processed.
            block_index (int):      The index of the visibility block.
        """
        if not self.coords_only:
            self.write_block(block, block_index)
        self._imager.update_from_block(self.vis_header(), block)

    # pylint: disable=arguments-differ
    def run(self, return_images=0):
        """Runs the interferometer simulator and imager.

        Args:
            return_images (int): Number of images to return.
        """
        # Save flags for use in finalise().
        self._return_images = return_images

        # Check if imaging with uniform weighting or W-projection.
        need_coords_first = False
        if (
            self._imager.weighting == "Uniform"
            or self._imager.algorithm == "W-projection"
        ):
            need_coords_first = True

        # Simulate coordinates first, if required.
        if need_coords_first:
            self.set_coords_only(True)
            oskar.Interferometer.run(self)
            self.set_coords_only(False)

        # Simulate and image the visibilities.
        return oskar.Interferometer.run(self)

    def set_coords_only(self, value):
        """Calls set_coords_only() on interferometer and imager objects."""
        oskar.Interferometer.set_coords_only(self, value)
        self._imager.set_coords_only(value)


def plot_panel(ax, x, y, z, title):
    shape = (int(numpy.sqrt(x.shape[0])), -1)
    X = numpy.reshape(x, shape)
    Y = numpy.reshape(y, shape)
    Z = numpy.reshape(z, shape)
    norm = matplotlib.colors.Normalize(vmin=numpy.min(Z), vmax=numpy.max(Z))
    ax.contour(X, Y, Z, cmap="plasma", norm=norm)
    sp = ax.scatter(x, y, c=z, cmap="plasma", norm=norm, zorder=10)
    plt.colorbar(sp, ax=ax)
    plt.gca().set_title(title)
    plt.gca().axis("equal")


def make_plots(prefix, ra0, dec0, frequency_hz, sky_pos, results):
    """Plot selected results."""
    ra = numpy.array([])
    dec = numpy.array([])
    stokes_i = numpy.array([])
    stokes_q = numpy.array([])
    stokes_u = numpy.array([])
    stokes_v = numpy.array([])
    num_pos = sky_pos.shape[0]

    # Loop over source positions.
    for s in range(num_pos):
        key = "%s_%.1f_%.1f_%.0f_MHz" % (
            prefix,
            sky_pos[s, 0],
            sky_pos[s, 1],
            frequency_hz * 1e-6,
        )
        if key in results:
            ra = numpy.append(ra, numpy.radians(sky_pos[s, 0]))
            dec = numpy.append(dec, numpy.radians(sky_pos[s, 1]))
            stokes_i = numpy.append(stokes_i, results[key]["i"])
            stokes_q = numpy.append(stokes_q, results[key]["q"])
            stokes_u = numpy.append(stokes_u, results[key]["u"])
            stokes_v = numpy.append(stokes_v, results[key]["v"])

    # Convert to direction cosines for plotting.
    x = numpy.cos(dec) * numpy.sin(ra - ra0)
    y = numpy.cos(numpy.radians(dec0)) * numpy.sin(dec) - numpy.sin(
        numpy.radians(dec0)
    ) * numpy.cos(dec) * numpy.cos(ra - ra0)

    # Sub-plots, one for each Stokes parameter.
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221)
    sc = ax.scatter(x, y, c=stokes_i, s=30, cmap="plasma")
    plt.colorbar(sc)
    plt.gca().set_title("Measured Stokes I")
    plt.gca().axis("equal")
    ax = fig.add_subplot(222)
    sc = ax.scatter(x, y, c=stokes_q, s=30, cmap="plasma")
    plt.colorbar(sc)
    plt.gca().set_title("Measured Stokes Q")
    plt.gca().axis("equal")
    ax = fig.add_subplot(223)
    sc = ax.scatter(x, y, c=stokes_u, s=30, cmap="plasma")
    plt.colorbar(sc)
    plt.gca().set_title("Measured Stokes U")
    plt.gca().axis("equal")
    ax = fig.add_subplot(224)
    sc = ax.scatter(x, y, c=stokes_v, s=30, cmap="plasma")
    plt.colorbar(sc)
    plt.gca().set_title("Measured Stokes V")
    plt.gca().axis("equal")

    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(
        labelcolor="none", top="off", bottom="off", left="off", right="off"
    )
    ax.set_xlabel("x direction cosine", labelpad=20)
    ax.set_ylabel("y direction cosine", labelpad=40)
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])

    # Save and close.
    fig.tight_layout()
    plt.savefig("%s_%03.0f_MHz.png" % (prefix, frequency_hz * 1e-6))
    plt.close("all")

    # Relative values:
    # normalised I, pol angle, fractional linear, fractional circular.
    norm_i = stokes_i / numpy.max(stokes_i)
    pol_angle = (0.5 * numpy.arctan2(stokes_u, stokes_q)) * 180.0 / numpy.pi
    frac_linear = (
        numpy.sqrt(numpy.square(stokes_q) + numpy.square(stokes_u)) / stokes_i
    )
    frac_circular = numpy.abs(stokes_v) / stokes_i

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221)
    plot_panel(ax, x, y, norm_i, "Measured Stokes I (Scaled to max)")
    ax = fig.add_subplot(222)
    plot_panel(ax, x, y, frac_linear, "Measured Linear Polarisation Fraction")
    ax = fig.add_subplot(223)
    plot_panel(
        ax, x, y, frac_circular, "Measured Circular Polarisation Fraction"
    )
    ax = fig.add_subplot(224)
    plot_panel(ax, x, y, pol_angle, "Measured Polarisation Angle [deg]")

    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(
        labelcolor="none", top="off", bottom="off", left="off", right="off"
    )
    ax.set_xlabel("x direction cosine", labelpad=20)
    ax.set_ylabel("y direction cosine", labelpad=40)
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])

    # Save and close.
    fig.tight_layout()
    plt.savefig("derived_%s_%03.0f_MHz.png" % (prefix, frequency_hz * 1e-6))
    plt.close("all")


def run_set(
    prefix, axis_freq, ra0_deg, dec0_deg, base_settings, sky_pos, results
):
    """Run a set of simulations."""
    settings = oskar.SettingsTree("oskar_sim_interferometer")
    num_pos = sky_pos.shape[0]
    settings.from_dict(base_settings)
    for freq_MHz in axis_freq:
        frequency_hz = freq_MHz * 1e6
        settings["observation/start_frequency_hz"] = frequency_hz
        for s in range(num_pos):
            ra_deg = sky_pos[s, 0]
            dec_deg = sky_pos[s, 1]

            # Check if already done.
            key = "%s_%.1f_%.1f_%.0f_MHz" % (prefix, ra_deg, dec_deg, freq_MHz)
            if key in results:
                continue

            # Set up unpolarised single-source sky model.
            sky = oskar.Sky.from_array([ra_deg, dec_deg, 1.0])

            # Set up imager for this source position.
            fov_ref_frequency_hz = 140e6
            fov_ref_deg = 0.05
            fov_deg = fov_ref_deg * (fov_ref_frequency_hz / frequency_hz)
            imager = oskar.Imager()
            imager.set(fov_deg=fov_deg, image_size=256, image_type="Stokes")
            imager.set(fft_on_gpu=True)
            # imager.set_direction(ra_deg, dec_deg)
            # if abs(ra_deg - 0) < 0.1 and abs(dec_deg - 0) < 0.1:
            #    imager.output_root = key

            # Run simulation and make images of all four polarisations.
            settings["observation/phase_centre_ra_deg"] = str(ra_deg)
            settings["observation/phase_centre_dec_deg"] = str(dec_deg)
            LOG.info(
                "Running simulation for source at (%.3f, %.3f)",
                ra_deg,
                dec_deg,
            )
            sim = Simulator(imager, settings=settings)
            sim.set_sky_model(sky)
            output = sim.run(return_images=4)
            images = output["images"]

            # Store image peak values in results.
            results[key] = {
                "i": max(images[0].min(), images[0].max(), key=abs),
                "q": max(images[1].min(), images[1].max(), key=abs),
                "u": max(images[2].min(), images[2].max(), key=abs),
                "v": max(images[3].min(), images[3].max(), key=abs),
            }
            del images, sim, imager

            # Update saved result set.
            with open("results.json", "w") as output_file:
                json.dump(results, output_file, indent=4)

        # Update plots.
        make_plots(prefix, ra0_deg, dec0_deg, frequency_hz, sky_pos, results)


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    ra0_deg = 0.0
    dec0_deg = 0.0
    obs_length_sec = 1.0
    base_settings = {
        "simulator/use_gpus": True,
        "simulator/max_sources_per_chunk": 2,
        "observation": {
            "start_frequency_hz": 110e6,
            "start_time_utc": get_start_time(ra0_deg, obs_length_sec),
            "length": obs_length_sec,
            "num_time_steps": 1,
            "phase_centre_ra_deg": ra0_deg,
            "phase_centre_dec_deg": dec0_deg,
        },
        "telescope": {
            "input_directory": "/Users/fred/Data/SKA1-LOW/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies_equator.tm",
            # "input_directory": "SKA1-LOW_SKO-0000422_Rev3_38m_equator.tm",
            "normalise_beams_at_phase_centre": False,
            "aperture_array/array_pattern/normalise": True,
            "aperture_array/element_pattern/normalise": True,
            "aperture_array/array_pattern/enable": False,
        },
        "interferometer/max_time_samples_per_block": 4,
    }

    # Generate source grid.
    sky_grid = oskar.Sky.generate_grid(ra0_deg, dec0_deg, 15, 80.0)
    sky_pos = sky_grid.to_array()

    # Load any saved result sets.
    results = {}
    for json_file in glob.glob("*results.json"):
        with open(json_file, "r") as input_file:
            results.update(json.load(input_file))

    # Run set.
    # axis_freq = [50, 70, 110, 137, 160, 230, 320]
    axis_freq = [230]
    prefix = "pol_scan_stokes_i_dipole_no_parangle"
    run_set(
        prefix, axis_freq, ra0_deg, dec0_deg, base_settings, sky_pos, results
    )


if __name__ == "__main__":
    main()
