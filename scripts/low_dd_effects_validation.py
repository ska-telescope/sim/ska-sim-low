"""
Creates animations of simulated data to check that they are as expected.
"""

import copy

import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.animation import FFMpegWriter
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy
import oskar


# From https://stackoverflow.com/a/48531878
class FasterFFMpegWriter(FFMpegWriter):
    """FFMpeg-pipe writer bypassing figure.savefig."""

    def __init__(self, **kwargs):
        """Initialize the Writer object and sets the default frame_format."""
        super().__init__(**kwargs)
        self.frame_format = "argb"

    def grab_frame(self, **savefig_kwargs):
        """Grab the image information from the figure and save as a movie frame.

        Doesn't use savefig to be faster: savefig_kwargs will be ignored.
        """
        try:
            # re-adjust the figure size and dpi in case it has been changed by the
            # user.  We must ensure that every frame is the same size or
            # the movie will not save correctly.
            self.fig.set_size_inches(self._w, self._h)
            self.fig.set_dpi(self.dpi)
            # Draw and save the frame as an argb string to the pipe sink
            self.fig.canvas.draw()
            self._frame_sink().write(self.fig.canvas.tostring_argb())
        except (RuntimeError, IOError) as e:
            out, err = self._proc.communicate()
            raise IOError(
                "Error saving animation to file (cause: {0}) "
                "Stdout: {1} StdError: {2}. It may help to re-run "
                "with --verbose-debug.".format(e, out, err)
            )


class Plotter:
    """Takes care of the details when making an animation using matplotlib."""

    def __init__(self, base_settings, obs, field, freqs, sources, ms0, ms1):
        self._artists = ()
        self._fig = None
        self._axes = None
        self._base_settings = base_settings
        self._obs = obs
        self._field = field
        self._freqs = freqs
        self._sources = sources
        self._ms0 = ms0
        self._ms1 = ms1

    def animate(self, num_times, fps, filename):
        fig, axes = plt.subplots(
            nrows=len(self._freqs), ncols=4, figsize=(16, 8)
        )
        self._fig = fig
        self._axes = axes.flatten()
        anim = animation.FuncAnimation(
            self._fig,
            self.animate_func,
            init_func=self.init_func,
            frames=range(0, num_times),
            interval=1000.0 / fps,
            blit=False,
        )

        # Save animation.
        writer = FasterFFMpegWriter(fps=fps, bitrate=3500)
        anim.save(filename, writer=writer)
        plt.close(fig=fig)

    def init_func(self):
        # Create an empty image.
        imsize = self._base_settings["image/size"]
        zeros = numpy.zeros((imsize, imsize))
        iono_names = ["ionosphere on", "ionosphere off"]

        # Create list of matplotlib artists that must be updated each frame.
        artists = []

        # Iterate plot panels.
        panel = 0
        for freq in self._freqs:
            for source_name, _ in self._sources.items():
                if self._field not in source_name:
                    continue
                for i in range(2):  # Ionosphere on/off
                    ax = self._axes[panel]
                    im = ax.imshow(zeros, aspect="equal", cmap="gist_heat")
                    divider = make_axes_locatable(ax)
                    cax = divider.append_axes("right", size="5%", pad=0.05)
                    plt.colorbar(im, cax=cax)
                    ax.invert_yaxis()
                    ax.axes.xaxis.set_visible(False)
                    ax.axes.yaxis.set_visible(False)
                    ax.set_title(
                        "%d MHz, %s,\n%s"
                        % (freq, iono_names[i], source_name.replace("_", " "))
                    )
                    artists.append(im)
                    panel += 1

        # Set figure title.
        self._fig.suptitle(
            "%s field, %s observation" % (self._field, self._obs), fontsize=16
        )

        # Return tuple of artists to update.
        self._artists = tuple(artists)
        return self._artists

    def animate_func(self, frame):
        """Function called per frame of animation."""
        # Read the visibility meta data.
        freq_start_hz = self._ms0.freq_start_hz
        freq_inc_hz = self._ms0.freq_inc_hz
        ra0_deg = self._ms0.phase_centre_ra_rad * 180.0 / numpy.pi
        dec0_deg = self._ms0.phase_centre_dec_rad * 180.0 / numpy.pi
        num_channels = self._ms0.num_channels
        num_stations = self._ms0.num_stations
        num_baselines = (num_stations * (num_stations - 1)) // 2

        # Read the visibility data and coordinates.
        start_row = frame * num_baselines
        (u, v, w) = self._ms0.read_coords(start_row, num_baselines)
        vis = []
        vis.append(self._ms1.read_column("DATA", start_row, num_baselines))
        vis.append(self._ms0.read_column("DATA", start_row, num_baselines))

        # Iterate plot panels.
        panel = 0
        for freq in self._freqs:
            for source_name, source_params in self._sources.items():
                if self._field not in source_name:
                    continue
                for i in range(len(vis)):
                    # Create the settings.
                    params = copy.deepcopy(self._base_settings)
                    params.update(source_params)
                    params["image/freq_min_hz"] = (freq - 0.5) * 1e6
                    params["image/freq_max_hz"] = (freq + 0.5) * 1e6
                    settings = oskar.SettingsTree("oskar_imager")
                    settings.from_dict(params)

                    # Make the image.
                    print("Generating frame %d, panel %d" % (frame, panel))
                    imager = oskar.Imager(settings=settings)
                    imager.set_vis_frequency(
                        freq_start_hz, freq_inc_hz, num_channels
                    )
                    imager.set_vis_phase_centre(ra0_deg, dec0_deg)
                    imager.coords_only = True
                    imager.update(
                        u,
                        v,
                        w,
                        vis[i],
                        end_channel=num_channels - 1,
                        num_pols=4,
                    )
                    imager.coords_only = False
                    imager.update(
                        u,
                        v,
                        w,
                        vis[i],
                        end_channel=num_channels - 1,
                        num_pols=4,
                    )
                    data = imager.finalise(return_images=1)

                    # Update the plot panel and colourbar.
                    self._artists[panel].set_data(data["images"][0])
                    self._artists[panel].autoscale()
                    panel += 1


def main():
    # Common settings.
    base_settings = {
        "image/fov_deg": 0.1333333333333,  # 8 arcmin
        "image/size": 256,
        "image/direction": "RA, Dec.",
        "image/weighting": "Uniform",
    }

    # List of observation types to image.
    observations = ["long", "medium", "short"]
    # observations = ['medium']

    # List of field names to image.
    fields = ["EoR0", "EoR1"]

    # List of frequencies to image (in MHz).
    freqs = [125, 175]

    # List of sources in each field to image.
    sources = {
        # EoR0, centre:
        "EoR0_source_1_(centre)": {
            "image/direction/ra_deg": 0.189167,
            "image/direction/dec_deg": -27.380278,
        },
        # EoR0, top right:
        "EoR0_source_2_(top_right)": {
            "image/direction/ra_deg": 357.709167,
            "image/direction/dec_deg": -24.950833,
        },
        # EoR1, bottom left:
        "EoR1_source_1_(bottom_left)": {
            "image/direction/ra_deg": 61.754166,
            "image/direction/dec_deg": -31.869444,
        },
        # EoR1, top right:
        "EoR1_source_2_(top_right)": {
            "image/direction/ra_deg": 57.916666,
            "image/direction/dec_deg": -27.7316667,
        },
    }

    # Iterate observations and fields.
    for obs in observations:
        for field in fields:
            # Open the Measurement Sets.
            ms0_name = "SKA_LOW_SIM_%s_%s_ionosphere_off_GLEAM.MS" % (
                obs,
                field,
            )
            ms1_name = "SKA_LOW_SIM_%s_%s_ionosphere_on_GLEAM.MS" % (
                obs,
                field,
            )
            ms0 = oskar.MeasurementSet.open(ms0_name, readonly=True)
            ms1 = oskar.MeasurementSet.open(ms1_name, readonly=True)

            # Read the visibility meta data.
            num_rows = ms0.num_rows
            num_stations = ms0.num_stations
            num_baselines = (num_stations * (num_stations - 1)) // 2
            num_times = num_rows // num_baselines
            # num_times = 20 # Test

            # Make animation.
            weighting = base_settings["image/weighting"]
            filename = "SKA_LOW_SIM_%s_%s_%s.mp4" % (obs, field, weighting)
            plotter = Plotter(
                base_settings, obs, field, freqs, sources, ms0, ms1
            )
            plotter.animate(num_times, 10, filename)


if __name__ == "__main__":
    main()
