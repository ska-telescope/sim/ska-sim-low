#!/usr/bin/env python3

"""Script to run LOW station beam error simulations."""

import concurrent.futures
import json
import logging
import os
import sys

from astropy.io import fits
from astropy.time import Time
import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
from matplotlib import pyplot as plt
import numpy
import oskar

from .utils import get_start_time

LOG = logging.getLogger()


def bright_sources():
    """Returns a list of bright A-team sources."""
    # Sgr A: guesstimates only!
    # For A: data from the Molonglo Southern 4 Jy sample (VizieR).
    # Others from GLEAM reference paper, Hurley-Walker et al. (2017), Table 2.
    return numpy.array(
        (
            [266.41683, -29.00781, 2000, 0, 0, 0, 0, 0, 0, 3600, 3600, 0],
            [
                50.67375,
                -37.20833,
                528,
                0,
                0,
                0,
                178e6,
                -0.51,
                0,
                0,
                0,
                0,
            ],  # For
            [
                201.36667,
                -43.01917,
                1370,
                0,
                0,
                0,
                200e6,
                -0.50,
                0,
                0,
                0,
                0,
            ],  # Cen
            [
                139.52500,
                -12.09556,
                280,
                0,
                0,
                0,
                200e6,
                -0.96,
                0,
                0,
                0,
                0,
            ],  # Hyd
            [
                79.95833,
                -45.77889,
                390,
                0,
                0,
                0,
                200e6,
                -0.99,
                0,
                0,
                0,
                0,
            ],  # Pic
            [
                252.78333,
                4.99250,
                377,
                0,
                0,
                0,
                200e6,
                -1.07,
                0,
                0,
                0,
                0,
            ],  # Her
            [
                187.70417,
                12.39111,
                861,
                0,
                0,
                0,
                200e6,
                -0.86,
                0,
                0,
                0,
                0,
            ],  # Vir
            [
                83.63333,
                22.01444,
                1340,
                0,
                0,
                0,
                200e6,
                -0.22,
                0,
                0,
                0,
                0,
            ],  # Tau
            [
                299.86667,
                40.73389,
                7920,
                0,
                0,
                0,
                200e6,
                -0.78,
                0,
                0,
                0,
                0,
            ],  # Cyg
            [
                350.86667,
                58.81167,
                11900,
                0,
                0,
                0,
                200e6,
                -0.41,
                0,
                0,
                0,
                0,
            ],  # Cas
        )
    )


def make_vis_data(settings, sky, tel):
    """Run simulation using supplied settings."""
    if os.path.exists(settings["interferometer/oskar_vis_filename"]):
        LOG.info("Skipping simulation, as output data already exist.")
        return
    LOG.info("Simulating %s", settings["interferometer/oskar_vis_filename"])
    # print(json.dumps(settings.to_dict(), indent=4))
    sim = oskar.Interferometer(settings=settings)
    sim.set_sky_model(sky)
    sim.set_telescope_model(tel)
    sim.run()


def make_sky_model(sky0, settings, radius_deg, flux_min_outer_jy):
    """Filter sky model.

    Includes all sources within the given radius, and sources above the
    specified flux outside this radius.
    """
    # Get pointing centre.
    ra0_deg = float(settings["observation/phase_centre_ra_deg"])
    dec0_deg = float(settings["observation/phase_centre_dec_deg"])

    # Create "inner" and "outer" sky models.
    sky_inner = sky0.create_copy()
    sky_outer = sky0.create_copy()
    sky_inner.filter_by_radius(0.0, radius_deg, ra0_deg, dec0_deg)
    sky_outer.filter_by_radius(radius_deg, 180.0, ra0_deg, dec0_deg)
    sky_outer.filter_by_flux(flux_min_outer_jy, 1e9)
    LOG.info("Number of sources in sky0: %d", sky0.num_sources)
    LOG.info("Number of sources in inner sky model: %d", sky_inner.num_sources)
    LOG.info(
        "Number of sources in outer sky model above %.3f Jy: %d",
        flux_min_outer_jy,
        sky_outer.num_sources,
    )
    sky_outer.append(sky_inner)
    LOG.info(
        "Number of sources in output sky model: %d", sky_outer.num_sources
    )
    return sky_outer


def make_plot(
    prefix, field_name, metric_key, results, axis_length, axis_bandwidth
):
    """Plot selected results."""
    # Axis setup.
    ax1 = plt.subplot(111)
    ax1.set_xscale("log")
    ax1.set_yscale("log")

    for bandwidth in axis_bandwidth:
        # Get data for line plot.
        X = numpy.array(axis_length)
        Y = numpy.zeros(X.shape)
        label = "%.0f MHz" % bandwidth
        for x, y in numpy.nditer([X, Y], op_flags=["readwrite"]):
            key = "length_%.0f_bandwidth_%.0f" % (x, bandwidth)
            if key in results:
                y[...] = results[key][metric_key]

        # Line plot.
        plt.plot(X, Y, marker="o", label=label)

    # Title and axis labels.
    metric_name = "[ UNKNOWN ]"
    if metric_key == "image_centre_rms":
        metric_name = "Central RMS [Jy/beam]"
    elif metric_key == "image_medianabs":
        metric_name = "MEDIAN(ABS(image)) [Jy/beam]"
    sky_model = prefix
    plt.title("%s for %s field (%s)" % (metric_name, field_name, sky_model))
    plt.xlabel("Observation length [sec]")
    plt.ylabel("%s" % metric_name)
    plt.gca().set_ylim([4e-6, 9e-4])
    plt.legend()
    plt.tight_layout()
    plt.savefig("%s_%s_%s.png" % (prefix, field_name, metric_key))
    plt.close("all")


def create_imagers_multi_channel(
    base_settings_img, settings_sim, prefix_field, length_sec, results
):
    # Set up the imagers.
    settings_img = oskar.SettingsTree("oskar_imager")
    settings_img.from_dict(base_settings_img)
    ra_deg = float(settings_sim["observation/phase_centre_ra_deg"])
    num_times = int(settings_sim["observation/num_time_steps"])
    time_inc = float(settings_sim["observation/length"]) / num_times
    num_channels = int(settings_sim["observation/num_channels"])
    freq_inc = float(settings_sim["observation/frequency_inc_hz"])
    freq_start = float(settings_sim["observation/start_frequency_hz"])
    channel_step = 5
    imagers = []
    time_start = Time(get_start_time(ra_deg, length_sec))
    time_min = time_start.mjd - (0.5 * time_inc) / 86400.0
    time_max = time_start.mjd + (0.5 * time_inc + length_sec) / 86400.0
    for channel_start in range(0, num_channels, channel_step):
        channel_end = channel_start + channel_step - 1
        if channel_end >= num_channels - 1:
            channel_end = num_channels - 1
        key = "length_%.0f_channels_%d-%d" % (
            length_sec,
            channel_start,
            channel_end,
        )
        if key in results:
            continue
        imager = oskar.Imager(settings=settings_img)
        freq_min = freq_start + freq_inc * (channel_start - 0.5)
        freq_max = freq_start + freq_inc * (channel_end + 0.5)
        LOG.info(
            '"%s" has frequency range %.5e to %.5e Hz', key, freq_min, freq_max
        )
        imager.channel_snapshots = True
        imager.grid_on_gpu = True
        imager.freq_min_hz = freq_min
        imager.freq_max_hz = freq_max
        imager.time_min_utc = time_min
        imager.time_max_utc = time_max
        imager.output_root = prefix_field + "_" + key
        imagers.append((key, imager))
    LOG.info(
        "Will make %d images of %d channels each.", len(imagers), channel_step
    )
    return imagers


def read_coords(out0_name, imagers):
    """Read all visibility blocks and update imagers with coordinates."""
    LOG.info("Reading headers...")
    (hdr1, handle1) = oskar.VisHeader.read(out0_name)
    block_diff = []
    block_diff.append(oskar.VisBlock.create_from_header(hdr1))
    block_diff.append(oskar.VisBlock.create_from_header(hdr1))

    for _, imager in imagers:
        imager.coords_only = True

    executor = concurrent.futures.ThreadPoolExecutor(16)
    for i_block in range(hdr1.num_blocks + 1):
        tasks_proc = []
        tasks_read = []
        if i_block > 0:
            # Process the previous block.
            for i, (key, imager) in enumerate(imagers):
                LOG.info(
                    'Updating imager %d/%d "%s" with block %d/%d...',
                    i + 1,
                    len(imagers),
                    key,
                    i_block,
                    hdr1.num_blocks,
                )
                i_buffer = (i_block - 1) % 2
                tasks_proc.append(
                    executor.submit(
                        imager.update_from_block, hdr1, block_diff[i_buffer]
                    )
                )
        if i_block < hdr1.num_blocks:
            # Read both visibility blocks.
            LOG.info("Reading blocks %d/%d", i_block + 1, hdr1.num_blocks)
            tasks_read.append(
                executor.submit(
                    block_diff[i_block % 2].read, hdr1, handle1, i_block
                )
            )

        # Ensure reads have finished.
        concurrent.futures.wait(tasks_read)

        # Ensure processing tasks have finished.
        concurrent.futures.wait(tasks_proc)

    del block_diff
    for _, imager in imagers:
        imager.coords_only = False


def read_data(out0_name, out_name, prefix_field, imagers):
    """Read all visibility blocks, take differences and update imagers."""
    LOG.info("Reading headers...")
    (hdr1, handle1) = oskar.VisHeader.read(out0_name)
    (hdr2, handle2) = oskar.VisHeader.read(out_name)
    block_diff = []
    block_diff.append(oskar.VisBlock.create_from_header(hdr1))
    block_diff.append(oskar.VisBlock.create_from_header(hdr1))
    block_temp = oskar.VisBlock.create_from_header(hdr2)

    executor = concurrent.futures.ThreadPoolExecutor(3)
    for i, (key, imager) in enumerate(imagers):
        imager.check_init()
        for i_block in range(hdr1.num_blocks + 1):
            tasks_proc = []
            tasks_read = []
            if i_block > 0:
                # Process the previous block.
                LOG.info(
                    'Updating imager %d/%d "%s" with block %d/%d...',
                    i + 1,
                    len(imagers),
                    key,
                    i_block,
                    hdr1.num_blocks,
                )
                i_buffer = (i_block - 1) % 2
                tasks_proc.append(
                    executor.submit(
                        imager.update_from_block, hdr1, block_diff[i_buffer]
                    )
                )
            if i_block < hdr1.num_blocks:
                # Read both visibility blocks.
                LOG.info("Reading blocks %d/%d", i_block + 1, hdr1.num_blocks)
                tasks_read.append(
                    executor.submit(
                        block_diff[i_block % 2].read, hdr1, handle1, i_block
                    )
                )
                tasks_read.append(
                    executor.submit(block_temp.read, hdr2, handle2, i_block)
                )

            # Ensure reads have finished.
            concurrent.futures.wait(tasks_read)
            block_diff[i_block % 2].cross_correlations()[
                ...
            ] -= block_temp.cross_correlations()

            # Ensure processing tasks have finished.
            concurrent.futures.wait(tasks_proc)
        output = imager.finalise()
        numpy.save(prefix_field + "_" + key + "_grid.npy", output["grids"])


def run_set_new(
    base_settings_sim,
    base_settings_img,
    sky0,
    prefix_field,
    field,
    length_sec,
    results,
):
    """Runs a set of simulations for one field."""
    # Load telescope model.
    settings_sim = oskar.SettingsTree("oskar_sim_interferometer")
    settings_sim.from_dict(base_settings_sim)
    tel = oskar.Telescope(settings=settings_sim)

    # Update settings for field.
    settings_dict = base_settings_sim.copy()
    settings_dict.update(field)
    settings_sim.from_dict(settings_dict)
    ra_deg = float(settings_sim["observation/phase_centre_ra_deg"])
    dec_deg = float(settings_sim["observation/phase_centre_dec_deg"])
    settings_sim["observation/start_time_utc"] = get_start_time(
        ra_deg, float(settings_sim["observation/length"])
    )
    tel.set_phase_centre(ra_deg, dec_deg)

    # Create the sky model.
    sky = make_sky_model(sky0, settings_sim, 20.0, 10.0)

    # Simulate the perfect case.
    tel.override_element_gains(1.0, 0.0)
    tel.override_element_cable_length_errors(0.0)
    out0_name = "%s_no_errors.vis" % (prefix_field)
    settings_sim["interferometer/oskar_vis_filename"] = out0_name
    make_vis_data(settings_sim, sky, tel)

    # Simulate the error case.
    # Phase error 1.82 degrees at 100 MHz.
    # Corresponds to 0.015156174265556 metres cable length error.
    gain_std_dB = 0.27
    gain_std = numpy.power(10.0, gain_std_dB / 20.0) - 1.0
    tel.override_element_gains(1.0, gain_std)
    tel.override_element_cable_length_errors(0.015156174265556)
    out_name = "%s_%.3f_dB.vis" % (prefix_field, gain_std_dB)
    settings_sim["interferometer/oskar_vis_filename"] = out_name
    make_vis_data(settings_sim, sky, tel)

    # Set up the imagers.
    imagers = create_imagers_multi_channel(
        base_settings_img, settings_sim, prefix_field, length_sec, results
    )

    # Iterate visibility blocks to update imagers.
    read_coords(out0_name, imagers)
    read_data(out0_name, out_name, prefix_field, imagers)


def run_set(
    base_settings_sim,
    base_settings_img,
    sky0,
    prefix_field,
    field,
    axis_time_sec,
    axis_bandwidth_MHz,
    results,
):
    """Runs a set of simulations for one field."""
    # Load telescope model.
    settings_sim = oskar.SettingsTree("oskar_sim_interferometer")
    settings_sim.from_dict(base_settings_sim)
    tel = oskar.Telescope(settings=settings_sim)

    # Update settings for field.
    settings_dict = base_settings_sim.copy()
    settings_dict.update(field)
    settings_sim.from_dict(settings_dict)
    ra_deg = float(settings_sim["observation/phase_centre_ra_deg"])
    dec_deg = float(settings_sim["observation/phase_centre_dec_deg"])
    settings_sim["observation/start_time_utc"] = get_start_time(
        ra_deg, float(settings_sim["observation/length"])
    )
    tel.set_phase_centre(ra_deg, dec_deg)

    # Create the sky model.
    sky = make_sky_model(sky0, settings_sim, 20.0, 10.0)

    # Simulate the perfect case.
    tel.override_element_gains(1.0, 0.0)
    tel.override_element_cable_length_errors(0.0)
    out0_name = "%s_no_errors.vis" % (prefix_field)
    settings_sim["interferometer/oskar_vis_filename"] = out0_name
    make_vis_data(settings_sim, sky, tel)

    # Simulate the error case.
    # Phase error 1.82 degrees at 100 MHz.
    # Corresponds to 0.015156174265556 metres cable length error.
    gain_std_dB = 0.27
    gain_std = numpy.power(10.0, gain_std_dB / 20.0) - 1.0
    tel.override_element_gains(1.0, gain_std)
    tel.override_element_cable_length_errors(0.015156174265556)
    out_name = "%s_%.3f_dB.vis" % (prefix_field, gain_std_dB)
    settings_sim["interferometer/oskar_vis_filename"] = out_name
    make_vis_data(settings_sim, sky, tel)

    # Set up the imagers.
    settings_img = oskar.SettingsTree("oskar_imager")
    settings_img.from_dict(base_settings_img)
    num_times = int(settings_sim["observation/num_time_steps"])
    time_inc = float(settings_sim["observation/length"]) / num_times
    num_channels = int(settings_sim["observation/num_channels"])
    freq_inc = float(settings_sim["observation/frequency_inc_hz"])
    freq_start = float(settings_sim["observation/start_frequency_hz"])
    freq_end = freq_start + num_channels * freq_inc
    freq_mid = (freq_end + freq_start) * 0.5
    imagers = []
    for length in axis_time_sec:
        time_start = Time(get_start_time(ra_deg, length))
        time_min = time_start.mjd - (0.5 * time_inc) / 86400.0
        time_max = time_start.mjd + (0.5 * time_inc + length) / 86400.0
        for bandwidth in axis_bandwidth_MHz:
            key = "length_%.0f_bandwidth_%.0f" % (length, bandwidth)
            if key in results:
                continue
            imager = oskar.Imager(settings=settings_img)
            freq_min = freq_mid - (bandwidth * 1e6 + freq_inc) * 0.5
            freq_max = freq_mid + (bandwidth * 1e6 + freq_inc) * 0.5
            LOG.info(
                '"%s" has frequency range %.5e to %.5e Hz',
                key,
                freq_min,
                freq_max,
            )
            imager.freq_min_hz = freq_min
            imager.freq_max_hz = freq_max
            imager.time_min_utc = time_min
            imager.time_max_utc = time_max
            imager.output_root = prefix_field + "_" + key
            imagers.append((key, imager))

    LOG.info("Will make %d images.", len(imagers))
    LOG.info("Reading headers...")
    (hdr1, handle1) = oskar.VisHeader.read(out0_name)
    (hdr2, handle2) = oskar.VisHeader.read(out_name)
    block1 = oskar.VisBlock.create_from_header(hdr1)
    block2 = oskar.VisBlock.create_from_header(hdr2)

    num_threads = 16
    executor = concurrent.futures.ThreadPoolExecutor(num_threads)
    if settings_img["image/algorithm"] == "W-projection":
        LOG.info("Reading coordinates...")
        for _, imager in imagers:
            imager.coords_only = True
        for i_block in range(hdr1.num_blocks):
            LOG.info("Block %d/%d", i_block + 1, hdr1.num_blocks)
            block1.read(hdr1, handle1, i_block)
            tasks = []
            for i, (key, imager) in enumerate(imagers):
                LOG.info(
                    'Updating imager %d/%d "%s"...', i + 1, len(imagers), key
                )
                tasks.append(
                    executor.submit(imager.update_from_block, hdr1, block1)
                )
            concurrent.futures.wait(tasks)
        for i, (key, imager) in enumerate(imagers):
            imager.coords_only = False
            imager.check_init()
            LOG.info(
                'Imager %d/%d "%s" using %d W-projection planes',
                i + 1,
                len(imagers),
                key,
                imager.num_w_planes,
            )

    LOG.info("Reading visibility data...")
    for i_block in range(hdr1.num_blocks):
        LOG.info("Block %d/%d", i_block + 1, hdr1.num_blocks)
        a = executor.submit(block1.read, hdr1, handle1, i_block)
        b = executor.submit(block2.read, hdr2, handle2, i_block)
        concurrent.futures.wait((a, b))
        block1.cross_correlations()[...] -= block2.cross_correlations()
        tasks = []
        for _, imager in imagers:
            tasks.append(
                executor.submit(imager.update_from_block, hdr1, block1)
            )
        concurrent.futures.wait(tasks)
    executor.shutdown()
    del handle1, handle2, hdr1, hdr2, block1, block2

    LOG.info("Finalising %d images...", len(imagers))
    for i, (key, imager) in enumerate(imagers):
        LOG.info("Finalising image %d/%d...", i + 1, len(imagers))
        output = imager.finalise(return_images=1)
        image = output["images"][0]

        LOG.info("Generating image statistics")
        image_size = imager.image_size
        box_size = int(0.1 * image_size)
        centre = image[
            (image_size - box_size) // 2 : (image_size + box_size) // 2,
            (image_size - box_size) // 2 : (image_size + box_size) // 2,
        ]
        results[key] = {
            "image_medianabs": numpy.median(numpy.abs(image)),
            "image_mean": numpy.mean(image),
            "image_std": numpy.std(image),
            "image_rms": numpy.sqrt(numpy.mean(image**2)),
            "image_centre_mean": numpy.mean(centre),
            "image_centre_std": numpy.std(centre),
            "image_centre_rms": numpy.sqrt(numpy.mean(centre**2)),
        }
        del image, output


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    base_settings_sim = {
        "simulator": {
            "double_precision": True,
            "use_gpus": True,
            "max_sources_per_chunk": 23000,
        },
        "observation": {
            "length": 21600.0,
            "start_frequency_hz": 132e6,
            "frequency_inc_hz": 100e3,
            "num_channels": 160,  # Should be 160 for 16 MHz bandwidth.
            "num_time_steps": 360,  # Should be 360 for 60-second intervals.
        },
        "telescope": {
            "input_directory": "SKA1-LOW_SKO-0000422_Rev3_38m.tm",
            "pol_mode": "Scalar",
        },
        "interferometer": {
            "channel_bandwidth_hz": 100e3,
            "time_average_sec": 1.0,
            "max_time_samples_per_block": 4,
            "ignore_w_components": False,
        },
    }
    base_settings_img = {
        "image": {
            "double_precision": base_settings_sim["simulator"][
                "double_precision"
            ],
            "size": 8192,
            "fov_deg": 5.0,
            "fft/use_gpu": True,
            "algorithm": "W-projection",
        }
    }

    # Define axes of parameter space.
    axis_time_sec = [60, 420, 900, 1800, 3600, 7200, 14400, 21600]
    axis_bandwidth_MHz = [1, 3, 10]
    fields = {
        "EoR0": {
            "observation/phase_centre_ra_deg": 0.0,
            "observation/phase_centre_dec_deg": -27.0,
        },
        "EoR1": {
            "observation/phase_centre_ra_deg": 60.0,
            "observation/phase_centre_dec_deg": -30.0,
        },
        "EoR2": {
            "observation/phase_centre_ra_deg": 170.0,
            "observation/phase_centre_dec_deg": -10.0,
        },
    }

    plot_only = False
    prefix = "GLEAM_A-team"

    # Run simulations.
    if not plot_only:
        sky0 = oskar.Sky()
        if "GLEAM" in prefix:
            # Load GLEAM catalogue from FITS binary table.
            hdulist = fits.open("GLEAM_EGC.fits")
            # pylint: disable=no-member
            cols = hdulist[1].data[0].array
            data = numpy.column_stack(
                (cols["RAJ2000"], cols["DEJ2000"], cols["peak_flux_wide"])
            )
            data = data[data[:, 2].argsort()[::-1]]
            sky_gleam = oskar.Sky.from_array(data)
            sky0.append(sky_gleam)
        if "A-team" in prefix:
            sky_bright = oskar.Sky.from_array(bright_sources())
            sky0.append(sky_bright)

    # Iterate over fields.
    for field_name, field in fields.items():
        # Load result set, if it exists.
        prefix_field = prefix + "_" + field_name
        results = {}
        json_file = prefix_field + "_results.json"
        if os.path.exists(json_file):
            with open(json_file, "r") as input_file:
                results = json.load(input_file)

        if not plot_only:
            run_set_new(
                base_settings_sim,
                base_settings_img,
                sky0,
                prefix_field,
                field,
                14400,
                results,
            )

        # Save result set.
        with open(json_file, "w") as output_file:
            json.dump(results, output_file, indent=4)

        # Generate plot for the field.
        make_plot(
            prefix,
            field_name,
            "image_centre_rms",
            results,
            axis_time_sec,
            axis_bandwidth_MHz,
        )
        make_plot(
            prefix,
            field_name,
            "image_medianabs",
            results,
            axis_time_sec,
            axis_bandwidth_MHz,
        )


if __name__ == "__main__":
    main()
