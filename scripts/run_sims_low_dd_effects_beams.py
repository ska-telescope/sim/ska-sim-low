#!/usr/bin/env python3
"""
Run simulations for SKA1-LOW direction-dependent effects.
https://confluence.skatelescope.org/display/SE/Simulations+with+Direction-Dependent+Effects
https://jira.skatelescope.org/browse/SP-1056
"""

import copy
import subprocess
import oskar

from .utils import get_start_time


def main():
    # Define common base settings.
    app = "oskar_sim_beam_pattern"
    settings_path = "_temp_settings.ini"
    tel_dir = "./"
    tel_model = "SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm"
    common_settings = {
        "simulator/max_sources_per_chunk": 4096,
        "observation/start_frequency_hz": 125e6,
        "observation/frequency_inc_hz": 50e6,  # Should be 5 MHz.
        "observation/num_channels": 1,  # Should be 11 (use fewer for testing)
        "telescope/input_directory": tel_dir + tel_model,
        "beam_pattern/coordinate_frame": "Equatorial",
        "beam_pattern/beam_image/size": 256,
        "beam_pattern/beam_image/fov_deg": 40.0,
        # For individual station beam amplitudes:
        "beam_pattern/all_stations": False,
        "beam_pattern/station_ids": "0",
        "beam_pattern/output/separate_time_and_channel": True,
        "beam_pattern/output/average_time_and_channel": False,
        "beam_pattern/output/average_single_axis": "None",
        "beam_pattern/station_outputs/fits_image/amp": True,
        "beam_pattern/station_outputs/fits_image/phase": False,
        "beam_pattern/station_outputs/fits_image/auto_power_real": True,
        # For averaged cross-power station beams:
        # "beam_pattern/all_stations": True,
        # "beam_pattern/output/separate_time_and_channel": False,
        # "beam_pattern/output/average_time_and_channel": False,
        # "beam_pattern/output/average_single_axis": "Time",
        # "beam_pattern/telescope_outputs/fits_image/cross_power_amp": True,
        # "beam_pattern/telescope_outputs/fits_image/cross_power_real": True,
        # "beam_pattern/telescope_outputs/fits_image/cross_power_imag": True
    }

    # Define observations.
    observations = {
        # "short": {
        #    "observation/length": 5*60,
        #    "observation/num_time_steps": 300,
        #    "telescope/external_tec_screen/input_fits_file": "screen_short_300_1.0.fits"
        # },
        # "medium": {
        #    "observation/length": 30*60,
        #    "observation/num_time_steps": 300,
        #    "telescope/external_tec_screen/input_fits_file": "screen_medium_300_6.0.fits"
        # },
        "long": {
            "observation/length": 4 * 60 * 60,
            "observation/num_time_steps": 240,
            "telescope/external_tec_screen/input_fits_file": "screen_long_240_60.0.fits",
        }
    }

    # Define fields.
    fields = {
        "EoR0": {
            "observation/phase_centre_ra_deg": 0.0,
            "observation/phase_centre_dec_deg": -27.0,
        },
        # "EoR1": {
        #    "observation/phase_centre_ra_deg": 60.0,
        #    "observation/phase_centre_dec_deg": -30.0
        # },
        # "EoR2": {
        #    "observation/phase_centre_ra_deg": 170.0,
        #    "observation/phase_centre_dec_deg": -10.0
        # }
    }

    # Define ionosphere settings.
    ionosphere = {
        # "ionosphere_on": {
        #    "telescope/ionosphere_screen_type": "External"
        # },
        "ionosphere_off": {"telescope/ionosphere_screen_type": "None"}
    }

    # Loop over observations.
    for obs_name, obs_params in observations.items():
        # Copy the base settings dictionary.
        current_settings = copy.deepcopy(common_settings)

        # Update current settings with observation parameters.
        current_settings.update(obs_params)

        # Loop over fields.
        for field_name, field_params in fields.items():
            # Update current settings with field parameters.
            current_settings.update(field_params)

            # Update current settings with start time.
            ra0_deg = current_settings["observation/phase_centre_ra_deg"]
            length_sec = current_settings["observation/length"]
            start_time = get_start_time(ra0_deg, length_sec)
            current_settings["observation/start_time_utc"] = start_time

            # Loop over ionospheric screen on/off.
            for iono_name, iono_params in ionosphere.items():
                # Update current settings with ionosphere parameters.
                current_settings.update(iono_params)

                # Update output filename based on current parameters.
                root_path = "SKA_LOW_SIM_BEAM"
                root_path += "_" + obs_name
                root_path += "_" + field_name
                root_path += "_" + iono_name

                # Create the settings file.
                open(settings_path, "w").close()
                settings = oskar.SettingsTree(app, settings_path)
                settings.from_dict(current_settings)
                settings["beam_pattern/root_path"] = root_path

                # Run the app with the settings file.
                subprocess.call([app, settings_path])


if __name__ == "__main__":
    main()
