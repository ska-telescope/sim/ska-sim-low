import sys

from astropy.io import fits
import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy


def plot_panel(ax, image, title, cmap):
    """Plots a single panel."""
    im = ax.imshow(numpy.squeeze(image), cmap=cmap)
    plt.colorbar(im, format="%.2e")
    plt.tick_params(
        labelcolor="none", top="off", bottom="off", left="off", right="off"
    )
    ax.set_xticks([])
    ax.set_yticks([])
    ax.invert_yaxis()
    ax.set_title(title)
    ax.axis("equal")


def main():
    """Main function."""
    # Load 4 image files from the first four command line arguments.
    images = []
    for i in range(len(sys.argv) - 3):
        images.append(fits.getdata(sys.argv[i + 1]))
    out_name = sys.argv[-1]
    use_stokes = sys.argv[-2][0].upper() == "S"

    # Set titles and colour maps to use.
    cmap = ""
    titles = []
    if use_stokes:
        # cmap = 'plasma'
        cmap = "Blues_r"
        titles = ["Stokes I", "Stokes Q", "Stokes U", "Stokes V"]
    else:
        # cmap = 'jet'
        cmap = "CMRmap"
        titles = ["XX", "XY", "YX", "YY"]

    # Sub-plots, one for each Stokes parameter.
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(221, frameon=False)
    plot_panel(ax, images[0], titles[0], cmap)
    ax = fig.add_subplot(222, frameon=False)
    plot_panel(ax, images[1], titles[1], cmap)
    ax = fig.add_subplot(223, frameon=False)
    plot_panel(ax, images[2], titles[2], cmap)
    ax = fig.add_subplot(224, frameon=False)
    plot_panel(ax, images[3], titles[3], cmap)

    # Common axis labels.
    ax = fig.add_subplot(111, frameon=False)
    plt.tick_params(
        labelcolor="none", top="off", bottom="off", left="off", right="off"
    )
    # Hide ticks for this new axis.
    ax.set_xticks([])
    ax.set_yticks([])

    # Save and close.
    fig.tight_layout()
    plt.savefig("%s.png" % (out_name))
    plt.close("all")


if __name__ == "__main__":
    main()
