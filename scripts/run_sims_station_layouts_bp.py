#!/usr/bin/env python3

"""Script to run LOW station layout simulations at spot frequencies.

Generate cross-power beam patterns.
"""

import glob
import logging
import subprocess
import sys

import oskar

from .utils import get_start_time

LOG = logging.getLogger()


def run_set(prefix, base_settings, fields, axis_stations, axis_freq):
    """Runs a set of simulations."""

    settings_file = "sim_beam_pattern.ini"
    open(settings_file, "a").close()  # Create file if it doesn't exist.

    # Iterate over fields.
    for field_name, field in fields.items():
        # Update settings for field.
        settings = oskar.SettingsTree("oskar_sim_beam_pattern", settings_file)
        settings_dict = base_settings.copy()
        settings_dict.update(field)
        settings.from_dict(settings_dict)
        ra_deg = float(settings["observation/phase_centre_ra_deg"])
        length_sec = float(settings["observation/length"])
        settings["observation/start_time_utc"] = get_start_time(
            ra_deg, length_sec
        )

        # Iterate over frequencies.
        for freq_MHz in axis_freq:
            settings["observation/start_frequency_hz"] = str(freq_MHz * 1e6)

            # Simulate telescope models with fewer station types.
            for stations in axis_stations:
                settings["telescope/input_directory"] = (
                    "SKA1-LOW_SKO-0000422_Rev3_SKALA4_%03d_different_38m_stations.tm"
                    % stations
                )
                root_path = "%s_%03d_different_stations_%s_%.0f_MHz" % (
                    prefix,
                    stations,
                    field_name,
                    freq_MHz,
                )
                test_path = ""
                if (
                    settings["beam_pattern/output/separate_time_and_channel"]
                    == "True"
                ):
                    test_path = root_path + "*TIME_SEP*"
                else:
                    test_path = root_path + "*TIME_AVG*"
                if not glob.glob(test_path):
                    settings["beam_pattern/root_path"] = root_path
                    LOG.info('Generating "%s"', root_path)
                    subprocess.call(["oskar_sim_beam_pattern", settings_file])
                else:
                    LOG.info("Skipping %s: already exists", root_path)


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    base_settings = {
        "observation": {
            "length": 1.0,
            "num_time_steps": 1,
            # "length": 14400.0,
            # "num_time_steps": 240,
            # "num_time_steps": 24
        },
        "beam_pattern": {
            "all_stations": False,
            # "all_stations": True,
            "output/separate_time_and_channel": True,
            # "output/separate_time_and_channel": False,
            "output/average_single_axis": "None",
            # "output/average_single_axis": "Time",
            "coordinate_frame": "Horizon",
            "beam_image/size": 512,
            "beam_image/fov_deg": 180.0,
            # "telescope_outputs/fits_image/cross_power_amp": True,
            "station_outputs/fits_image/auto_power": True,
        },
    }

    # Define axes of parameter space.
    fields = {
        "EoR0": {
            "observation/phase_centre_ra_deg": 0.0,
            "observation/phase_centre_dec_deg": -27.0,
        }
    }
    # axis_freq = [50, 110, 230, 320]
    # axis_stations = [1, 2, 4, 8, 16, 32, 256, 512]
    # run_set("cross_power_beam", base_settings,
    #        fields, axis_stations, axis_freq)
    axis_freq = [110, 230]
    # axis_stations = [1, 4, 16, 512]
    axis_stations = [1]
    run_set(
        "auto_power_beam_snapshot",
        base_settings,
        fields,
        axis_stations,
        axis_freq,
    )


if __name__ == "__main__":
    main()
