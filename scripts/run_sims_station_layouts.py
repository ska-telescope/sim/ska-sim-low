#!/usr/bin/env python3

"""Script to run LOW station layout simulations at spot frequencies."""

import concurrent.futures
import glob
import json
import logging
import os
import sys

from astropy.io import fits
import matplotlib

matplotlib.use("Agg")
# pylint: disable=wrong-import-position
import matplotlib.pyplot as plt
import numpy
import oskar

from .utils import get_start_time

LOG = logging.getLogger()


def bright_sources():
    """Returns a list of bright A-team sources."""
    # Sgr A: guesstimates only!
    # For A: data from the Molonglo Southern 4 Jy sample (VizieR).
    # Others from GLEAM reference paper, Hurley-Walker et al. (2017), Table 2.
    return numpy.array(
        (
            [266.41683, -29.00781, 2000, 0, 0, 0, 0, 0, 0, 3600, 3600, 0],
            [
                50.67375,
                -37.20833,
                528,
                0,
                0,
                0,
                178e6,
                -0.51,
                0,
                0,
                0,
                0,
            ],  # For
            [
                201.36667,
                -43.01917,
                1370,
                0,
                0,
                0,
                200e6,
                -0.50,
                0,
                0,
                0,
                0,
            ],  # Cen
            [
                139.52500,
                -12.09556,
                280,
                0,
                0,
                0,
                200e6,
                -0.96,
                0,
                0,
                0,
                0,
            ],  # Hyd
            [
                79.95833,
                -45.77889,
                390,
                0,
                0,
                0,
                200e6,
                -0.99,
                0,
                0,
                0,
                0,
            ],  # Pic
            [
                252.78333,
                4.99250,
                377,
                0,
                0,
                0,
                200e6,
                -1.07,
                0,
                0,
                0,
                0,
            ],  # Her
            [
                187.70417,
                12.39111,
                861,
                0,
                0,
                0,
                200e6,
                -0.86,
                0,
                0,
                0,
                0,
            ],  # Vir
            [
                83.63333,
                22.01444,
                1340,
                0,
                0,
                0,
                200e6,
                -0.22,
                0,
                0,
                0,
                0,
            ],  # Tau
            [
                299.86667,
                40.73389,
                7920,
                0,
                0,
                0,
                200e6,
                -0.78,
                0,
                0,
                0,
                0,
            ],  # Cyg
            [
                350.86667,
                58.81167,
                11900,
                0,
                0,
                0,
                200e6,
                -0.41,
                0,
                0,
                0,
                0,
            ],  # Cas
        )
    )


def make_vis_data(settings, sky):
    """Run simulation using supplied settings."""
    if os.path.exists(settings["interferometer/oskar_vis_filename"]):
        LOG.info("Skipping simulation, as output data already exist.")
        return
    LOG.info("Simulating %s", settings["interferometer/oskar_vis_filename"])
    # print(json.dumps(settings.to_dict(), indent=4))
    sim = oskar.Interferometer(settings=settings)
    sim.set_sky_model(sky)
    sim.run()


def make_sky_model(sky0, settings, radius_deg, flux_min_outer_jy):
    """Filter sky model.

    Includes all sources within the given radius, and sources above the
    specified flux outside this radius.
    """
    # Get pointing centre.
    ra0_deg = float(settings["observation/phase_centre_ra_deg"])
    dec0_deg = float(settings["observation/phase_centre_dec_deg"])

    # Create "inner" and "outer" sky models.
    sky_inner = sky0.create_copy()
    sky_outer = sky0.create_copy()
    sky_inner.filter_by_radius(0.0, radius_deg, ra0_deg, dec0_deg)
    sky_outer.filter_by_radius(radius_deg, 180.0, ra0_deg, dec0_deg)
    sky_outer.filter_by_flux(flux_min_outer_jy, 1e9)
    LOG.info("Number of sources in sky0: %d", sky0.num_sources)
    LOG.info("Number of sources in inner sky model: %d", sky_inner.num_sources)
    LOG.info(
        "Number of sources in outer sky model above %.3f Jy: %d",
        flux_min_outer_jy,
        sky_outer.num_sources,
    )
    sky_outer.append(sky_inner)
    LOG.info(
        "Number of sources in output sky model: %d", sky_outer.num_sources
    )
    return sky_outer


def make_diff_image_stats(
    filename1, filename2, use_w_projection, out_image_root=None
):
    """Make an image of the difference between two visibility data sets.

    This function assumes that the observation parameters for both data sets
    are identical. (It will fail horribly otherwise!)
    """
    # Set up an imager.
    (hdr1, handle1) = oskar.VisHeader.read(filename1)
    (hdr2, handle2) = oskar.VisHeader.read(filename2)
    frequency_hz = hdr1.freq_start_hz
    fov_ref_frequency_hz = 140e6
    fov_ref_deg = 5.0
    fov_deg = fov_ref_deg * (fov_ref_frequency_hz / frequency_hz)
    imager = oskar.Imager(precision="double")
    imager.set(
        fov_deg=fov_deg, image_size=8192, fft_on_gpu=True, grid_on_gpu=True
    )
    if out_image_root is not None:
        imager.output_root = out_image_root

    LOG.info("Imaging differences between '%s' and '%s'", filename1, filename2)
    block1 = oskar.VisBlock.create_from_header(hdr1)
    block2 = oskar.VisBlock.create_from_header(hdr2)
    if hdr1.num_blocks != hdr2.num_blocks:
        raise RuntimeError(
            "'%s' and '%s' have different dimensions!" % (filename1, filename2)
        )
    if use_w_projection:
        imager.set(algorithm="W-projection")
        imager.coords_only = True
        for i_block in range(hdr1.num_blocks):
            block1.read(hdr1, handle1, i_block)
            imager.update_from_block(hdr1, block1)
        imager.coords_only = False
        imager.check_init()
        LOG.info("Using %d W-planes", imager.num_w_planes)
    executor = concurrent.futures.ThreadPoolExecutor(2)
    for i_block in range(hdr1.num_blocks):
        tasks_read = []
        tasks_read.append(executor.submit(block1.read, hdr1, handle1, i_block))
        tasks_read.append(executor.submit(block2.read, hdr2, handle2, i_block))
        concurrent.futures.wait(tasks_read)
        block1.cross_correlations()[...] -= block2.cross_correlations()
        imager.update_from_block(hdr1, block1)
    del handle1, handle2, hdr1, hdr2, block1, block2

    # Finalise image and return it to Python.
    output = imager.finalise(return_images=1)
    image = output["images"][0]

    LOG.info("Generating image statistics")
    image_size = imager.image_size
    box_size = int(0.1 * image_size)
    centre = image[
        (image_size - box_size) // 2 : (image_size + box_size) // 2,
        (image_size - box_size) // 2 : (image_size + box_size) // 2,
    ]
    del imager
    return {
        "image_medianabs": numpy.median(numpy.abs(image)),
        "image_mean": numpy.mean(image),
        "image_std": numpy.std(image),
        "image_rms": numpy.sqrt(numpy.mean(image**2)),
        "image_centre_mean": numpy.mean(centre),
        "image_centre_std": numpy.std(centre),
        "image_centre_rms": numpy.sqrt(numpy.mean(centre**2)),
    }


def make_plots(prefix, metric_key, results, fields, axis_stations, axis_freq):
    """Plot selected results."""
    # Plot each frequency separately.
    for freq in axis_freq:
        for field_name in fields.keys():
            x = numpy.zeros(len(axis_stations))
            y = numpy.zeros(len(axis_stations))
            for i, stations in enumerate(axis_stations):
                key = "%s_%s_%03d_stations_%d_MHz" % (
                    prefix,
                    field_name,
                    stations,
                    freq,
                )
                if key in results:
                    x[i] = stations
                    y[i] = numpy.log10(results[key][metric_key])
            plt.plot(x, y, label=field_name + " field")

        # Title and axis labels.
        metric_name = "[ UNKNOWN ]"
        if metric_key == "image_centre_rms":
            metric_name = "Central RMS [Jy/beam]"
        elif metric_key == "image_medianabs":
            metric_name = "MEDIAN(ABS(image)) [Jy/beam]"
        sky_model = "GLEAM"
        if "A-team" in prefix:
            sky_model = sky_model + " + A-team"
        plt.title("%s @ %d MHz (%s)" % (metric_name, freq, sky_model))
        plt.xlabel("Number of different stations")
        plt.ylabel("log10(%s)" % metric_name)
        plt.gca().set_xscale("log")
        plt.legend()
        plt.savefig("%s_%d_MHz_%s.png" % (prefix, freq, metric_key))
        plt.close("all")


def run_single(
    prefix_field, settings, sky, stations, freq_MHz, out0_name, results
):
    """Run a single simulation and generate image statistics for it."""
    out = "%s_%03d_stations_%d_MHz" % (prefix_field, stations, freq_MHz)
    if out in results:
        LOG.info("Using cached results for '%s'", out)
        return
    out_name = out + ".vis"
    settings["interferometer/oskar_vis_filename"] = out_name
    make_vis_data(settings, sky)
    out_image_root = out
    use_w_projection = True
    if str(settings["interferometer/ignore_w_components"]).lower() == "true":
        use_w_projection = False
    results[out] = make_diff_image_stats(
        out0_name, out_name, use_w_projection, out_image_root
    )
    # os.remove(out_name)  # Delete visibility data to save space.


def run_set(prefix, base_settings, fields, axis_stations, axis_freq, results):
    """Runs a set of simulations."""

    # Load base sky model
    sky0 = oskar.Sky()
    if "GLEAM" in prefix:
        # Load GLEAM catalogue from FITS binary table.
        hdulist = fits.open("GLEAM_EGC.fits")
        # pylint: disable=no-member
        cols = hdulist[1].data[0].array
        data = numpy.column_stack(
            (cols["RAJ2000"], cols["DEJ2000"], cols["peak_flux_wide"])
        )
        data = data[data[:, 2].argsort()[::-1]]
        sky_gleam = oskar.Sky.from_array(data)
        sky0.append(sky_gleam)
    if "A-team" in prefix:
        sky_bright = oskar.Sky.from_array(bright_sources())
        sky0.append(sky_bright)

    # Iterate over fields.
    for field_name, field in fields.items():
        prefix_field = prefix + "_" + field_name
        settings = oskar.SettingsTree("oskar_sim_interferometer")

        # Iterate over frequencies.
        for freq_MHz in axis_freq:
            # Update settings for field.
            settings_dict = base_settings.copy()
            settings_dict.update(field)
            settings.from_dict(settings_dict)
            ra_deg = float(settings["observation/phase_centre_ra_deg"])
            length_sec = float(settings["observation/length"])
            settings["observation/start_frequency_hz"] = str(freq_MHz * 1e6)
            settings["observation/start_time_utc"] = get_start_time(
                ra_deg, length_sec
            )

            # Create the sky model.
            sky = make_sky_model(sky0, settings, 20.0, 10.0)
            settings["interferometer/ignore_w_components"] = True
            if "A-team" in prefix:
                settings["interferometer/ignore_w_components"] = False

            # Simulate the fully randomised (reference) case.
            settings["telescope/input_directory"] = (
                "SKA1-LOW_SKO-0000422_Rev3_SKALA4_REF_512_different_38m_stations.tm"
            )
            out0_name = "%s_REF_512_stations_%d_MHz.vis" % (
                prefix_field,
                freq_MHz,
            )
            settings["interferometer/oskar_vis_filename"] = out0_name
            make_vis_data(settings, sky)

            # Simulate telescope models with fewer station types.
            for stations in axis_stations:
                settings["telescope/input_directory"] = (
                    "SKA1-LOW_SKO-0000422_Rev3_SKALA4_%03d_different_38m_stations.tm"
                    % stations
                )
                run_single(
                    prefix_field,
                    settings,
                    sky,
                    stations,
                    freq_MHz,
                    out0_name,
                    results,
                )

                # Update saved result set.
                with open("results.json", "w") as output_file:
                    json.dump(results, output_file, indent=4)


def main():
    """Main function."""
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    LOG.addHandler(handler)
    LOG.setLevel(logging.INFO)

    # Define common settings.
    base_settings = {
        "simulator/max_sources_per_chunk": 23000,
        "observation/length": 14400.0,
        "observation/num_time_steps": 240,
        "interferometer/channel_bandwidth_hz": 100e3,
        "interferometer/time_average_sec": 1.0,
        "interferometer/max_time_samples_per_block": 4,
    }

    # Define axes of parameter space.
    fields = {
        "EoR0": {
            "observation/phase_centre_ra_deg": 0.0,
            "observation/phase_centre_dec_deg": -27.0,
        },
        "EoR1": {
            "observation/phase_centre_ra_deg": 60.0,
            "observation/phase_centre_dec_deg": -30.0,
        },
        "EoR2": {
            "observation/phase_centre_ra_deg": 170.0,
            "observation/phase_centre_dec_deg": -10.0,
        },
    }
    axis_freq = [50, 110, 230, 320]
    axis_stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 16, 32, 64, 128, 256, 512]

    # Load any saved result sets.
    results = {}
    for json_file in glob.glob("*results.json"):
        with open(json_file, "r") as input_file:
            results.update(json.load(input_file))

    # GLEAM + A-team sky model simulations.
    prefix = "GLEAM_A-team"
    run_set(prefix, base_settings, fields, axis_stations, axis_freq, results)

    # Generate plots.
    make_plots(
        prefix, "image_centre_rms", results, fields, axis_stations, axis_freq
    )
    make_plots(
        prefix, "image_medianabs", results, fields, axis_stations, axis_freq
    )


if __name__ == "__main__":
    main()
