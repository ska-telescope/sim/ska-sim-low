.. _documentation_master:

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

SKA-Low simulations
===================

This repository collects scripts used for various SKA-Low simulations.

Local development
-----------------

.. toctree::
  :maxdepth: 2

  installation/index

Direction-dependent effects
---------------------------

Many of the simulations of direction-dependent effects on the sky make
use of the `OSKAR <https://developer.skatelescope.org/projects/sim-tools/>`_
simulator. Scripts for these simulations are written specifically for each
investigation.

A "cookbook" is provided here to help describe the examples that use OSKAR,
and to provide a starting point for writing new scripts.

.. toctree::
  :maxdepth: 2

  oskar_cookbook/index

Low-level RFI
-------------

These provide scripts to simulate data containing propagated radio frequency
interference (RFI) from terrestrial antennas.

.. toctree::
  :maxdepth: 2

  lowlevel-rfi/index
