.. _transmitters:

Terrestrial transmitter data
=============================

The example RFI simulations focus on digital television (DTV) transmitters and specifically a single antenna located
in Perth, AU. However, there are a significant number of DTV as well as other terrestrial transmitters operating in Western
Australia. Information has been gathered on the current broadcasting transmitters in Western Australia from several
sources including the Australian Communications and Media Authority (ACMA)
`license register <https://web.acma.gov.au/rrl/register_search.main_page>`_,
`Oz Digital TV <https://ozdigitaltv.com/>`_ and `TX Australia <https://www.txaustralia.com.au/>`_.
The ACMA is extensive and, if desired, a full list of transmitters is available from the link above (note the full file
size will be several GBs). A CSV copy of the simplified ACMA information for only the Western Australia DTV antennas is
included in the **data/transmitters** directory (``Filtered_DTV_list_plain.csv``) which is usable with theses simulations.
Other example CSV files are also present in the directory, containing only a handful of transmitters, which can be used for testing.
Alternatively, filtering the larger csv file for specific areas or frequencies can provide larger sub-groups to simulate.

The main characteristics of the DTV transmitters have been taken primarily from a copy of the current license register
from ACMA, which provides power output, direction of polarisation (H, horizontal or V, vertical) as well as antenna
type (e.g. omnidirectional or directional). A number of the transmitter types have beam pattern information available alongside the license
information, which contains antenna gains for a number of azimuthal directions. Where no further information
is available, the transmitter is assumed to be represented by a fixed-link antenna as described in F.699-7. The map below shows the
locations of the SKA-Low stations and the DTV transmitters in Western Australia that transmit in the 50 - 350 MHz range.
The map below shows the locations of the SKA-Low stations and the relevant transmitters in Western Australia that transmit in the 50 - 350 MHz range.
DTV and DR (digital radio) transmitters are shown by default. FM transmitters can also be shown by opening the map.
The colour range displays groups of transmitters based on their emitting power.


.. raw:: html

    <iframe src="https://www.google.com/maps/d/u/0/embed?mid=12WaHZkOIjhA_7_0ylHIyPSXIEK01dsC9" width="640" height="480"></iframe>




.. To create the map above:
.. Need google account with maps functionality - chose 'Creat a map' option
.. The three transmitter lists were taken from the ACMA BroadcastTransmitterExcel_full.xls file (comes zipped)
.. Created filtered versions of each list (AM, FM, DTV, DR). N.B. No relevant transmitters for AM.
.. Filters used (in libreoffice) were, state contains 'WA' AND FREQ <= 350 AND FREQ >= 50 (should be in MHz).
.. N.B. this will miss any potential harmonics from transmitters at frequencies outside of this range.
.. Loaded each into the map via import option on left-hand menu using longitude and latitude columns to position and Callsign as name.
.. Customised the display using the options to set a symbol as well as apply a colour-range based on the Maximum ERP (power) column.
.. To add to the documentation, use the three dots and the 'Embed' option to get the relevant iframe command as above.