.. _rfi_interface:

Radio Frequency Interference (RFI) interface
============================================

Command line tool to standardize the output of RFI attenuation scripts into a format,
which can be processed by visibility simulation pipelines. The agreed standard format
is HDF5.

The following RFI sources are supported::

    - TV Transmitter


Usage
-----

:ref:`rfi_interface_cli`


Input
-----

The Interface is fully compatible with the Propagation attenuation scripts described in :ref:`pycraf_scripts`.
At the moment, the following input arguments can be directly modified from the interface:

.. code-block:: none

    --transmitter_file   path to the CSV file containing the TV transmitter information.
    --n_time_chunks      number of time samples to run the simulation for; default = 1. Optional

    --frequency_range    start and end of frequency range in MHz (specified as <freq_start> and <freq_end>). Optional.
    --n_channels         number of channels to run the simulation for (specified as <n_channels>). Optional.

Note, if any of ``<freq_start>, <freq_end>``, or ``<n_channels>`` is supplied, the other two also needs to
be part of the input arguments.

Output
------

The RFI signal data are saved in an HDF5 file with the following structure:

    - Source ID, string, dimensions: (nsources)
    - Source type, string, dimensions: (nsources)
    - Time samples, string, dimensions: (ntimes)
    - Frequency channels, FP64, dimensions: (nfreqs), units: [Hz]
    - SKA station ID, string, dimensions: (nstations)
    - Apparent source coordinates in antenna rest frame, FP64, dimensions: (nsources, ntimes, nants, 3)
      These are [azimuth, elevation, distance], units: [degree, degree, m]
    - Transmitter power as received by an isotropic antenna, FP64, dimensions: (nsources, ntimes, nants, nfreqs)
      This does not include the antenna beam pattern which will be applied in the visibility simulation pipeline.
      units: [dB]


Class description
-----------------

DataCube
^^^^^^^^

.. autoclass:: rfi.rfi_interface.rfi_data_cube.DataCube
   :members:
   :undoc-members:
