.. _lowlevel_rfi:

Low-level RFI simulations
*************************

These simulations are designed to provide simulated data containing received signal from known Australian
terrestrial transmitters. The aim of providing these simulations is to enable testing of RFI-mitigation techniques and
specifically to understand the level of low-level RFI (radio frequency interference) likely to be present for
SKA Low observations and the limitations of the standard mitigation software. This has particular relevance for
the Epoch of Re-ionisation (EoR) Key Science Project and was in part motivated by the presence of
such RFI in `MWA EoR experiments <https://www.mwatelescope.org/science/epoch-of-reionization-eor>`_.

There are several scripts provided in the **ska-sim-low/rfi** directory. For the main end-to-end simulation providing output
images and measurement sets the bash script :code:`rfi_sim.sh` should be used. This runs three python scripts
which in combination will take input transmitter characteristics, calculate the propagation attenuation, the directional
beam gain and then simulate observations outputting FITS images or measurement set files as required. Alternatively these
scripts can be run individually via the command line. There is an additional script :code:`rfi/power_spectrum.py`,
which is not part of the main simulation but can optionally be used to calculate a power spectrum from the FITS images.

For local environments, we recommend running :code:`rfi_sim_test.sh`, which is a version of the original bash script that
is scaled to run on a laptop and executes the same three python scripts.

These simulations rely on `Pycraf <https://bwinkel.github.io/pycraf/index.html>`_,
`OSKAR <https://github.com/OxfordSKA/OSKAR/releases>`_ and
`RASCIL <https://developer.skao.int/projects/rascil/en/latest/index.html>`_. Please see the relevant documentation for further
information.

Details of the inputs required, models used and instructions of how to use the scripts can be found in the links below:

.. toctree::
  :maxdepth: 1

  pycraf
  oskar_beam_gain
  rascil


A command line interface is also available to accommodate multiple different RFI sources
(at the moment, TV antennas only), and produce a standardized output (in HDF5 format) of
Propagation attenuation scripts, which can be consumed by visibility simulations.

.. toctree::
  :maxdepth: 1

  rfi_interface

Additional information and the list of command line arguments of relevant scripts can be found here:

.. toctree::
  :maxdepth: 1

  supplement/index
  cli
