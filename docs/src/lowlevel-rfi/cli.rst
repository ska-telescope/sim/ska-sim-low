.. _cli:

CLI
===

Below can be found the command line interface, usage and command line argument description,
of the three main RFI simulation scripts and any additional relevant scripts.

.. _pycraf_cli:

Pycraf Python script
--------------------

.. argparse::
   :filename: ../../rfi/pycraf_scripts/SKA_low_RFI_propagation.py
   :func: cli_parser
   :prog: SKA_low_RFI_propagation.py
   :nodefault:

.. _oskar_cli:

OSKAR Python script
-------------------

.. argparse::
   :filename: ../../rfi/oskar_sim_beam_gain_sing.py
   :func: cli_parser
   :prog: oskar_sim_beam_gain_sing.py
   :nodefault:

.. _rascil_cli:

RASCIL Python script
--------------------

.. argparse::
   :filename: ../../rfi/rascil_scripts/simulate_low_rfi_visibility_propagation.py
   :func: cli_parser
   :prog: simulate_low_rfi_visibility_propagation.py
   :nodefault:

.. _power_spec_cli:

Power Spectrum Python script
----------------------------

.. argparse::
   :filename: ../../rfi/rascil_scripts/power_spectrum.py
   :func: cli_parser
   :prog: power_spectrum.py
   :nodefault:

.. _rfi_interface_cli:

RFI Interface
-------------

.. code-block:: none

    Usage:
        rfi_source_signal_interface.py tv_antenna --transmitters=<transmitter-csv> [<n_channels> <freq_start> <freq_end>]
        rfi_source_signal_interface.py aircraft
        rfi_source_signal_interface.py (-h | --help)

    Arguments:
        # if tv_antenna
        --transmitters=<transmitter-csv>      Location of input CSV file containing TV transmitter properties

    Options:
        -h --help           Show this screen.

        # if any of the following is provided, all three has to be provided as a CLI argument
        <n_channels>        Number of frequency channels. Default: 3
        <freq_start>        Start of Frequency range [MHz]. Default: 170.5
        <freq_end>          End of Frequency range [MHz]. Default: 184.5
