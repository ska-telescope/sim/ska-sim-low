.. _rascil_scripts:

Simulation of visibility with RASCIL
====================================

The final stage of the three-stage RFI simulation, :code:`simulate_low_rfi_visibility_propagation.py` uses RASCIL to
calculate the visibility measured by SKA-Low (LOW) for a number of emitters, and generate output images or measurement sets.
We are interested in the effects of RFI signals that cannot be detected in the visibility data. Therefore, in our
simulations we add transmitter apparent power and beam-gain information calculated in the previous stages.

As before, we study the effects of a TV station located in Perth, AU, emitting a broadband signal of a known power
(information stored in CSV files in **rfi/data/transmitters**). We presume the following scenario:

The emission from the TV station arrives at LOW stations with phase delay and attenuation. We calculate :ref:`pycraf_scripts`.
The RFI enters LOW stations in a side-lobe of the station beam. We perform :ref:`oskar_script`, which, together
with the pre-calculated apparent power values, is used as an input for the RASCIL script. The RFI enters each LOW station with fixed delay
and zero fringe rate (assuming no e.g. ionospheric ducting or reflection from a plane). When tracking a source on the sky,
the signal from one station is delayed and fringe-rotated. Fringe rotation stops the fringe from a source at the phase
tracking centre but phase-rotates the RFI, which now becomes time-variable. To de-correlate the RFI signal,
the correlation data are time- and frequency-averaged over a timescale appropriate for the station field of view.

We want to study the effects of this RFI on statistics of the visibilities, and on images made on source and
at the pole. The :code:`simulate_low_rfi_visibility_propagation.py` script averages the data producing baseline-dependent
de-correlation and uses
`RASCIL functions <https://developer.skao.int/projects/rascil/en/latest/processing_components/simulation/index.html#module-rascil.processing_components.simulation.rfi>`_
and input data from the previous stages to produce FITS images,
and un-averaged MeasurementSets (one per time chunk). The images are on signal channels and on pure noise channels,
and for the source of interest. Distributed processing is implemented via `Dask <https://dask.org/>`_.

Simulation inputs
-----------------

SKA Low configuration
^^^^^^^^^^^^^^^^^^^^^

An input configuration file (txt or equivalent, called as the "antenna_file") containing position information in longitude and latitude.
The default configuration file
(``rfi/data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt``) is
used by the code if the :code:`--use_antfile` argument is set to :code:`True`, else it uses the RASCIL equivalent.
If :code:`--use_antfile == True`, you can specify an alternative configuration file by setting the :code:`--antenna_file`
CLI argument (see :ref:`rascil_cli`).

Transmitter apparent power
^^^^^^^^^^^^^^^^^^^^^^^^^^

The apparent power of the transmitter is calculated by :ref:`pycraf_cli` and stored in an HDF5 file,
together with other relevant information, such as time and station-dependent azimuth
and elevation data. For more information, follow :ref:`pycraf_scripts`.

Beam gain data
^^^^^^^^^^^^^^

Beam gain values as a function of frequency, calculated by :ref:`oskar_cli` and stored in an HDF5 file,
together with pointing information (i.e. right ascension and declination).
For more information, follow :ref:`oskar_script`.

Usage and command line arguments
--------------------------------

:ref:`rascil_cli`

Power spectrum
--------------

The :code:`power_spectrum.py` script can be used following the production of output FITS images from the simulation to
produce power spectrum plots.

Usage and command line arguments: :ref:`power_spec_cli`
