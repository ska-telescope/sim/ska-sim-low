.. _cookbook-guide:

OSKAR cookbook
**************

The sections below are intended to be read in order.

.. toctree::
  :maxdepth: 2

  basic_concepts/index
  sky_model/index
  telescope_model/index
  parameter_space/index
  imaging_visibilities/index
  making_an_animation/index
