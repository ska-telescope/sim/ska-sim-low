.. _sky_model:

Setting up a sky model
**********************

Sky models used by OSKAR exist completely independently of any other
simulation parameters. The sky model can be thought of as simply a table
of source data, where each row of the table contains parameters for a single
source. As a bare minimum (but often sufficient for many simulations),
each source must specify a Right Ascension, Declination, and Stokes I flux
as the first three columns. **Source coodinates must be specified in
decimal degrees, and source fluxes in Jy.**

The class ``oskar.Sky`` is used to encapsulate data for a sky model.
Useful class methods (which create and return a new sky model) include:

- from_array(array, precision='double')
    - to convert a numpy array to a sky model.
- generate_grid(ra0_deg, dec0_deg, side_length, fov_deg, mean_flux_jy=1.0, std_flux_jy=0.0, seed=1, precision='double')
    - to generate a grid of sources around a point.
- load(filename, precision='double')
    - to load a sky model from a text file.

Useful methods on the class include:

- append(from_another)
    - to append another sky model to this one.
- create_copy()
    - to create and return a copy of a sky model.
- filter_by_flux(min_flux_jy, max_flux_jy)
    - to remove sources from the sky model based on their Stokes I flux.
      (Sources with fluxes outside the specified range will be removed.)
- filter_by_radius(inner_radius_deg, outer_radius_deg, ref_ra_deg, ref_dec_deg)
    - to remove sources from the sky model based on their angular distance
      from a reference point.
      (Sources with distances outside the specified range will be removed.)
- save(filename)
    - to save the sky model to a text file.
- to_array()
    - to convert the sky model to a numpy array.


Example: Using the GLEAM catalogue
----------------------------------

The GLEAM Extragalactic Catalogue can be downloaded as a FITS binary table
from the `VizieR <http://vizier.u-strasbg.fr/>`_ service. To use data from a
FITS binary table as a sky model, pull the data columns out into a new array
using ``astropy`` and then create an OSKAR sky model from the array,
as follows:

  .. code-block:: python

    from astropy.io import fits
    import numpy
    import oskar

    # Get the first HDU (a binary table) in the specified FITS file.
    data = fits.getdata('GLEAM_EGC.fits', 1)

    # Create a 3-column numpy array (RA, Dec, Stokes I).
    sky_array = numpy.column_stack(
                    (data['RAJ2000'], data['DEJ2000'], data['peak_flux_wide']))

    # Create the sky model from the 3-column numpy array above.
    sky = oskar.Sky.from_array(sky_array)

    # Print the number of sources in the sky model.
    print(sky.num_sources)
    >>> 307455

Example: Filtering a sky model
------------------------------

It may be necessary to filter a sky model to remove sources inside or outside
a certain radius from a specific point (such as the phase centre)
as part of a simulation script.

For example, to keep sources only within 20 degrees of the point
at (RA, Dec) = (0, 80) degrees, use:

  .. code-block:: python

    ra0 = 0
    dec0 = 80
    sky.filter_by_radius(0, 20, ra0, dec0)

Similarly, to keep sources only *outside* a radius of 20 degrees from the
same point, use instead:

  .. code-block:: python

    sky.filter_by_radius(20, 180, ra0, dec0)
