.. _defining_a_parameter_space:

Defining a parameter space and running simulations
**************************************************

An investigation may require a large number of simulations to be carried out
in order to explore a parameter space, which typically means that a set of
nested loops must be written in order to run all the simulations.

In many cases, only the simulation parameters in the settings tree need to be
changed to run a new simulation, but sometimes the sky model and/or
telescope model also needs to be changed within a loop.
Defining the parameters that need to vary is the first thing to do when writing
a new simulation script.

Example: A four-dimensional parameter space
-------------------------------------------

A simple example script which iterates over a 4-dimensional parameter space
is shown below. In this case,
the observation length (3 values: short, medium, long),
the target field (3 values: EoR0, EoR1, EoR2),
the ionospheric phase screen (2 values: on, off) and
the sky model (2 values: GLEAM and A-team only) were all varied for a total
of 36 simulations, and a CASA Measurement Set was written for each case.

Note how nested Python dictionaries are used to define groups of parameters
that need to change on each iteration, and the ``update()`` method is used to
merge one dictionary into another.
Each dimension is iterated using the general form:

.. code-block:: python

    # Iterate over a dimension.
    for key_name, params_to_update in dictionary.items():

        # Update the current settings dictionary.
        current_settings.update(params_to_update)

        # Iterate over the next dimension...

These are all standard Python constructs.

After all the parameters have been set up in the settings tree, an instance
of ``oskar.Interferometer`` is created using it. Finally,
calling ``oskar.Interferometer.run()`` will run each simulation.

.. literalinclude:: ../../../../scripts/run_sims_low_dd_effects.py
   :language: python
   :linenos:


Example: An irregular frequency axis
------------------------------------

Frequency channels which are regularly spaced can be run in one go (and written
to a single Measurement Set if required) by specifying multiple channels in
the settings.
However, when running simulations at spot frequencies across a band, these
will need to be run separately by explicitly looping over each one.
All that is required is to define a list of frequencies and then loop over
them, for example:

.. code-block:: python

    axis_freq_MHz = [50, 70, 110, 137, 160, 230, 320]
    for freq_MHz in axis_freq_MHz:
        settings['observation/start_frequency_hz'] = freq_MHz * 1e6
        ...
