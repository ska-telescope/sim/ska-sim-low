.. _local_env:

Local virtual environment
=========================

**Note:** the following information is to set up an environment that works with the
RFI simulations only. Other scripts may need more packages to be added to requirements.txt.

Differences based on Operating System
-------------------------------------

**Linux:** you may create a virtual environment with conda, virtualenvwrapper, or other
python-based virtual environment tool. Installing the requirements via pip should work in all.

**MacOS:** you will need to create the environemnt with conda. That is because :code:`python-casacore`
does not currently behave well, when trying to install it via ``pip`` into a standard python environment.

Create a virtual environment
----------------------------

virtualenvwrapper
^^^^^^^^^^^^^^^^^

To install and set up virtualenvwrapper follow `this guide <https://virtualenvwrapper.readthedocs.io/en/latest/install.html>`_.

Create an environment: replace `my-environment` with the name you prefer, and replace `python3.7` with
the path to your python3 installation. If the PYTHONPATH used by virtualenvwrapper is the python3 version
you want to use, then you can omit the ``-p`` option.

.. code-block:: bash

   mkvirtualenv -p python3.7 my-environment

Start environment:

.. code-block:: bash

   workon my-environemnt

Deactivate environment:

.. code-block:: bash

   deactivate

conda
^^^^^

To install and set up conda follow `the conda guide <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_.

Create an environemnt (replace `my-environment` with the name you prefer):

.. code-block:: bash

   conda create --name my-environment python=3.7

Start environment:

.. code-block:: bash

   conda activate my-environemnt

Deactivate environment:

.. code-block:: bash

   conda deactivate

Install requirements
--------------------

Linux
^^^^^

Once you have activated your environment and navigated into the ska-sim-low directory
(i.e. the root directory of the git repository), run the following:

.. code-block:: bash

   pip install -r requirements.txt --pre

:code:`--pre` will allow you to download the latest beta versions of dependencies.
This is necessary to get the latest RASCIL version from PyPi.

Depending on what python version you used to create the environment, the pip within that
will be tied to that python version. This command should install all of the necessary requirements.

In addition, you will have to obtain RASCIL data. RASCIL will be installed via pip as part of the above
command, however the additional setup described at `RASCIL Installation <https://developer.skao.int/projects/rascil/en/latest/RASCIL_install.html>`_
is required. If you encounter with a :code:`Tuple index out of range` error while running RASCIL-dependent code,
you may also need to go through the ``Git LFS`` steps on the same page under "Installation via git clone".

You will also need OSKAR set up. On Linux, you may use the `Singularity image <https://github.com/OxfordSKA/OSKAR>`_.

MacOS
^^^^^

On MacOS, :code:`python-casacore`, a dependency of RASCIL, does not behave well with ``pip``,
so you will need ``conda`` to install it.

Install :code:`python-casacore` with conda:

.. code-block:: bash

    conda install -c conda-forge python-casacore

Install the rest of the requirements using ``pip``:

.. code-block:: bash

    pip install -r requirements.txt --pre

:code:`--pre` will allow you to download the latest beta versions of dependencies.
This is necessary to get the latest RASCIL version from PyPi.

Depending on what python version you used to create the environment, the pip within that
will be tied to that python version. This command should install all of the necessary requirements.

In addition, you will have to obtain RASCIL data. RASCIL will be installed via pip as part of the above
command, however the additional setup described at `RASCIL Installation <https://developer.skao.int/projects/rascil/en/latest/RASCIL_install.html>`_
is required. If you encounter with a :code:`Tuple index out of range` error while running RASCIL-dependent code,
you may also need to go through the ``Git LFS`` steps on the same page under "Installation via git clone".

You will also need OSKAR set up, which you can do via `installing the binary version <https://github.com/OxfordSKA/OSKAR/releases>`_.
