.. _install:

Local development
=================

Download
--------

To clone the repository, use:

  .. code-block:: bash

     git clone https://gitlab.com/ska-telescope/ska-low-simulations.git

Or browse the files at https://gitlab.com/ska-telescope/ska-low-simulations

Install requirements
--------------------

.. toctree::
  :maxdepth: 1

  local_env
  docker_env