.. _docker_env:

Docker container as Python interpreter
======================================

**Note:** the following instructions are still under development, as not all of the RFI code
has been tested with this setup.

If you don not want to set up a complicated environment locally with all sorts of data also added
to your machine, then you can create a Docker image, which then you can use as your python
interpreter both from the command line and from `PyCharm` or `Visual Studio Code`.

Create a docker image
---------------------

Create a Dockerfile, called `docker_python_env`, with the following information in it (do not add the file to git):

.. code-block:: docker

   FROM nexus.engageska-portugal.pt/rascil-docker/rascil-base

   WORKDIR /rascil/sim-low-rfi/

   ADD requirements.txt requirements-test.txt .
   ADD docs/requirements-docs.txt .

   RUN pip install -r requirements.txt -r requirements-test.txt -r requirements-docs.txt

The starting image is ``rascil-base``. This does not contain any RASCIL data. If you need RASCIL data as part of
the image, you'll need to use ``rascil-full``. Here you can read more about
`RASCIL container images <https://gitlab.com/ska-telescope/external/rascil-docker>`_.

Build the docker image (be in the ska-sim-low directory, where your personal dockerfile should also be:

.. code-block:: bash

   docker build -t rfi-environment -f docker_python_env .

Docker as Python interpreter in IDE
-----------------------------------

Use this image as your Python interpreter in `Pycharm Professional <https://www.jetbrains.com/help/pycharm/using-docker-as-a-remote-interpreter.html>`_
or `Visual Studio Code <https://code.visualstudio.com/docs/remote/containers>`_. To set the environment up, please follow the links.

Develop locally and run code in Docker
--------------------------------------

You can also run your code, tests, bash scripts directly from the Docker container, while still accessing and
changing the files on your machine with your favourite IDE or text editor.

Start the container:

.. code-block::

   docker run -it -v ${PWD}:/rascil/sim-low-rfi --rm rfi-environment:latest

This will take you inside the container. :code:`--rm` will stop the container from running once you exit it.
:code:`${PWD}:/rascil/sim-low-rfi` will attach the directory where you start the container from, into a
directory called ``/rascil/sim-low-rfi`` that is within the container. If you change something in this directory
outside the container, the same changes will appear within the container. Make sure you start the container
from the `ska-sim-low` directory, that way you can carry on changing those files and the changes will be present
in the container as well. Now you can run, e.g., test within your container where you have a fully functioning
python environment, while still developing on your local machine.