include .make/base.mk
include .make/python.mk

PYTHON_LINT_TARGET = rfi/ tests/
PYTHON_SWITCHES_FOR_BLACK = --line-length 79 scripts/

# ignore the scripts directory
# E0401: import-error -> if we don't want to install all the requirements for linting, ignore this
# C0411: wrong-import-order -> caused by matplotlib.use("Agg")
# C0413: wrong-import-position -> caused by matplotlib.use("Agg")
# FIX THESE:
# W0511: fixme
PYTHON_SWITCHES_FOR_PYLINT = --ignore=scripts --disable E0401,C0411,C0413,W0511

# E203: whitespace before operations character
# E402: module level import not at top of file (because of matplotlib.use('Agg'))
# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,E402,W503

set-up-docs:
	pip install -r requirements.txt
	mkdir -p docs/build

docs:  ## build docs; Outputs docs/build/html
	$(MAKE) -C docs/src html

.PHONY: docs