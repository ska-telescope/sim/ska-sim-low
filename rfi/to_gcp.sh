#!/usr/bin/env bash
# Upload images to GCP

set -x

gsutil rsync -r rfi_sims gs://ska1-simulation-data/ska1-low/rfi_sims

# The rfi sims should be at: ska1-simulation-data/ska1-low/rfi_sims
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/rfi_sims/aa0p5/*.ms
