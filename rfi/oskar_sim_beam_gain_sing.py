# pylint: disable=too-many-arguments,too-many-locals,duplicate-code
# pylint: disable=too-many-positional-arguments

"""Run OSKAR"""

import argparse
import logging
import os
import pprint
import subprocess
import sys

import h5py
import numpy as np
import pandas as pd

from rfi.common_tools import (
    calculate_bandwidth_channel_range,
    default_or_antenna_value,
)
from rfi.old_run_checker import check_for_old_run, write_settings
from rfi.rfi_interface.rfi_data_cube import BeamGainDataCube

LOG = logging.getLogger("rfi-logger")
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """
    Defines the command line arguments needed to run the script.
    """
    parser = argparse.ArgumentParser(
        description="Calculate beam gain for SKA1-Low"
    )

    # General inputs
    parser.add_argument(
        "--ra", type=float, default=0.0, help="Right Ascension (deg)"
    )
    parser.add_argument(
        "--declination", type=float, default=-18.0, help="Declination"
    )
    parser.add_argument(
        "--indir",
        type=str,
        default="./data/attenuation",
        help="Directory where transmitter Az_El or HDF5 files are stored",
    )
    parser.add_argument(
        "--outdir", type=str, default="", help="Directory to store results"
    )
    parser.add_argument(
        "--oskar_path",
        type=str,
        default="./OSKAR-2.7.6-Python3.sif",
        help="Path to the singularity SIF file for OSKAR",
    )
    parser.add_argument(
        "--telescope_path",
        type=str,
        default="./data/telescope_files/"
        "SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm",
        help="Path to telescope model directory",
    )

    # HDF5 input
    parser.add_argument(
        "--input_hdf_file",
        type=str,
        default="tv_transmitter_attenuation_cube.hdf5",
        help="HDF5 file located in --indir, which contains "
        "necessary coordinate information for each RFI source."
        "If not specified, use individual az/el and "
        "transmitter files located in --indir.",
    )

    # Transmitter file and Az/El file inputs
    # and manually set frequency information
    parser.add_argument(
        "--transmitters",
        type=str,
        default="data/transmitters/Filtered_DTV_list_1_el.csv",
        help="CSV file containing transmitter properties; "
        "not used with HFD data",
    )
    parser.add_argument(
        "--set_freq",
        type=str,
        default="False",
        help="Choose the central frequency with --freq, "
        "otherwise read it from the CSV file;"
        "not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--freq",
        type=float,
        default=177.5,
        help="Central frequency (MHz); not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--set_bandwidth",
        type=str,
        default="False",
        help="Choose the bandwidth with --bandwidth, otherwise "
        "read it from the CSV file; not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--bandwidth",
        type=float,
        default=7,
        help="Bandwidth (MHz); not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--N_channels",
        type=int,
        default=3,
        help="Number of frequency channels (must match "
        "nchannels_per_chunk for RFI simulation run); "
        "not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--frequency_range",
        type=float,
        nargs=2,
        default=[170.5, 184.5],
        help="Frequency range (MHz); not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--choose_range",
        type=str,
        default="False",
        help="use channels over full frequency range given. "
        "If False, default to only over specified bandwidth. "
        "If full frequency range larger than bandwidth number "
        "of output channels will be those within the bandwidth only. "
        "Not used when input is an HDF5 file.",
    )
    parser.add_argument(
        "--beam_gain_out",
        type=str,
        default="transmitter_name",
        help="Starting name of output beam gain file "
        "for each transmitter. Directory and file-type "
        "not required. Not used when input is an HDF5 file.",
    )
    return parser


# pylint: disable=inconsistent-return-statements
def _generate_default_oskar_settings(
    args,
    dtv_antenna,
    in_dir,
    out_dir,
    telescope_file_path,
    telescope_file_name,
):
    """
    Generate settings for OSKAR default run.
    OSKAR run for only the SKA array centre.

    :param args: Result of argparse.ArgumentParser.parse_args() (namespace)
    :param dtv_antenna: pandas.Series with antenna information
                        from args.transmitters file
    :param in_dir: directory where transmitter Az_El files are stored
    :param out_dir: directory to store results
    :param telescope_file_path: path to CSV file of telescope model
    :param telescope_file_name: CSV file name of telescope model

    :return: dict of settings
    """
    n_freqs = args.N_channels
    freq_start = args.frequency_range[0]
    freq_end = args.frequency_range[1]
    frequency = np.linspace(freq_start, freq_end, n_freqs)
    freq_inc = frequency[1] - frequency[0]

    tx_freq = default_or_antenna_value(
        "central frequency", args.set_freq, args.freq, dtv_antenna
    )
    bandwidth = default_or_antenna_value(
        "bandwidth", args.set_bandwidth, args.bandwidth, dtv_antenna
    )

    if args.choose_range == "False" and freq_end - freq_start > bandwidth:
        (
            n_freqs,
            freq_start,
            freq_end,
            new_freq,
        ) = calculate_bandwidth_channel_range(bandwidth, frequency, tx_freq)
        try:
            freq_inc = new_freq[1] - new_freq[0]
        except IndexError:
            freq_inc = 1.0
        except TypeError:  # when above function returns a tuple of Nones
            LOG.warning("Cannot obtain frequency range values over bandwidth.")
            return

        LOG.info(
            "Calculating attenuation over transmitter bandwidth only "
            "(%.2f, %.2f MHz)",
            freq_start,
            freq_end,
        )
        LOG.info("Attenuation values for %d channels", n_freqs)
    else:
        LOG.info("Calculating attenuation for all %d channels", n_freqs)

    # Get name of output file.
    if args.beam_gain_out == "transmitter_name":
        out_file = dtv_antenna["Name"] + "_beam_gain"
    else:
        out_file = args.beam_gain_out

    # Create OSKAR settings.
    settings = {
        "simulator": {"use_gpus": False},
        "observation": {
            "num_channels": n_freqs,
            "start_frequency_hz": round(freq_start * 1e06, 1),
            "frequency_inc_hz": round(freq_inc * 1e06, 1),
            "phase_centre_ra_deg": args.ra,
            "phase_centre_dec_deg": args.declination,
            "num_time_steps": 1,
            # source at ra-dec has to be visible from station at this time
            "start_time_utc": "01-01-2000 09:32:00.000",
            "length": 1.0,
        },
        "telescope": {
            "input_directory": os.path.join(
                telescope_file_path, telescope_file_name
            )
        },
        "beam_pattern": {
            "all_stations": True,
            "coordinate_type": "Sky model",
            "coordinate_frame": "Horizon",
            "sky_model/file": os.path.join(
                in_dir, dtv_antenna["Name"] + "_AzEl.txt"
            ),
            "root_path": os.path.join(out_dir, out_file),
            "telescope_outputs/text_file/cross_power_amp": True,
        },
    }
    return settings


def run_default(app, args, in_dir, out_dir, telescope_model, telescope_parent):
    """
    Generate OSKAR setting using the default arguments and run OSKAR:
        - transmitters read from a transmitter CSV file
        - az/el data read from .txt files
        - OSKAR is run for the centre of SKA Low array
        - no HDF5 is used
        - outputs individual files for each transmitter for each Stokes param

    :param app: available OSKAR binary
    :param args: input command line arguments
    :param in_dir: directory where transmitter Az_El files are stored
    :param out_dir: directory to store results
    :param telescope_model: CSV file name of telescope model
    :param telescope_parent: path to CSV file of telescope model
    """
    # Loop over transmitters.
    filtered_transmitter_df = pd.read_csv(
        args.transmitters, sep=",", low_memory=False
    )
    for _, dtv_antenna in filtered_transmitter_df.iterrows():
        LOG.info("Transmitter data:\n%s", pprint.pformat(dtv_antenna))
        settings = _generate_default_oskar_settings(
            args,
            dtv_antenna,
            in_dir,
            out_dir,
            telescope_parent,
            telescope_model,
        )

        if settings is None:
            LOG.warning("No settings for transmitter. Skipping.")
            continue

        # Write OSKAR settings to file.
        settings_path = "oskar_settings.ini"
        with open(settings_path, "w", encoding="utf-8") as file:
            for section in settings:
                file.write(f"\n[{section}]\n")
                for key, value in settings[section].items():
                    file.write(f"{key}={str(value)}\n")

        # Evaluate cross-power station beam using the settings file.
        error_code = subprocess.call(app + [settings_path])
        if error_code != 0:
            raise RuntimeError("Failed to run OSKAR")


def _generate_oskar_settings_from_hdf5(
    args,
    freq_info,
    az_el_coords,
    station_index,
    telescope_file_path,
    telescope_file_name,
):
    """
    Generate settings for OSKAR from HDF5 file.
    OSKAR run for each SKA station individually.

    :param args: Result of argparse.ArgumentParser.parse_args() (namespace)
    :param freq_info: tuple of number of channels, start of frequency range,
                      frequency increment (n_channels, freq_start, freq_inc)
    :param az_el_coords: list of [azimuth, elevation, distance]
                         coordinates for the RFI source
    :param station_index: index of SKA station (int)
    :param telescope_file_path: path to CSV file of telescope model
    :param telescope_file_name: CSV file name of telescope model

    :return: dict of settings
    """
    # OSKAR has to read in the azimuth/elevation info from a text file
    # here we create a temporary one, which is removed at the end of the run
    with open("tmp_in.txt", "w", encoding="utf-8") as f:
        print(az_el_coords[0], az_el_coords[1], file=f)

    # Create OSKAR settings.
    settings = {
        "simulator": {"use_gpus": False},
        "observation": {
            "num_channels": freq_info[0],
            "start_frequency_hz": round(freq_info[1]),
            "frequency_inc_hz": round(freq_info[2]),
            "phase_centre_ra_deg": args.ra,
            "phase_centre_dec_deg": args.declination,
            "num_time_steps": 1,
            # source at ra-dec has to be visible from station at this time
            "start_time_utc": "01-01-2000 09:32:00.000",
            "length": 1.0,
        },
        "telescope": {
            "input_directory": os.path.join(
                telescope_file_path, telescope_file_name
            )
        },
        "beam_pattern": {
            "all_stations": False,
            "station_ids": station_index,
            "coordinate_type": "Sky model",
            "coordinate_frame": "Horizon",
            "sky_model/file": "tmp_in.txt",
            # output into temp file, which is read back in later during the run
            "root_path": "tmp_out",
            "station_outputs/text_file/auto_power": True,
        },
    }
    return settings


def _read_oskar_output_data(out_files):
    """
    OSKAR produces four files per run, each containing
    beam gain information for every channel.
    The four files are one each for Stokes I, Q, U, and V

    Here, we read this data back from the files and
    save it in a dict for later use.

    :param out_files: list of files produced by OSKAR (path+filename)

    :return: dict of beam_gain data for each Stokes value.
    """
    beam_gain_data = {"i": [], "q": [], "u": [], "v": []}

    stokes_dict = {"i": "_I.txt", "q": "_Q.txt", "u": "_U.txt", "v": "_V.txt"}
    for k, stokes in stokes_dict.items():
        read_file = [f for f in out_files if f.endswith(stokes)][0]

        with open(read_file, "r", encoding="utf-8") as ff:
            for line in ff:
                if not line.strip().startswith("#"):
                    beam_gain_data[k].append(float(line.rstrip("\n")))

    return beam_gain_data


def run_with_hdf5(
    app, args, in_dir, out_dir, telescope_model, telescope_parent
):
    """
    Generate OSKAR setting using the input RFI data HDF5 file
        - OSKAR is run for each SKA Low station, and for each TV transmitter
        - the output (temporary) OSKAR files are read back in
          and concatenated into a second HDF5 file.
        - temporary files are removed

    :param app: available OSKAR binary
    :param args: input command line arguments
    :param in_dir: directory where HDF5 files is stored
    :param out_dir: directory to store results
    :param telescope_model: CSV file name of telescope model
    :param telescope_parent: path to CSV file of telescope model
    """
    hdf_file = f"{in_dir}/{args.input_hdf_file}"
    LOG.info("Loading data from %s.", hdf_file)

    with h5py.File(hdf_file, "r") as hdf:
        coords = hdf["coordinates"][()]
        freq_channels = hdf["frequency_channels"][()]
        source_ids = hdf["source_id"][()]
        station_ids = hdf["station_id"][()]

    n_freqs = len(freq_channels)
    freq_start = freq_channels[0]
    if n_freqs == 1:
        freq_inc = 0.0
    else:
        freq_inc = freq_channels[1] - freq_channels[0]
    freq_info = (n_freqs, freq_start, freq_inc)

    LOG.info(
        "Running OSKAR for %s RFI sources, %s stations, "
        "and %s frequency channels",
        len(source_ids),
        len(station_ids),
        n_freqs,
    )
    beam_gain_cube = BeamGainDataCube(
        args.ra,
        args.declination,
        # time of observation, same as one in OSKAR settings
        "01-01-2000 09:32:00.000",
        freq_channels,
        source_ids,
        len(station_ids),
    )

    beam_gain_array = np.zeros(
        (len(source_ids), len(station_ids), len(freq_channels), 4)
    )  # 4 ==> four stokes values, I, Q, U, V
    # iterate over each RFI source
    for i, rfi_source in enumerate(source_ids):
        LOG.info("Running OSKAR for transmitter: %s", rfi_source)

        coords_for_source = coords[i][0]

        # iterate over each SKA station
        for j, _ in enumerate(station_ids):
            settings = _generate_oskar_settings_from_hdf5(
                args,
                freq_info,
                coords_for_source[j],
                j,
                telescope_parent,
                telescope_model,
            )

            # Write OSKAR settings to file.
            settings_path = "oskar_settings.ini"
            with open(settings_path, "w", encoding="utf-8") as file:
                for sect_key, section in settings.items():
                    file.write(f"\n[{sect_key}]\n")
                    for key, value in section.items():
                        file.write(f"{key}={str(value)}\n")

            # Evaluate cross-power station beam using the settings file.
            error_code = subprocess.call(app + [settings_path])
            if error_code != 0:
                raise RuntimeError("Failed to run OSKAR")

            # read output OSKAR files back in + append beam_gain data to array
            out_files = [f for f in os.listdir(".") if f.startswith("tmp_out")]
            data = _read_oskar_output_data(out_files)

            beam_gain_array[i, j, :, 0] = np.array(data["i"])
            beam_gain_array[i, j, :, 1] = np.array(data["q"])
            beam_gain_array[i, j, :, 2] = np.array(data["u"])
            beam_gain_array[i, j, :, 3] = np.array(data["v"])

            # delete temp_out files
            for f in out_files:
                os.remove(f)

    os.remove("tmp_in.txt")

    # add beam_gain data to cube and save results
    beam_gain_cube.beam_gain = beam_gain_array
    beam_gain_cube.export_to_hdf5(f"{out_dir}/beam_gain_tv_transmitters.hdf5")


def run_oskar_beam(args):
    """
    Evaluates the cross-power station beam at transmitter locations
    using the supplied command line arguments.

    Args:
        args (namespace): Result of argparse.ArgumentParser.parse_args().
    """

    LOG.info("Starting LOW beam gain calculation")

    # file to store args of the call
    settings_file = "old_settings/LOW_beam_gain_settings.csv"
    check_for_old_run(vars(args), settings_file)

    LOG.info("Input arguments:\n%s", pprint.pformat(vars(args)))

    # Get name and parent directory of telescope model, inputs and outputs.
    telescope_model = os.path.basename(args.telescope_path)
    telescope_parent = os.path.dirname(args.telescope_path)
    in_dir = args.indir
    out_dir = args.outdir

    # Ensure output directory exists.
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Check for presence of Singularity and set up command to execute.
    app = ["oskar_sim_beam_pattern"]
    if os.path.exists(args.oskar_path):
        try:
            # Command will fail with exception if Singularity is not installed.
            subprocess.call(
                ["singularity", "--version"], stdout=subprocess.DEVNULL
            )

            # Construct the Singularity command to use.
            app = [
                "singularity",
                "exec",
                "--bind",
                f"{telescope_parent}:/telescope",
                "--bind",
                f"{in_dir}:/in_dir",
                "--bind",
                f"{out_dir}:/out_dir",
                args.oskar_path,
            ] + app
            LOG.info(
                "Running OSKAR using Singularity container: [%s]",
                " ".join(app),
            )

            # Re-assign directories to those bound inside the container.
            telescope_parent = "/telescope"
            in_dir = "/in_dir"
            out_dir = "/out_dir"

        except FileNotFoundError:
            # If Singularity is not installed, run natively.
            LOG.info("Singularity not found: Running OSKAR natively")
    else:
        # If Singularity image is not found, run natively.
        LOG.info(
            "Singularity image '%s' not found: Running OSKAR natively",
            args.oskar_path,
        )

    # Generate individual bram_gain output files,
    # load individual az-el text files.
    if args.input_hdf_file == "":
        run_default(
            app, args, in_dir, out_dir, telescope_model, telescope_parent
        )

    # Generate beam_gain HDF5 file,
    # load rfi HDF5 file with transmitter coordinates
    else:
        run_with_hdf5(
            app, args, in_dir, out_dir, telescope_model, telescope_parent
        )

    # write args of the call to file
    write_settings(vars(args), settings_file)


def main():
    """Main function"""
    parser = cli_parser()
    args = parser.parse_args()
    run_oskar_beam(args)


if __name__ == "__main__":
    main()
