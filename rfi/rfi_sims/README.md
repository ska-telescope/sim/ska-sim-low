
The SKA direction-dependent RFI simulations are kept on the Google Cloud Platform.

The python command line tool gsutil allows for interacting with the Google Cloud Platform: 

    https://cloud.google.com/storage/docs/gsutil 

After installing gsutil, you may download the simulations as follows: 

    mkdir rfi_sims
    cd rfi_sims
    gsutil rsync -r gs://ska1-simulation-data/ska1-low/rfi_sims .

If you wish to run these or similar simulations, install RASCIL, and use the
shell scripts and slurm files as template.
