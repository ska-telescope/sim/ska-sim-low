#!/usr/bin/env bash

# Common settings for running an end-to-end simulation.
# See the python3 help for more details about these options.

# necessary for imports to work
# we are adding the git directory ska-sim-low to PYTHONPATH
git_root_dir=`git rev-parse --show-toplevel`
export PYTHONPATH="${git_root_dir}:$PYTHONPATH"

# RA
ra=0.0
# Dec
dec=-90

n_chan="512"
# Number of channels to average
ch_avg="1"
# Pre-averaging integration time
t_int="1.0"
# Number of integrations per chunk
n_t=16
# Number of integrations to average
t_avg="1"
# Hour angle range (in hours)
t_range_beg=-4.0
t_range_end=4.0

# Number of transmitters
n_trans="100"
freq_range_mhz_low="170.5" #total range -- lower end
freq_range_mhz_high="184.5" #total range -- upper end

atten_dir="./data/attenuation/"
beamgain_dir="./data/beam_gain/"
pycraf_file="./data/transmitters/Filtered_DTV_list_1.csv"
trans_out_file="./data/attenuation/DTV_above0_1775_${n_trans}T_${n_chan}C.csv"
antenna_workfile="./data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt"
telescope_path='data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm'

# Run

echo "--------------------------"
echo " Running pycraf script... "
echo "--------------------------"

python ./pycraf_scripts/SKA_low_RFI_propagation.py --transmitters ${pycraf_file} --output_dir ${atten_dir} \
  --n_channels ${n_chan} --frequency_range ${freq_range_mhz_low} ${freq_range_mhz_high} --trans_out ${trans_out_file} \
  --srtm_directory ./data/SRTM_data/ --antenna_file ${antenna_workfile} || exit

echo "--------------------------"
echo " Running OSKAR script...  "
echo "--------------------------"

python3 ./oskar_sim_beam_gain_sing.py --transmitters ${trans_out_file}  --outdir ${beamgain_dir} --indir ${atten_dir} \
  --oskar_path ./OSKAR-2.7.6-Python3.sif --N_channels ${n_chan} \
  --frequency_range ${freq_range_mhz_low} ${freq_range_mhz_high} \
  --ra ${ra} --dec ${dec} --telescope_path ${telescope_path}|| exit

echo "--------------------------"
echo " Running RASCIL script... "
echo "--------------------------"

python3 ./rascil_scripts/simulate_low_rfi_visibility_propagation.py --use_dask=True  \
  --telescope LOW-AA0.5 --rmax 1e4 --noise True --use_agg True  --ra ${ra} --declination ${dec} --use_beamgain True \
  --beamgain_dir ${beamgain_dir} --attenuation_dir ${atten_dir} \
  --transmitter_file ${trans_out_file} --time_range ${t_range_beg} ${t_range_end} --channel_average ${ch_avg} \
  --nintegrations_per_chunk ${n_t} \
  --nchannels_per_chunk ${n_chan} --integration_time ${t_int} --time_average ${t_avg} --write_ms True || exit

sleep 30

echo "--------------------------"
echo " Finding MS size...       "
echo "--------------------------"
echo `du -sh /alaska/tim/Code/ska-sim-low/rfi/simulate_rfi.ms`
echo
echo "--------------------------"
echo " Running RASCIL imager... "
echo "--------------------------"

python3 $RASCIL/rascil/apps/rascil_imager.py \
--ingest_msname /alaska/tim/Code/ska-sim-low/rfi/simulate_rfi.ms --ingest_dd 0 \
--ingest_vis_nchan ${n_chan} --ingest_chan_per_vis 32 --mode invert --imaging_cellsize 0.0001 \
--imaging_npixel 8192 --imaging_weighting uniform || exit

echo "--------------------------"
echo " Running RASCIL visualise "
echo "--------------------------"

python3 $RASCIL/rascil/apps/rascil_vis_ms.py \
  --ingest_msname /alaska/tim/Code/ska-sim-low/rfi/simulate_rfi.ms || exit

echo "--------------------------"
echo " Finished.                "
echo "--------------------------"
