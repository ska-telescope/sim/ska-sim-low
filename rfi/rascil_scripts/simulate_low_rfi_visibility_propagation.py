# pylint: disable=too-many-arguments,too-many-locals
# pylint: disable=too-many-statements,line-too-long
# pylint: disable=redefined-outer-name
# pylint: disable=too-many-positional-arguments

# coding: utf-8
"""
Simulate low level RFI

We are interested in the effects of RFI signals that cannot be detected in
the visibility data. Therefore, in our simulations we add attenuation and beam-gain
information. For reference ~180dB attenuation for a DTV station in Perth provides
an SNR about 1 in the un-averaged time-frequency data.

The scenario is:
* There is a TV station at a remote location (e.g. Perth), emitting a broadband signal
(e.g. 7MHz) of known power (e.g. 50kW).
* The emission from the TV station arrives at LOW stations with phase delay and attenuation.
Neither of these are well known but they are probably static. Attenuation values are
pre-calculated and can be included.
* The RFI enters LOW stations in a side-lobe of the station beam. Calculations by Fred Dulwich
indicate that this provides attenuation of about 55 - 60dB for a source close to the horizon.
Beam-gain values can be pre-calculated and included.
* The RFI enters each LOW station with fixed delay and zero fringe rate (assuming no e.g.
ionospheric ducting or reflection from a plane)
* In tracking a source on the sky, the signal from one station is delayed and fringe-rotated
to stop the fringes for one direction on the sky.
* The fringe rotation stops the fringe from a source at the phase tracking centre but
phase rotates the RFI, which now becomes time-variable.
* The correlation data are time- and frequency-averaged over a timescale appropriate for
the station field of view. This averaging de-correlates the RFI signal.
* We want to study the effects of this RFI on statistics of the visibilities, and on images
made on source and at the pole.

The simulate_low_rfi_visibility.py script averages the data producing baseline-dependent
de-correlation. The effect of averaging is not more than about -20dB but it does vary with
baseline giving the radial power spectrum we see. The 55-60 dB is part of the 180dB. To give
a signal to noise of 1 or less, the terrain propagation must be about 100dB.

The simulation is implemented in some functions in RASCIL, and the script
simulate_low_rfi_visibility_propagation is available in the SKA Github repository ska-sim-low.
Distributed processing is implemented via Dask. The outputs are fits file and plots of the
images: on signal channels and on pure noise channels, and for the source of interest and
the Southern Celestial Pole. The un-averaged MeasurementSets are also output, one per time chunk.
"""  # noqa: E501

import argparse
import logging
import os
import pprint
import sys
import time

import astropy.constants as const
import h5py
import matplotlib
import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from rascil.processing_components.simulation.rfi import (
    calculate_averaged_correlation,
    simulate_rfi_block_prop,
)
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute
from rascil.workflows.rsexecute.visibility.visibility_rsexecute import (
    concatenate_visibility_time_rsexecute,
)
from ska_sdp_datamodels.configuration.config_create import select_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)
from ska_sdp_datamodels.visibility import (
    create_visibility,
    export_visibility_to_ms,
)
from ska_sdp_func_python.util.array_functions import average_chunks

from rfi.common_tools import generate_ska_antenna_configuration
from rfi.old_run_checker import check_for_old_run, write_settings

matplotlib.use("Agg")

log = logging.getLogger("rfi-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """CLI argument parser"""
    parser = argparse.ArgumentParser(
        description="Simulate RFI data with RASCIL"
    )
    parser.add_argument(
        "--seed", type=int, default=18051955, help="Random number seed"
    )
    parser.add_argument(
        "--noise",
        type=str,
        default="False",
        help="Add random noise to the visibility samples?",
    )
    parser.add_argument(
        "--ra", type=float, default=0.0, help="Right Ascension (degrees)"
    )
    parser.add_argument(
        "--declination",
        type=float,
        default=-45.0,
        help="Declination (degrees)",
    )
    parser.add_argument(
        "--nchannels_per_chunk",
        type=int,
        default=1024,
        help="Number of channels in a chunk",
    )
    parser.add_argument(
        "--channel_average",
        type=int,
        default=16,
        help="Number of channels in a chunk to average",
    )
    parser.add_argument(
        "--frequency_range",
        type=float,
        nargs=2,
        default=[170.5e6, 184.5e6],
        help="Frequency range (Hz)",
    )
    parser.add_argument(
        "--time_average",
        type=int,
        default=16,
        help="Number of integrations in a chunk to average",
    )
    parser.add_argument(
        "--integration_time",
        type=float,
        default=0.25,
        help="Integration time (s)",
    )
    parser.add_argument(
        "--time_range",
        type=float,
        nargs=2,
        default=[-6.0, 6.0],
        help="Hourangle range (hours)",
    )
    parser.add_argument(
        "--input_file",
        type=str,
        default="",
        help="Full path to the HDF5 file, which contains necessary "
        "RFI information for each RFI source.",
    )
    parser.add_argument(
        "--use_beamgain",
        type=str,
        default="False",
        help="Use beam gain values in calculation",
    )
    parser.add_argument(
        "--beamgain_hdf_file",
        type=str,
        default="beam_gain_tv_transmitters.hdf5",
        help="HDF5 file with beam gain, transmitter, frequency, "
        "and pointing (RA, DEC) information.",
    )
    parser.add_argument(
        "--beamgain_dir",
        type=str,
        default="../data/beam_gain",
        help="Folder containing multiple Numpy files or the HDF file "
        "with beam gain information.",
    )
    parser.add_argument(
        "--use_antfile",
        type=str,
        default="False",
        help="Use the antenna file in the rfi data in calculation, "
        "otherwise use from RASCIL",
    )
    parser.add_argument(
        "--antenna_file",
        type=str,
        default="./data/telescope_files/"
        "SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/"
        "layout_wgs84.txt",
        help="txt file containing antenna locations",
    )
    parser.add_argument(
        "--write_ms", type=str, default="True", help="Write measurement set?"
    )
    parser.add_argument(
        "--msout",
        type=str,
        default="./simulate_rfi.ms",
        help="Name for MeasurementSet",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default="./",
        help="Output directory for storing files",
    )

    # Dask usage
    parser.add_argument(
        "--use_dask",
        type=str,
        default="True",
        help="Use dask to distribute processing?",
    )

    return parser


def add_noise(bvis):
    """
    Add noise to the visibilities.
    Note: this will update the original bvis input array in-place too.

    :param bvis: Visibility
    :return: Visibility with noise
    """
    # The specified sensitivity:
    # (effective area / T_sys) is roughly 610 m ^ 2 / K in the range 160 - 200MHz # noqa: E501
    # sigma_vis = 2 k T_sys / (area * sqrt(tb)) = 2 k 512 / (610 * sqrt(tb)
    sens = 610
    bt = bvis.channel_bandwidth[0] * bvis.integration_time[0]
    sigma = 2 * 1e26 * const.k_B.value / ((sens / 512) * (numpy.sqrt(bt)))
    sshape = bvis.vis.shape
    bvis["vis"].data += numpy.random.normal(
        0.0, sigma, sshape
    ) + 1j * numpy.random.normal(0.0, sigma, sshape)
    return bvis


def simulate_rfi_image_prop(
    config,
    times,
    frequency,
    apparent_transmitter_power,
    channel_bandwidth,
    phasecentre,
    polarisation_frame,
    time_average,
    channel_average,
    noise,
    beam_gain,
    transmitter_coordinates=None,
    transmitter_ids=None,
    rfi_frequencies=None,
):
    """
    Simulate RFI and then average over time and frequency,
    producing a Visibility

    :param config: RASCIL telescope Configuration e.g. LOW
    :param times: observation times (hour angles in radians)
    :param frequency: Frequencies (numpy.array) Hz
    :param apparent_transmitter_power: power transmitted by RFI source
                        as seen by an isotropic antenna
                        (numpy.ndarray) dimensions:
                        [nsource, ntimes, nstations, nfreqs]
    :param channel_bandwidth: Channel bandwidth (numpy.array) Hz
    :param phasecentre: Phasecentre of observations (as SkyCoord)
    :param polarisation_frame: In RASCIL format
                               e.g. PolarisationFrame("stokesI")
    :param time_average: Number of integrations to average (int)
    :param channel_average: Number of channels to average (int)
    :param noise: Add noise? Add thermal noise (approximate numbers)
    :param beam_gain: beam gain (numpy.ndarray or float)
                      to be applied to the apparent power
    :param transmitter_coordinates: apparent coordinates of the RFI transmitter
                    (azimuth, elevation, distance)
                    (numpy.ndarray) dimensions:
                    [nsources, ntimes, nstations, 3]
    :param transmitter_ids: 1D numpy array of the RFI source IDs
    :param rfi_frequencies: 1D numpy array of the frequencies
                            where the RFI signal appears

    :return: Visibility containing averaged data
    """

    averaged_frequency = numpy.array(
        average_chunks(frequency, numpy.ones_like(frequency), channel_average)
    )[0]
    averaged_channel_bandwidth, wts = numpy.array(
        average_chunks(
            channel_bandwidth, numpy.ones_like(frequency), channel_average
        )
    )
    averaged_channel_bandwidth *= wts
    averaged_times = numpy.array(
        average_chunks(times, numpy.ones_like(times), time_average)
    )[0]

    # times are the hour angles in seconds (!) relative to transit.
    # create_visibility needs hour angles in radians
    s2r = numpy.pi / 43200.0
    bvis = create_visibility(
        config,
        s2r * times,
        frequency,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phasecentre,
        polarisation_frame=polarisation_frame,
        zerow=False,
    )

    # Now fill in the visibility with simulated data
    bvis = simulate_rfi_block_prop(
        bvis,
        apparent_transmitter_power,
        transmitter_coordinates,
        transmitter_ids,
        rfi_frequencies,
        low_beam_gain=beam_gain,
        apply_primary_beam=False,  # not needed for Low
    )

    # Now create a visibility for the averaged data
    averaged_bvis = create_visibility(
        config,
        s2r * averaged_times,
        averaged_frequency,
        channel_bandwidth=averaged_channel_bandwidth,
        phasecentre=phasecentre,
        polarisation_frame=polarisation_frame,
        zerow=False,
    )

    # Calculate the averaged visibility values and fill into the averaged_bbvis
    npol = 1
    for itime, _ in enumerate(averaged_times):
        atime = itime * time_average
        for ibaseline, (_, _) in enumerate(averaged_bvis.baselines.data):
            for ichan, _ in enumerate(averaged_frequency):
                achan = ichan * channel_average
                for pol in range(npol):
                    averaged_bvis["vis"].data[itime, ibaseline, ichan, pol] = (
                        calculate_averaged_correlation(
                            bvis["vis"].data[
                                atime : (atime + time_average),
                                ibaseline,
                                achan : (achan + channel_average),
                                pol,
                            ],
                            time_average,
                            channel_average,
                        )[0, 0]
                    )
                    averaged_bvis["vis"].data[itime, ibaseline, ichan, pol] = (
                        numpy.conjugate(
                            averaged_bvis["vis"].data[
                                itime, ibaseline, ichan, pol
                            ]
                        )
                    )
                achan += 1
        atime += 1

    del bvis

    # Optionally add noise
    if noise:
        averaged_bvis = add_noise(averaged_bvis)

    return averaged_bvis


def _init_logging_for_dask(output_dir):
    """Set up logging which can be called from Dask."""
    logfile = f"{output_dir}/simulate_low_rfi_visibility_propagation.log"

    def init_logging():
        logging.basicConfig(
            filename=logfile,
            filemode="a",
            format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%H:%M:%S",
            level=logging.INFO,
        )

    init_logging()
    # pylint: disable=redefined-outer-name
    log = logging.getLogger("rfi-logger")
    log.info("Writing log file to %s", logfile)
    return init_logging, log


def load_beam_gain_and_ra_dec(args, source_ids, rfi_freq_chans, log):
    """
    Load and format beam gain data. Also load the RA and DEC values,
    which may come either from the beam gain HDF file, or
    via user provided args.

    If beam gain is not used, 1.0 is returned.
    Else a numpy array loaded from file,
    with shape [nrfi_sources x nstations x nchannels].
    If beam gain data are for the station centre, nstations = 1

    :param args: user provided arguments
    :param source_ids: 1D array of RFI source ids (string)
    :param rfi_freq_chans: 1D array of RFI frequencies (float)
    :param log: logging object

    :returns beam_gain, right_ascension, declination
    """
    if args.use_beamgain == "True":
        bg_direct = os.path.abspath(args.beamgain_dir)
        if not bg_direct.endswith("/"):
            bg_direct = bg_direct + "/"

        if not args.beamgain_hdf_file == "":
            hdf_file = f"{bg_direct}{args.beamgain_hdf_file}"
            log.info(
                "Loading beam gain data from HDF file: %s. "
                "Only loading Stokes I.",
                hdf_file,
            )

            with h5py.File(hdf_file, "r") as hdf:
                beam_gain = hdf["beam_gain"][()][
                    ..., 0
                ]  # first of the last index is Stokes I
                right_ascension = hdf["right_ascension"][()]
                declination = hdf["declination"][()]
                bg_frequencies = hdf["freq_channels"][()]

            if (bg_frequencies != rfi_freq_chans).any():
                raise (
                    ValueError(
                        "Miss-match between RFI frequencies "
                        "and beam gain frequencies."
                    )
                )

            return beam_gain, right_ascension, declination

        log.info(
            "Using Numpy beam gain files in %s. Only loading Stokes I.",
            bg_direct,
        )

        beam_gain = numpy.zeros((len(source_ids), 1, len(rfi_freq_chans)))

        for i, source in enumerate(source_ids):
            beam_gain_files = (
                f"{bg_direct}{source}_beam_gain_TIME_SEP_CHAN_"
                f"SEP_CROSS_POWER_AMP_I_I.txt"
            )
            beam_gain[i, :, :] = numpy.loadtxt(beam_gain_files)

        return beam_gain, args.ra, args.declination

    log.info("No beam gains applied.")
    beam_gain = 1.0
    return beam_gain, args.ra, args.declination


def get_chunk_start_times(
    time_range, integration_time, nintegrations_per_chunk, log
):
    """
    Generate start times for each chunk.

    :param time_range: hour angle range (??) -- see argparse arguments
    :param integration_time: integration time within a chunk
    :param nintegrations_per_chunk: number of integrations in a time chunk
    :param log: logging object also used in Dask
    """
    start_times = numpy.arange(
        time_range[0] * 3600.0,
        time_range[1] * 3600.0,
        nintegrations_per_chunk * integration_time,
    )
    log.info("Start times %s", start_times)

    chunk_start_times = [
        start_times[i : i + 1] for i in range(0, len(start_times))
    ]
    log.info("Chunk start times %s", [c[0] for c in chunk_start_times])
    log.info("Processing %d time chunks", len(start_times))
    return chunk_start_times


def rfi_simulation(args):
    """
    :param args: user-defined arguments;
                 result of argparse.ArgumentParser.parse_args()
    """
    settings_file = "old_settings/RFI_propagation_settings.csv"
    check_for_old_run(vars(args), settings_file)

    output_dir = os.path.abspath(args.output_dir)
    init_logging, log = _init_logging_for_dask(output_dir)

    log.info("Starting LOW propagation calculation")

    start_epoch = time.asctime()
    log.info(
        "\nSKA LOW RFI simulation using RASCIL\nStarted at %s\n", start_epoch
    )

    log.info("Starting LOW low-level RFI simulation")
    log.info("Input arguments:\n%s", pprint.pformat(vars(args)))

    numpy.random.seed(args.seed)

    rsexecute.set_client(use_dask=args.use_dask == "True")
    rsexecute.run(init_logging)
    rsexecute.init_statistics()

    with h5py.File(os.path.abspath(args.input_file), "r") as hdf:
        coords = hdf["coordinates"][()]
        rfi_freq_channels = hdf["frequency_channels"][()]
        source_ids = hdf["source_id"][()]
        rfi_signal = hdf["signal"][()]
        # n_time_samples ==> same as "Number of integrations in a time chunk"
        n_time_samples = len(hdf["time_samples"][()])
        rmax = hdf["station_id"].attrs["max_station_distance_from_centre"]
        station_skip = hdf["station_id"].attrs["station_skip"]

    source_ids = [sid.decode() for sid in source_ids]

    beam_gain, ra, dec = load_beam_gain_and_ra_dec(
        args, source_ids, rfi_freq_channels, log
    )

    if args.noise == "True":
        noise = True
        log.info("Adding noise to simulated data")
    else:
        noise = False

    low = generate_ska_antenna_configuration(
        args.use_antfile, args.antenna_file, rmax, log
    )
    # xarray update - update all xarray variables
    # to match station skipping in configuration
    sub_low = select_configuration(low, low.names[::station_skip])

    phasecentre = SkyCoord(
        ra=ra * u.deg, dec=dec * u.deg, frame="icrs", equinox="J2000"
    )
    log.info("Phasecentre is %s", phasecentre)

    # has to match what came in the HDF5 file
    # Integration time within a chunk
    integration_time = args.integration_time
    # Number of integrations to average (??), Integration time after averaging
    time_average = args.time_average
    log.info(
        "Each chunk has %d integrations of duration %.2f (s)",
        n_time_samples,
        integration_time,
    )
    chunk_start_times = get_chunk_start_times(
        args.time_range, integration_time, n_time_samples, log
    )

    # Frequency and bandwidth related set-up
    frequency = numpy.linspace(
        args.frequency_range[0],
        args.frequency_range[1],
        args.nchannels_per_chunk,
    )
    channel_bandwidth = (frequency[-1] - frequency[0]) / (
        args.nchannels_per_chunk - 1
    )
    channel_average = args.channel_average
    log.info(
        "Each chunk has %d frequency channels of width %.3f (MHz)",
        args.nchannels_per_chunk,
        channel_bandwidth * 1e-6,
    )
    channel_bandwidth = numpy.ones_like(frequency) * channel_bandwidth

    # We process the chunks (and accumulate the images)
    # in two stages to avoid a large final reduction
    vis_graph_list = []

    for chunk_start_time in chunk_start_times:
        for start_time in chunk_start_time:
            times = (
                start_time + numpy.arange(n_time_samples) * integration_time
            )

            # Perform the simulation for this chunk
            vis_graph = rsexecute.execute(simulate_rfi_image_prop)(
                sub_low,
                times,
                frequency,
                rfi_signal,
                channel_bandwidth,
                phasecentre,
                polarisation_frame=PolarisationFrame("stokesI"),
                time_average=time_average,
                channel_average=channel_average,
                noise=noise,
                beam_gain=beam_gain,
                transmitter_coordinates=coords,
                transmitter_ids=source_ids,
                rfi_frequencies=rfi_freq_channels,
            )
            vis_graph_list.append(vis_graph)

    entire_bvis = concatenate_visibility_time_rsexecute(vis_graph_list)
    entire_bvis = rsexecute.compute(entire_bvis, sync=True)

    if args.write_ms:
        export_visibility_to_ms(args.msout, [entire_bvis])

    log.info("\nSKA LOW RFI simulation using RASCIL")
    log.info("Started at %s", start_epoch)
    log.info("Finished at %s", time.asctime())

    # Attempt to save the statistics
    rsexecute.save_statistics(name="RFI_simulation")
    rsexecute.close()

    write_settings(vars(args), settings_file)


def main():
    """Main function"""
    parser = cli_parser()
    args = parser.parse_args()
    rfi_simulation(args)


if __name__ == "__main__":
    main()
