# pylint: disable=too-many-statements,too-many-locals
# pylint: disable=redefined-outer-name

# coding: utf-8
"""
Power spectrum for an image, adapted from Fred Dulwich code
"""
import argparse
import csv
import os

import astropy.constants as consts
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy
from rascil.processing_components.image.operations import show_image
from ska_sdp_datamodels.image.image_io_and_convert import (
    import_image_from_fits,
)
from ska_sdp_datamodels.xarray_coordinate_support import griddata_wcs
from ska_sdp_func_python.image.operations import fft_image_to_griddata_with_wcs


def radial_profile(image, centre=None):
    """Generate radial profile"""
    if centre is None:
        centre = (image.shape[0] // 2, image.shape[1] // 2)
    x, y = numpy.indices((image.shape[0:2]))
    r = numpy.sqrt((x - centre[0]) ** 2 + (y - centre[1]) ** 2)
    r = r.astype(int)
    return numpy.bincount(r.ravel(), image.ravel()) / numpy.bincount(r.ravel())


def cli_parser():
    """CLI argument parser"""
    parser = argparse.ArgumentParser(
        description="Display power spectrum of image"
    )
    parser.add_argument("--image", type=str, default=None, help="Image name")
    parser.add_argument(
        "--signal_channel",
        type=int,
        default=None,
        help="Channel containing both signal and noise",
    )
    parser.add_argument(
        "--noise_channel",
        type=int,
        default=0,
        help="Channel containing noise only",
    )
    parser.add_argument(
        "--resolution",
        type=float,
        default=5e-4,
        help="Resolution in radians needed for conversion to K",
    )
    return parser


def power_spectrum(args):
    """Generate power spectrum"""
    basename = os.path.basename(os.getcwd())

    print("Display power spectrum of an image")

    im = import_image_from_fits(args.image)

    nchan = im["pixels"].shape[0]

    if args.signal_channel is None:
        signal_channel = nchan // 2
    else:
        signal_channel = args.signal_channels

    noise_channel = args.noise_channel
    resolution = args.resolution

    plt.clf()
    show_image(im, chan=signal_channel)
    plt.title(f"Signal image {basename}")
    plt.savefig(f"simulation_image_channel_{signal_channel}.png")
    plt.show()
    plt.clf()
    show_image(im, chan=noise_channel)
    plt.title(f"Noise image {basename}")
    plt.savefig(f"simulation_noise_channel_{signal_channel}.png")
    plt.show()

    print(im)
    imfft = fft_image_to_griddata_with_wcs(im)
    print(imfft)

    omega = numpy.pi * resolution**2 / (4 * numpy.log(2.0))
    wavelength = consts.c / numpy.average(im.frequency)
    kperjy = 1e-26 * wavelength**2 / (2 * consts.k_B * omega)

    im_spectrum = imfft.copy()
    im_spectrum["pixels"].data = kperjy.value * numpy.abs(imfft["pixels"].data)
    noisy = numpy.max(im_spectrum["pixels"].data[noise_channel, 0]) > 0.0

    profile = radial_profile(im_spectrum["pixels"].data[signal_channel, 0])
    noise_profile = radial_profile(
        im_spectrum["pixels"].data[noise_channel, 0]
    )

    plt.clf()
    cellsize_uv = numpy.abs(griddata_wcs(imfft).wcs.cdelt[0])
    lambda_max = cellsize_uv * len(profile)
    lambda_axis = numpy.linspace(cellsize_uv, lambda_max, len(profile))
    theta_axis = 180.0 / (numpy.pi * lambda_axis)
    plt.plot(theta_axis, profile, color="blue", label="signal")
    if noisy:
        plt.plot(theta_axis, noise_profile, color="red", label="noise")
    plt.gca().set_title(f"Power spectrum of image {basename}")
    plt.gca().legend()
    plt.gca().set_xlabel(r"$\theta$")
    plt.gca().set_ylabel(r"$K^2$")
    plt.gca().set_xscale("log")
    plt.gca().set_yscale("log")
    plt.gca().set_ylim(1e-6 * numpy.max(profile), 2.0 * numpy.max(profile))
    plt.tight_layout()
    plt.savefig(f"power_spectrum_profile_channel_{signal_channel}.png")
    plt.show()

    filename = "power_spectrum_channel.csv"
    results = []
    for row, th_axis in enumerate(theta_axis):
        result = {
            "inverse_theta": th_axis,
            "profile": profile[row],
            "noise_profile": noise_profile[row],
        }
        results.append(result)

    with open(filename, "w", encoding="utf-8") as csvfile:
        writer = csv.DictWriter(
            csvfile,
            fieldnames=results[0].keys(),
            delimiter=",",
            quotechar="|",
            quoting=csv.QUOTE_MINIMAL,
        )
        writer.writeheader()
        for result in results:
            writer.writerow(result)
        csvfile.close()


if __name__ == "__main__":
    parser = cli_parser()
    args = parser.parse_args()
    power_spectrum(args)
