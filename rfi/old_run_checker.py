"""
Is used by each of the scripts in the rfi_sim file to both write out the cmd
args to file and to check new cmd args against old saved args. So that if a new
run has the same settings as the old run, it will not run again unnecessarily.
"""

import csv
import logging
import os
import sys

LOG = logging.getLogger("rfi-logger")


def write_settings(args, settings_file):
    """
    Writes csv file for current runs settings.
    :param args: Dict of user defined arguments
    :param settings_file: file name for setting file
    """

    LOG.info("Writing settings to: %s.", settings_file)

    directory = os.path.dirname(settings_file)
    if not os.path.exists(directory):
        os.makedirs(directory)

    with open(settings_file, "w", encoding="utf-8") as outfile:
        writer = csv.writer(outfile)
        for key, val in args.items():
            writer.writerow([key, val])


def check_for_old_run(args, settings_file):
    """
    Checks for old csv file from previous run and if present check the
    args values to see if there is a difference. If there is no difference
    then the script exits with zero status.
    :param args: Dict of user defined arguments
    :param settings_file: file name for setting file
    """

    LOG.info("Checking for old settings file: %s", settings_file)

    if os.path.isfile(settings_file):
        LOG.info("Old settings file found.")

        new_settings = args.copy()

        old_settings = {}
        with open(settings_file, "r", encoding="utf-8") as infile:
            for row in csv.reader(infile):
                old_settings[row[0]] = row[1]

        # Needed as the old settings are read in as strings
        # Converting the new settings to strings saves having to
        # check individually every variable type.
        for key in new_settings.keys():
            new_settings[key] = str(new_settings[key])

        if old_settings == new_settings:
            LOG.info("New settings match old settings: Exiting.")
            LOG.info(
                "If this is incorrect behaviour, please delete '%s' "
                "and run again.",
                settings_file,
            )
            sys.exit(0)
        else:
            LOG.info("Old settings do not match new settings.")
            LOG.info("Running RFI program.")
            return

    else:
        LOG.info("No old settings found.")
        LOG.info("Running RFI program.")
        return
