#!/usr/bin/env python
# coding: utf-8

# pylint: disable=invalid-name,too-many-arguments
# pylint: disable=too-many-locals,line-too-long
# pylint: disable=too-many-positional-arguments

"""
Core pycraf calculation of the propagation attenuation of
a transmitter to each of the SKA low stations defined in
the input workbook. Returns a 2D float array of frequency
and attenuation values.
"""

# Original by Federico Di Vruno
# Alterations by DMF

import logging
import time

import numpy as np
from astropy import (
    units as u,  # must be after pycraf to avoid astropy import conflicts
)
from pycraf import antenna, geometry, pathprof
from ska_sdp_datamodels.configuration import select_configuration

from rfi.common_tools import generate_ska_antenna_configuration

# allow download of missing SRTM data
pathprof.SrtmConf.set(download="missing", server="viewpano")

LOG = logging.getLogger("rfi-logger")


def calc_prop_atten(
    freq,
    temp,
    press,
    timepercent,
    h_tg,
    h_rg,
    zone_t,
    zone_r,
    N_freqs,
    freq_start,
    freq_end,
    hprof_step,
    tx_lon,
    tx_lat,
    diam,
    direct,
    rmax,
    station_skip=None,
    locations_file="./telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_"
    "SKALA4_spot_frequencies.tm/layout_wgs84.txt",
    polarization=None,
    srtm_directory="./data/SRTM_data",
):
    """
    Use Pycraf to calculate the propagation attenuation using the fast path method.

    :param freq: transmitter frequency [MHz]
            (Note: pycraf doc says, this should be GHz. Does this matter?)
    :param temp: assumed ambient temperature at path midpoint [K]. See pycraf documentation
    :param press: assumed ambient pressure at path midpoint [hPa]. See pycraf documentation
    :param timepercent: time percentage [%]. See pycraf documentation and P.452 report
    :param h_tg: transmitter (DTV antenna) height above ground [m]
    :param h_rg: assumed height of receiver above ground [m]. See pycraf documentation
    :param zone_t: list of clutter types for transmitter. See pycraf documentation
    :param zone_r: list of clutter types for receiver. See pycraf documentation
    :param N_freqs: number of frequency channels
    :param freq_start: start of frequency range [MHz]
    :param freq_end: end of frequency range [MHz]
    :param hprof_step: distance resolution of the calculated solution. See pycraf documentation
    :param tx_lon: transmitter (DTV antenna) longitude
    :param tx_lat: transmitter (DTV antenna) latitude
    :param diam: assumed diameter of transmitter [m]. See pycraf documentation
    :param direct: transmitter antenna pattern ?
    :param locations_file: absolute path to file with SKA antenna locations
    :param polarization: 1 if transmitter polarisation is 'V' (vertical), 0 if 'H' (horizontal)
    :param srtm_directory: absolute path to directory for the SRTM files required by
            pycraf for terrain information

    :return: three numpy arrays:
        - antenna attenuations; dim = [Nant, Nfreq] Note: Nant is the number of SKA stations
        - corresponding frequencies; dim = [Nfreq]
        - array containing azimuth, elevation, distance for each station; dim = [Nant]
            (i.e. number of SKA stations) and the station ids used for the attenuation calculations
    """  # noqa: E501
    pathprof.SrtmConf.set(srtm_dir=srtm_directory)

    freq = freq * u.MHz
    temperature = temp * u.K
    pressure = press * u.hPa
    timepercent = timepercent * u.percent  # see P.452 for explanation

    # height of the receiver and transmitter above gnd
    h_tg, h_rg = h_tg * u.m, h_rg * u.m
    hprof_step = hprof_step * u.m  # resolution of solution

    # transmitter location?
    lon_tx, lat_tx = tx_lon * u.deg, tx_lat * u.deg

    # For LOW, get list of station locations from the txt file
    # then sub-select the ones which are at the right place
    # from the array centre
    all_long_x, all_lat_x = np.loadtxt(
        locations_file, delimiter=",", unpack=True
    )
    low = generate_ska_antenna_configuration(True, locations_file, rmax)
    LOG.info("Maximum station distance from array centre: %s m", rmax)

    nants_start = len(low.names)  # initial number of stations (antennas)
    LOG.info("There are %d stations", nants_start)

    # apply station skipping
    sub_low = select_configuration(low, low.names[::station_skip])
    LOG.info("There are %d stations after decimation", len(sub_low.names))

    # TODO: differences in amplitude in the final visibility
    #   are about an order of magnitude need to investigate the reason,
    #   and once satisfied, update code to use NEW instead of OLD
    # *NEW*: taking the stations that match by name in the configuration
    # stations_to_use = [int(x.strip("LOW_")) - 1 for x in low.names.values]
    # long_x = all_long_x[stations_to_use]
    # lat_x = all_lat_x[stations_to_use]

    # *OLD*: taking the first x stations from the lat/long coordinate list
    long_x = all_long_x[: len(sub_low.names)]
    lat_x = all_lat_x[: len(sub_low.names)]

    n_ant = len(long_x)
    # when the coordinates file contains fewer entries
    # than stations in the configuration we need to know
    # which of those stations (and how many) could be used
    # TODO: this will probably be different once we use the proper stations
    station_ids = sub_low.names.data[:n_ant]

    # Find the attenuation at each position of SKA antennas
    # using path info for each freq:
    attenpath_calc_start = time.time()
    LOG.info(
        "Calculating frequency dependent attenuation for "
        "each station from path..."
    )
    freqs = (
        np.logspace(np.log10(freq_start), np.log10(freq_end), N_freqs) * u.MHz
    )

    diameter_fl = diam * u.m

    Atten_ant = np.zeros([n_ant, N_freqs])
    angular_dist_ant = np.zeros([n_ant, 3])
    for k in range(n_ant):
        hprof_cache = pathprof.height_path_data(
            lon_tx, lat_tx, long_x[k] * u.deg, lat_x[k] * u.deg, hprof_step
        )

        # Calls the default ITU-R 452-16 Pycraf propagation model
        pprop_fl_ras = pathprof.PathProp(
            freq,
            temperature,
            pressure,
            lon_tx,
            lat_tx,
            long_x[k] * u.deg,
            lat_x[k] * u.deg,
            h_tg,
            h_rg,
            hprof_step,
            timepercent,
            zone_t=zone_t,
            zone_r=zone_r,
            polarization=polarization,
        )

        ang_dist = geometry.true_angular_distance(
            pprop_fl_ras.alpha_tr,
            pprop_fl_ras.eps_pt,
            0.0 * u.deg,
            0.0 * u.deg,
        )

        # apparent coordinates per ska station: azimuth, elevation, distance
        angular_dist_ant[k] = [
            pprop_fl_ras.alpha_rt.value,
            pprop_fl_ras.eps_pr.value,
            pprop_fl_ras.distance.value,
        ]

        for i in range(N_freqs):
            wavelen_fl = freqs[i].to(u.m, equivalencies=u.spectral())
            G_max_fl = antenna.fl_G_max_from_size(diameter_fl, wavelen_fl)

            if direct == "OD":
                G_eff = G_max_fl
            else:
                G_eff = antenna.fl_pattern(
                    ang_dist, diameter_fl, wavelen_fl, G_max_fl
                )

            results = pathprof.atten_path_fast(
                freqs[i],
                temperature,
                pressure,
                h_tg,
                h_rg,
                timepercent,
                hprof_cache,
                polarization,
            )

            Atten_ant[k, i] = results["L_b"][-1].value - G_eff.value

    attenpath_calc_end = time.time()
    LOG.info(
        "Completed in %f sec.", (attenpath_calc_end - attenpath_calc_start)
    )

    return Atten_ant, freqs, angular_dist_ant, station_ids
