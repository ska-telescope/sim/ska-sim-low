#!/usr/bin/env python
# coding: utf-8
# flake8: noqa

"""
Test pycraf using SKA LOW coordinates.

The following code is adapted from scripts originally provided by Frederico di Vruno. These take an SKA LOW
configuration in workbook format and calculate the propagation attenuation from a transmitter. These utilise the Pycraf
python module implementations of the ITU-R recommendations for calculating attenuation between transmitters and
receivers. This outputs a heatmap of the attenuation identifying specific antennas as well as the attenuation as a
function of frequency. This script is designed to be an test case providing an introduction to the key pycraf tools used
 to calculate attenuation. For further details, please see the inputs in the code below and refer to the
 Pycraf documentation.
"""

import time
from collections import OrderedDict, namedtuple

import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from pycraf import conversions as cnv
from pycraf import pathprof

# allow download of missing SRTM data:
srtm_directory = "../data/SRTM_data/"
pathprof.SrtmConf.set(download="missing", server="viewpano")
pathprof.SrtmConf.set(srtm_dir=srtm_directory)
"""
===========================================
    Coordinates
    Set the coordinates of the transmitter.
===========================================
"""
# SKA_LOW offset 1km
# tx_lon, tx_lat = 116.747085, -26.82510333
# site, lon_tx, lat_tx = 'DTV1km', tx_lon*u.deg, tx_lat * u.deg

# SKA_LOW offset 10km
# tx_lon, tx_lat = 116.747085, -26.743875
# site, lon_tx, lat_tx = 'DTV10km', tx_lon*u.deg, tx_lat * u.deg

# SKA_LOW offset 100km
# tx_lon, tx_lat = 116.747085, -25.931551666
# site, lon_tx, lat_tx = 'DTV100km', tx_lon*u.deg, tx_lat * u.deg

# DTV Bickley site Perth 180MHz Seven
tx_lon, tx_lat = 116.084166666667, -32.0083333333333
site, lon_tx, lat_tx = "DTVSeven", tx_lon * u.deg, tx_lat * u.deg

# Centre of low telescope
# rx_name, rx_lon, rx_lat = 'Low_E4', 116.4525771, -26.60055525 # 116.75, -26.835

"""
===========================================
    Sites of particular interest
     
===========================================
"""

Site = namedtuple("sitetype", ["name", "coord", "pixoff", "color"])
sites = OrderedDict(
    [
        # ID: tuple(Name, (lon, lat), Type, height, diameter, (xoff, yoff), color)
        # ('Tx', Site('Tx', (u.Quantity(lon_tx).value, u.Quantity(lat_tx).value), (20, +30), 'k')),
        (
            "Low_E4",
            Site("Low_E4", (116.4525771, -26.60055525), (60, -20), "k"),
        ),  # ensure receiver first
        (
            "Trans",
            Site(site, (tx_lon, tx_lat), (60, -20), "k"),
        ),  # must be 'Trans' for transmitter
    ]
)

for site_id, site in sites.items():
    if site.name == "Low_E4":
        map_cen_lon = site.coord[0]
        map_cen_lat = site.coord[1]
        rx_cen_lon = map_cen_lon
        rx_cen_lat = map_cen_lat


"""
===========================================
    Antennas locations
     
===========================================
"""

# For LOW, get list of station name/locations from txt file
antenna_locations = "../../data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt"
long_x, lat_x = np.loadtxt(antenna_locations, delimiter=",", unpack=True)

"""
=======================================
Start of inputs

Map size inputs
=======================================
"""

do_map_solution = (
    True  # set to variable if want map solutions to be calculated and map
)
# plots produced. Unless set below will provide full resolution plots localised to receivers.

doplotAll = True  # True for plots and calculated maps to be same area i.e. cover area over transmitter and receiver.

choose_resolution = True  # if doplotAll True, by default a full resolution full spatial extent plot
# (choose_resolution=False) or specify lower resolution options for quicker plotting
# (choose_resolution=True), specify options below.

hprof_step = (
    10000 * u.m
)  # Sets distance resolution for all attenuation calculations.

if do_map_solution:
    # Produce plots locally to receiver at full resolution
    # map 0.5deg squared, resolution 3.6 asecs,
    map_size_x = 2.0
    map_size_y = 2.0
    map_size_lon, map_size_lat = map_size_x * u.deg, map_size_y * u.deg
    map_resolution = 0.1 * u.deg  # minimum resolution allowed 0.1

    atten_map_size_lon = map_size_lon
    atten_map_size_lat = map_size_lat
    atten_map_resolution = map_resolution
    atten_hprof_step = hprof_step  # overrides map_resolution when given
    map_extent = [
        map_cen_lon - (0.5 * map_size_x),
        map_cen_lat - (0.5 * map_size_y),
        map_cen_lon + (0.5 * map_size_x),
        map_cen_lat + (0.5 * map_size_y),
    ]
    contour_levels = [
        20,
        40,
        60,
        80,
        100,
        120,
        140,
        160,
        180,
    ]  # Attenuation contour levels for plot

    if doplotAll:
        # Need to ensure this map is large enough to cover all receivers and transmitter and lower resolution
        map_size_x = 12.0
        map_size_y = 12.0
        atten_map_size_lon = map_size_x * u.deg
        atten_map_size_lat = map_size_y * u.deg
        atten_map_resolution = map_resolution
        atten_hprof_step = hprof_step  # keep full solution resolution
        map_cen_lon = tx_lon
        map_cen_lat = tx_lat
        # map_extent = [map_cen_lon-(0.5*map_size_x), map_cen_lat-(0.5*map_size_y), map_cen_lon+(0.5*map_size_x),
        #        map_cen_lat+(0.5*map_size_y)]
        if choose_resolution:
            # Need to ensure this map is large enough to cover all receivers and transmitter and lower resolution
            map_size_x = 12.0
            map_size_y = 12.0
            atten_map_size_lon = map_size_x * u.deg
            atten_map_size_lat = map_size_y * u.deg
            atten_map_resolution = 0.1 * u.deg
            atten_hprof_step = (
                15000 * u.m
            )  # overrides map_resolution when given
            print("Full distance plot at restricted resolution")
        else:
            print("Full distance plot")
    else:
        print("Localised plot")

"""
=======================================
Attenuation calculation inputs
=======================================
"""
# Here input the parameters for the ITU-R 452-16 model

freq = 180 * u.MHz
omega = 0.0 * u.percent  # fraction of path over sea
temperature = 290.0 * u.K
pressure = 1013.0 * u.hPa
timepercent = 0.02 * u.percent  # see P.452 for explanation
h_tg, h_rg = (
    3 * u.m,
    2.1 * u.m,
)  # height of the receiver and transmitter above gnd
G_t, G_r = 0 * cnv.dBi, 0 * cnv.dBi
zone_t, zone_r = pathprof.CLUTTER.UNKNOWN, pathprof.CLUTTER.UNKNOWN
polarization = 0  # 0 horizontal, 1 vertical
# clutter type for transmitter/receiver
"""
=======================================
Path attenuation additional inputs
=======================================
"""
N_freqs = 10  # no. of channels
freq_start, freq_end = 170, 180  # frequency range MHz

"""
======================================
End of inputs
======================================
"""

if do_map_solution:
    """
    ===========================================
         Get the map height information

    ===========================================
    """

    terrain_start = time.time()
    print("Getting terrain map information...")

    # get the data for the terrain map of region around location above i.e. transmitter
    # covers area defined above
    # mostly just for getting the max/min for plotting the terrain below
    lons, lats, heightmap = pathprof.srtm_height_map(
        map_cen_lon * u.deg,
        map_cen_lat * u.deg,
        atten_map_size_lon,
        atten_map_size_lat,
        map_resolution=atten_map_resolution,
        hprof_step=atten_hprof_step,
    )

    terrain_end = time.time()
    print("Completed in %f sec." % (terrain_end - terrain_start))

    """
    ===========================================
        Plot terrain information
         
    ===========================================
    """
    _lons = lons.to(u.deg).value
    _lats = lats.to(u.deg).value
    _heightmap = heightmap.to(u.m).value

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_axes((0.0, 0.0, 1.0, 1.0))

    vmin, vmax = np.min(_heightmap), np.max(_heightmap)

    # Produces terrain colormap and norm for plotting
    terrain_cmap, terrain_norm = pathprof.terrain_cmap_factory(
        sealevel=vmin, vmax=vmax
    )

    if doplotAll:
        ax.set_aspect(abs(_lons[-1] - _lons[0]) / abs(_lats[-1] - _lats[0]))
        ax.set_ylim([_lats[0], _lats[-1]])
        ax.set_xlim([_lons[0], _lons[-1]])
        nextent = (_lons[0], _lons[-1], _lats[0], _lats[-1])
        _lons_fin = _lons
        _lats_fin = _lats
    else:
        # Limit to receiver area
        ax.set_aspect(
            abs(map_extent[2] - map_extent[0])
            / abs(map_extent[3] - map_extent[1])
        )
        # nextent = (map_extent[0], map_extent[2], map_extent[1], map_extent[3])
        lon_ind = np.where((_lons > map_extent[0]) & (_lons < map_extent[2]))[
            0
        ]
        lat_ind = np.where((_lats > map_extent[1]) & (_lats < map_extent[3]))[
            0
        ]
        _lons_fin = _lons[lon_ind]
        _lats_fin = _lats[lat_ind]
        ax.set_ylim([_lats_fin[0], _lats_fin[-1]])
        ax.set_xlim([_lons_fin[0], _lons_fin[-1]])
        nextent = (_lons_fin[0], _lons_fin[-1], _lats_fin[0], _lats_fin[-1])

    cim = ax.imshow(
        _heightmap,
        origin="lower",
        interpolation="nearest",
        cmap=terrain_cmap,
        norm=terrain_norm,
        vmin=vmin,
        vmax=vmax,
        extent=nextent,
    )

    ax.set_xlabel("Longitude [deg]")
    ax.set_ylabel("Latitude [deg]")
    plt.title("Terrain map")

    cbar = fig.colorbar(cim, fraction=0.046, pad=0.04)
    cbar.set_label("Height (m)", rotation=270)

    # Annotate the sites of interest in the map
    # Ignore transmitter unless full plot
    for site_id, site in sites.items():
        color = site.color
        color = "k"
        if site_id != "Trans" or doplotAll:
            ax.annotate(
                site.name,
                xy=site.coord,
                xytext=site.pixoff,
                textcoords="offset points",
                color=color,
                arrowprops=dict(arrowstyle="->", color=color),
            )
            ax.scatter(site.coord[0], site.coord[1], marker="o", c="k")

    # Place a marker in each of the SKA antennas in the map
    # For LOW these are station positions
    for i in range(len(long_x)):
        ax.scatter(long_x[i], lat_x[i], marker="o", c="k")

    plt.savefig("terrain_map.png", bbox_inches="tight", pad_inches=0)
    print("Terrain map completed")

    """
    ===========================================
        Calculate the attenuation map
         
    ===========================================
    """

    attenmap_start = time.time()
    print("Calculating attenuation map...")

    # Faster approach

    # Calculates auxiliary maps and height profiles for atten_map_fast
    # Height profile data independent of frequency, so can be cached
    # lon_tx,lat_tx transmitter
    # returns dictionary containing lon_t, lat_t, map_size_lon, map_size_lat, hprof_step
    # xcoords, ycoords (coords first row/col in map) also others, input directly into attenmap_fast

    # Rough estimate to ensure map attenuation calculation reaches receiver from transmitter when doplotAll is False
    if not doplotAll:
        tx_map_size_lon = (
            2 * abs(abs(rx_cen_lon) - abs(tx_lon)) * u.deg + atten_map_size_lon
        )
        tx_map_size_lat = (
            2 * abs(abs(rx_cen_lat) - abs(tx_lat)) * u.deg + atten_map_size_lat
        )

        # print(tx_map_size_lon, tx_map_size_lat)
    else:
        tx_map_size_lon = atten_map_size_lon
        tx_map_size_lat = atten_map_size_lat
    hprof_cache = pathprof.height_map_data(
        lon_tx,
        lat_tx,
        tx_map_size_lon,
        tx_map_size_lat,
        map_resolution=atten_map_resolution,
        zone_t=zone_t,
        zone_r=zone_r,
    )
    results = pathprof.atten_map_fast(
        freq,
        temperature,
        pressure,
        h_tg,
        h_rg,
        timepercent,
        hprof_cache,  # dict_like
        polarization,
    )

    _lons = hprof_cache["xcoords"]
    _lats = hprof_cache["ycoords"]
    _total_atten = results[
        "L_b"
    ]  # L_b is the total attenuation, considering all the factors.
    # print(_total_atten.shape,_total_atten[0])
    _fspl_atten = results["L_b0p"]  # considers only the free space loss

    attenmap_end = time.time()
    print("Completed in %f sec." % (attenmap_end - attenmap_start))

    """
    ===========================================
        Plot the resulting attenuation map
        
    ===========================================
    """

    # Plot the results selected
    vmin, vmax = 90, 200  # Max and min scale

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_axes((0.0, 0.1, 1.0, 0.8))
    # cbax = fig.add_axes((0.3, 0., 0.4, .02))

    if doplotAll:
        ax.set_aspect(abs(_lons[-1] - _lons[0]) / abs(_lats[-1] - _lats[0]))
        nextent = (_lons[0], _lons[-1], _lats[0], _lats[-1])
        _total_atten_fin = _total_atten
        _lons_fin = _lons
        _lats_fin = _lats
    else:
        # Limit to receiver area
        ax.set_aspect(
            abs(map_extent[2] - map_extent[0])
            / abs(map_extent[3] - map_extent[1])
        )
        # nextent = (map_extent[0], map_extent[2], map_extent[1], map_extent[3])
        lon_ind = np.where((_lons > map_extent[0]) & (_lons < map_extent[2]))[
            0
        ]
        lat_ind = np.where((_lats > map_extent[1]) & (_lats < map_extent[3]))[
            0
        ]
        _lons_fin = _lons[lon_ind]
        _lats_fin = _lats[lat_ind]
        nextent = (_lons_fin[0], _lons_fin[-1], _lats_fin[0], _lats_fin[-1])
        _total_atten_fin = _total_atten[np.ix_(lat_ind, lon_ind)]
        # print(_total_atten_fin) #x[np.ix_(row_indices,col_indices)]

    cim = ax.imshow(
        _total_atten_fin.to(cnv.dB).value,
        origin="lower",
        interpolation="nearest",
        cmap="inferno_r",
        vmin=vmin,
        vmax=vmax,
        extent=nextent,
    )

    # colourbar
    # cbar = fig.colorbar(cim, cax=cbax, orientation='horizontal', )
    cbar = fig.colorbar(
        cim, orientation="horizontal", fraction=0.046, pad=0.08
    )
    cbar.set_label(r"Path propagation loss (dB)", color="black")

    ctics = np.arange(vmin, vmax, 10)
    cbar.set_ticks(ctics)
    ax.set_xlabel("Longitude [deg]")
    ax.set_ylabel("Latitude [deg]")
    ax.set_autoscale_on(False)

    # Annotate the coordinates of the interest sites and searches for the attenuation levels.
    lat_mesh, lon_mesh = np.meshgrid(
        _lats_fin, _lons_fin
    )  # hace un mesh para buscar los puntos
    # Makes a mesh to find the points
    for site_id, site in sites.items():
        color = site.color
        color = "b"
        aux = abs(lat_mesh - site.coord[1]) + abs(lon_mesh - site.coord[0])
        i, j = np.unravel_index(aux.argmin(), aux.shape)
        if site_id != "Trans" or doplotAll:
            ax.annotate(
                site.name
                + " att: "
                + str(_total_atten_fin.to(cnv.dB).value[j, i])[0:6]
                + " dB",
                xy=site.coord,
                xytext=site.pixoff,
                textcoords="offset points",
                color=color,
                arrowprops=dict(arrowstyle="->", color=color),
            )
            ax.scatter(site.coord[0], site.coord[1], marker="o", c="b")

    for i in range(
        len(long_x)
    ):  # Puts a mark in every place there is an antenna
        ax.scatter(long_x[i], lat_x[i], marker="o", c="w")

    plt.title("Attenuation map, Freq = " + str(freq))

    # ax.xaxis.tick_top()
    # ax.xaxis.set_label_position('top')
    plt.tight_layout()
    # plt.show()
    plt.savefig("path_attenuation.png", bbox_inches="tight")

    """
    ===========================================
        Plot the terrain map with attenuation contours
        Also add the contours for Free Space Path Loss. 
        
    ===========================================
    """

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_axes((0.0, 0.1, 1.0, 0.8))
    vmin, vmax = np.min(_heightmap), np.max(_heightmap)

    # terrain_cmap,terrain_norm = pathprof.terrain_cmap_factory(sealevel=vmin,vmax=vmax)

    cim = ax.imshow(
        _heightmap,
        origin="lower",
        interpolation="nearest",
        cmap=terrain_cmap,
        norm=terrain_norm,
        vmin=vmin,
        vmax=vmax,
        extent=nextent,
    )

    cbar = fig.colorbar(cim, fraction=0.046, pad=0.04)
    cbar.set_label("Height (m)", rotation=270)

    _fspl_atten = results["L_b0p"]

    ax.set_xlabel("Longitude [deg]")
    ax.set_ylabel("Latitude [deg]")
    plt.title("Terrain map with attenuation contours")

    ax.contour(
        _total_atten_fin.to(cnv.dB).value,
        levels=contour_levels,
        linestyles="-",
        origin="lower",
        extent=nextent,
        alpha=1,
    )

    """
    ax.contour(_fspl_atten.to(cnv.dB).value, levels=[100],
               colors=['red'], linestyles='-',
               origin='lower',
               extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
               alpha=1)

    ax.contourf(_total_atten.to(cnv.dB).value, levels=[0,90],
               colors='b', linestyles='-',
               origin='lower',
               extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
               alpha=0.2)

    ax.contourf(_total_atten.to(cnv.dB).value, levels=[120,130],
               colors='r', linestyles='-',
               origin='lower',
               extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
               alpha=0.4)

    """

    # Annotate the coordinates of the interest sites and searches for the attenuation levels.
    lat_mesh, lon_mesh = np.meshgrid(
        _lats_fin, _lons_fin
    )  # hace un mesh para buscar los puntos
    # Makes a mesh to find the points
    for site_id, site in sites.items():
        color = site.color
        color = "b"
        aux = abs(lat_mesh - site.coord[1]) + abs(lon_mesh - site.coord[0])
        i, j = np.unravel_index(aux.argmin(), aux.shape)
        if site_id != "Trans" or doplotAll:
            ax.annotate(
                site.name
                + " att: "
                + str(_total_atten_fin.to(cnv.dB).value[j, i])[0:6]
                + " dB",
                xy=site.coord,
                xytext=site.pixoff,
                textcoords="offset points",
                color=color,
                arrowprops=dict(arrowstyle="->", color=color),
            )
            ax.scatter(site.coord[0], site.coord[1], marker="o", c="b")

    # Position the SKA antennas in the map
    for i in range(len(long_x)):
        ax.scatter(long_x[i], lat_x[i], marker="o", c="k")

    if doplotAll:
        ax.set_ylim([_lats[0], _lats[-1]])
        ax.set_xlim([_lons[0], _lons[-1]])
    else:
        ax.set_ylim(map_extent[1], map_extent[3])
        ax.set_xlim(map_extent[0], map_extent[2])
    plt.tight_layout()
    # plt.show()
    plt.savefig("pathloss_terrain.png", bbox_inches="tight")
    print("Finished plotting attenuation maps")

    """
    ===========================================
         Find the attenuation at each position of SKA antennas using the map info
    ===========================================
    """

    attenmapcalc_start = time.time()
    print("Calculating attenuation at each station from map...")

    lat_mesh, lon_mesh = np.meshgrid(_lats, _lons)  # mesh in lats and longs
    for k in range(len(long_x) - 1):
        aux = abs(lat_mesh - lat_x[k]) + abs(lon_mesh - long_x[k])
        i, j = np.unravel_index(aux.argmin(), aux.shape)
        # print('Antenna: %s - Att: %.2f'%(name_x[k],(_total_atten.to(cnv.dB).value[j,i])))

    attenmapcalc_end = time.time()
    print("Completed in %f sec." % (attenmapcalc_end - attenmapcalc_start))


"""
===========================================
    Find the attenuation at each position of SKA antennas using path info
===========================================
"""

attenpath_calc_start = time.time()
print(
    "Calculating frequency dependent attenuation for each station from path..."
)

N_ant = len(long_x) - 1
freqs = np.logspace(np.log10(freq_start), np.log10(freq_end), N_freqs) * u.MHz
Atten_ant = np.zeros([N_ant, N_freqs])
results = []
hprof_cache = []

for k in range(N_ant):
    hprof_cache = pathprof.height_path_data(
        lon_tx, lat_tx, long_x[k] * u.deg, lat_x[k] * u.deg, hprof_step
    )
    for i in range(N_freqs):
        # print(k,i)
        results = pathprof.atten_path_fast(
            freqs[i],
            temperature,
            pressure,
            h_tg,
            h_rg,
            timepercent,
            hprof_cache,  # dict_like
            polarization,
        )
        Atten_ant[k, i] = results["L_b"][
            -1
        ].value  # gets the last value of the attenuation path.
        # _total_atten = results['L_b']  # L_b is the total attenuation, considering all the factors.
        _fspl_atten = results["L_b0p"]  # considers only the free space loss
        # print(i,k,' atten value ',Atten_ant[k,i])

# Output results to file
np.save("Attenuation_final.npy", Atten_ant)

fig = plt.figure(figsize=(10, 10))
# plt.semilogx(freqs,np.transpose(Atten_ant))
plt.plot(freqs, np.transpose(Atten_ant))
plt.xlabel("Frequency (MHz)")
plt.ylabel("Total attenuation (dB)")
plt.title("Station attenuation")
plt.grid()
plt.savefig("station_attenuation.png", bbox_inches="tight")

attenpath_calc_end = time.time()
print("Completed in %f sec." % (attenpath_calc_end - attenpath_calc_start))

"""
## Check single path calculation ##
"""

attenpath_calc_start = time.time()
print(
    "Calculating frequency dependent attenuation for each station from single path..."
)

Atten_ant = np.zeros([N_ant, N_freqs])
Atten_ant_all = np.zeros([N_ant, N_freqs])
for k in range(N_ant):
    for i in range(N_freqs):
        freq_ras = freqs[i]
        pprop_fl_ras = pathprof.PathProp(
            freq_ras,
            temperature,
            pressure,
            lon_tx,
            lat_tx,
            long_x[k] * u.deg,
            lat_x[k] * u.deg,
            h_tg,
            h_rg,
            hprof_step,
            timepercent,
            zone_t=zone_t,
            zone_r=zone_r,
            polarization=polarization,
        )
        G_eff = -4.7 * cnv.dBi
        tot_loss = pathprof.loss_complete(pprop_fl_ras, G_eff, G_r)
        (L_b0p, L_bd, L_bs, L_ba, L_b, L_b_corr, L) = tot_loss
        Atten_ant[k, i] = L_b.value
        Atten_ant_all[k, i] = L.value

# Output results to file
# np.save('Attenuation_final.npy', Atten_ant)

fig = plt.figure(figsize=(10, 10))
# plt.semilogx(freqs,np.transpose(Atten_ant))
plt.plot(freqs, np.transpose(Atten_ant))
plt.plot(freqs, np.transpose(Atten_ant_all))
plt.xlabel("Frequency (MHz)")
plt.ylabel("Total attenuation (dB)")
plt.title("Station attenuation")
plt.grid()
plt.savefig("Station_attenuation_single.png", bbox_inches="tight")

attenpath_calc_end = time.time()
print("Completed in %f sec." % (attenpath_calc_end - attenpath_calc_start))

print("Path calculated properties:")
print("L_b0p:    {0.value:5.2f} {0.unit} - Free-space loss".format(L_b0p))
print(
    "L_bd:     {0.value:5.2f} {0.unit} - Basic transmission loss associated with diffraction".format(
        L_bd
    )
)
print(
    "L_bs:     {0.value:5.2f} {0.unit} - Tropospheric scatter loss".format(
        L_bs
    )
)
print(
    "L_ba:     {0.value:5.2f} {0.unit} - Ducting/layer reflection loss".format(
        L_ba
    )
)
print(
    "L_b:      {0.value:5.2f} {0.unit} - Complete path propagation loss".format(
        L_b
    )
)
print(
    "L_b_corr: {0.value:5.2f} {0.unit} - As L_b but with clutter correction".format(
        L_b_corr
    )
)
print(
    "L:        {0.value:5.2f} {0.unit} - As L_b_corr but with gain correction".format(
        L
    )
)
