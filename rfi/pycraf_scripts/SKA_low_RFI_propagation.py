# pylint: disable=too-many-branches,too-many-statements
# pylint: disable=too-many-locals,too-many-arguments
# pylint: disable=invalid-name,duplicate-code
# pylint: disable=too-many-positional-arguments

"""
Uses pycraf to calculate the attenuation of the
input transmitters at each of the SKA antennas for
the required frequencies. This outputs a numpy array
(.npy file) for each transmitter in the input csv file.
"""

import argparse
import logging
import os
import pprint
import sys

import matplotlib

matplotlib.use("Agg")
from typing import Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy import (
    units as u,  # must be after pycraf to avoid astropy import conflicts
)
from numpy import ndarray
from pycraf import pathprof

from rfi.common_tools import (
    calculate_bandwidth_channel_range,
    default_or_antenna_value,
)
from rfi.old_run_checker import check_for_old_run, write_settings
from rfi.pycraf_scripts.SKA_low_pycraf_propagation import calc_prop_atten
from rfi.rfi_interface.rfi_data_cube import (
    DataCube,
    DataCubePerSource,
    RFISignal,
    RFISource,
)

LOG = logging.getLogger("rfi-logger")
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """CLI argument parser"""
    parser = argparse.ArgumentParser(description="Calculate RFI propagation")
    parser.add_argument(
        "--transmitters",
        type=str,
        default="data/transmitters/Filtered_DTV_list_1.csv",
        help="Location of input csv file containing transmitter properties.",
    )
    parser.add_argument(
        "--set_freq",
        type=str,
        default="False",
        help="Choose the central frequency with --freq, "
        "otherwise read it from the csv file",
    )
    parser.add_argument(
        "--freq", type=float, default=177.5, help="Central frequency (MHz)"
    )
    parser.add_argument(
        "--set_bandwidth",
        type=str,
        default="True",
        help="Choose the bandwidth with --bandwidth, "
        "otherwise read it from the csv file",
    )
    parser.add_argument(
        "--bandwidth", type=float, default=7.0, help="Bandwidth (MHz)"
    )
    parser.add_argument(
        "--n_channels",
        type=int,
        default=3,
        help="Number of frequency channels. "
        "(Must match nchannels_per_chunk for RFI simulation run)",
    )
    parser.add_argument(
        "--frequency_range",
        type=float,
        nargs=2,
        default=[170.5, 184.5],
        help="Frequency range (MHz)",
    )
    parser.add_argument(
        "--az_calc",
        type=str,
        default="True",
        help="Calculate and output the Az/El of the transmitter",
    )
    parser.add_argument(
        "--trans_out",
        type=str,
        default="infile",
        help="Name of output transmitter list. File-type not required. "
        "If 'infile' will supplement the input name of the input "
        "transmitter file. If not full path, it will be written to "
        "output_dir directory.",
    )
    parser.add_argument(
        "--non_below_el",
        type=str,
        default="True",
        help="If transmitter elevation to array centre < 0 deg, "
        "remove from list and do not simulate.",
    )
    parser.add_argument(
        "--srtm_directory",
        type=str,
        default="../data/SRTM_data",
        help="Directory for the SRTM files required by pycraf "
        "for terrain information.",
    )
    parser.add_argument(
        "--antenna_file",
        type=str,
        # pylint: disable=line-too-long
        default="../data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt",  # noqa: E501
        help="Location of text files with antenna locations",
    )
    parser.add_argument(
        "--rmax",
        type=float,
        default=100,
        help="Maximum distance of station from centre (m)",
    )
    parser.add_argument(
        "--station_skip",
        type=int,
        default=1,
        help="Decimate stations by this factor",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default="../data/attenuation",
        help="Default directory to write attenuation outputs",
    )
    parser.add_argument(
        "--array_centre",
        type=float,
        default=["Low_E4", 116.4525771, -26.60055525],
        help="List containing name, latitude (degs), "
        "longitude (degs) for the SKA Low array centre",
    )
    parser.add_argument(
        "--plot_attenuation",
        type=str,
        default=False,
        help="Output plot of attenuation values for each "
        "transmitter at the array centre.",
    )
    parser.add_argument(
        "--n_time_chunks",
        type=int,
        default=1,
        help="Number of time samples to simulate. "
        "(Same as the RASCIL-based part's "
        "--nintegrations_per_chunk arg.)",
    )
    parser.add_argument(
        "--frequency_variable",
        type=str,
        default="False",
        help="Simulate frequency-variable RFI signal?",
    )
    parser.add_argument(
        "--time_variable",
        type=str,
        default="False",
        help="Simulate time-variable RFI signal?",
    )

    # Pycraf specific
    parser.add_argument(
        "--omega",
        type=float,
        default=0.0,
        help="Fraction of path over sea. See pycraf documentation.",
    )
    parser.add_argument(
        "--temperature",
        type=float,
        default=290.0,
        help="Assumed temperature (K). See pycraf documentation.",
    )
    parser.add_argument(
        "--pressure",
        type=float,
        default=1013.0,
        help="Assumed pressure (hPa). See pycraf documentation.",
    )
    parser.add_argument(
        "--timepercent",
        type=float,
        default=0.02,
        help="Time percent. See pycraf documentation and P.452 report.",
    )
    parser.add_argument(
        "--height_rg",
        type=float,
        default=2.1,
        help="Assumed height of receiver above ground (m). "
        "See pycraf documentation.",
    )
    parser.add_argument(
        "--diam",
        type=float,
        default=2.0,
        help="Assumed diameter of transmitter (m). See pycraf documentation.",
    )
    parser.add_argument(
        "--zones",
        type=str,
        default=[pathprof.CLUTTER.UNKNOWN, pathprof.CLUTTER.UNKNOWN],
        help="List of clutter types for transmitter and "
        "receiver, default is unknown. See pycraf documentation.",
    )
    parser.add_argument(
        "--hprof_step",
        type=float,
        default=1000,
        help="Distance resolution of the calculated solution. "
        "See pycraf documentation.",
    )
    return parser


def transmitter_below_horizon(check_horizon, elevation):
    """
    Perform transmitter-horizon checks.

    :param check_horizon: if True, check where transmitter
                          is with respect to the horizon
    :param elevation: elevation of the transmitter

    :return bool: True if transmitter is below horizon
                  False if it is above horizon, or horizon limit is not checked
    """
    if check_horizon == "True":
        LOG.info("Checking if transmitter is below the horizon...")
        if elevation <= 0:
            LOG.info("Ignoring transmitter as below horizon")
            return True

        LOG.info("Above horizon, proceeding with calculation")
        return False

    LOG.info("Horizon limit not checked")
    return False


def plot_attenuation(atten_ant, freqs, outputdir, tx_name):
    """Plot attenuation data"""
    # TODO: Danielle says that plt didn't work without fig
    #  being defined and fig wouldn't work with plotting.
    #  This needs investigation.
    plt.figure(figsize=(10, 10))
    plt.plot(freqs, np.transpose(atten_ant))
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("Total attenuation (dB)")
    plt.title("Station attenuation")
    plt.grid()
    plt.savefig(
        outputdir + "Station_attenuation_" + tx_name + ".png",
        bbox_inches="tight",
    )


def construct_csv_file_path(
    transmitter_file_name, filtered_transmitters, outputdir
):
    """
    Generate the CSV file name and path where the
    transmitter information is saved

    :param transmitter_file_name: provided by user, same as args.trans_out
    :param filtered_transmitters: file containing input transmitter properties
    :param outputdir: directory to write the outputs

    :return csv_file: full path and name of csv file
    """
    file_end = ".csv"

    if transmitter_file_name == "infile":
        try:
            csv_file = (
                outputdir
                + filtered_transmitters[0:-4].rsplit("/", 1)[1]
                + "_el"
                + file_end
            )
        except IndexError:
            csv_file = (
                outputdir
                + filtered_transmitters[0:-4].rsplit("/", 1)[0]
                + "_el"
                + file_end
            )

    elif "/" in transmitter_file_name:
        if not transmitter_file_name.endswith(file_end):
            csv_file = os.path.abspath(transmitter_file_name + file_end)
        else:
            csv_file = os.path.abspath(transmitter_file_name)

    elif "\\" in transmitter_file_name:
        if not transmitter_file_name.endswith(file_end):
            csv_file = os.path.abspath(transmitter_file_name + file_end)
        else:
            csv_file = os.path.abspath(transmitter_file_name)
    else:
        if not transmitter_file_name.endswith(file_end):
            csv_file = outputdir + transmitter_file_name + file_end
        else:
            csv_file = outputdir + transmitter_file_name
    return csv_file


def write_transmitter_data_to_csv(
    transmitter_file_name, filtered_transmitters, outputdir, transmitter_dict
):
    """
    Write transmitter data into a CSV file.

    :param transmitter_file_name: provided by user, same as args.trans_out
    :param filtered_transmitters: file containing input transmitter properties
    :param outputdir: directory to write the outputs
    :param transmitter_dict: transmitter data to be written into the CSV file
    """
    df_content = []
    for key in transmitter_dict:
        entry = transmitter_dict[key]
        df_content.append(
            {
                "Name": key,
                "Longitude": entry["location"][0],
                "Latitude": entry["location"][1],
                "Power (W)": entry["power"],
                "Height (m)": entry["height"],
                "Frequency(MHz)": entry["freq"],
                "Bandwidth(MHz)": entry["bw"],
            }
        )
    out_df = pd.DataFrame(
        df_content,
        columns=[
            "Name",
            "Longitude",
            "Latitude",
            "Power (W)",
            "Height (m)",
            "Frequency(MHz)",
            "Bandwidth(MHz)",
        ],
    )

    csv_file = construct_csv_file_path(
        transmitter_file_name, filtered_transmitters, outputdir
    )
    out_df.to_csv(csv_file, index=False)


def calculate_apparent_power(
    n_times,
    frequency_range,
    transmitter_power,
    attenuation,
    frequency_variable=False,
    time_variable=False,
):
    """
    Calculate apparent transmitter power at station. Attenuation is applied
    to the transmitter source power.
    Apply frequency and/or time variability to amplitude, if needed.

    n_ants: number of SKA antennas
    n_chans: number of frequency channels

    :param n_times: number of time samples
    :param frequency_range: frequency range within which
                            transmitter emits (float) [MHz]
    :param transmitter_power: transmitter's emitted power (float)
    :param attenuation: numpy array of attenuation values [n_ants, n_chan]
    :param frequency_variable: whether signal should be frequency-variable
    :param time_variable: whether signal should be time-variable

    :return apparent_power: attenuated, apparent power of
                            the transmitter at SKA stations
    """

    if frequency_range == 0:
        frequency_range = 1.0e-6

    # this bit is from
    # rascil.processing_components.simulation.rfi.simulate_DTV_prop
    amp = np.sqrt(transmitter_power / (2.0 * frequency_range * 1e6))  # [W/Hz]

    n_chans = attenuation.shape[1]
    transmitter_signal = np.zeros([n_times, n_chans], dtype="complex")

    if time_variable:
        if frequency_variable:
            shape = [n_times, n_chans]
            transmitter_signal += np.random.normal(
                0.0, amp, shape
            ) + 1j * np.random.normal(0.0, amp, shape)
        else:
            shape = [n_times, 1]
            transmitter_signal += np.random.normal(
                0.0, amp, shape
            ) + 1j * np.random.normal(0.0, amp, shape)
    else:
        if frequency_variable:
            shape = [1, n_chans]
            transmitter_signal += np.random.normal(
                0.0, amp, shape
            ) + 1j * np.random.normal(0.0, amp, shape)
        else:
            transmitter_signal += amp

    # this is from
    # rascil.processing_components.simulation.rfi.create_propagators_prop
    # Danielle thinks that the attenuation information is per m^2,
    # the related calculations done within pycraf
    #   --> the units of propagation would be 1/m^2
    propagation = np.sqrt(
        np.power(10, attenuation / -10.0)
    )  # [n_ants, n_chans]

    # [n_times, n_ants, n_chans]
    apparent_power = (
        transmitter_signal[:, np.newaxis, ...] * propagation[np.newaxis, ...]
    )

    return apparent_power  # [W/(Hz m^2)]


def rfi_attenuation(args, save_azel_to_file=True, hdf5_only=False):
    """
    Performs the pycraf calculation for the input transmitters.
    It also takes the attenuation values produced by pycraf, and calculates the
    apparent transmitter power at the give SKA stations.

    :param args: User defined arguments
    :param save_azel_to_file: if True, save azimuth and elevation information
                              into a file for OSKAR to user;
                              set to False for tests
    :param hdf5_only: if True, do not produce any files,
                      but only return data to be saved in HDF5

    :return: data cube containing transmitter information,
             apparent source coordinates, and
             apparent transmitter power data
    """

    LOG.info("Starting LOW propagation calculation")

    pp = pprint.PrettyPrinter()
    LOG.info("Input arguments: \n %s", pp.pformat(vars(args)))

    # This dir is not tracked by git as it is initially empty.
    if not os.path.exists(args.srtm_directory):
        os.makedirs(args.srtm_directory)
    srtm_directory = os.path.abspath(args.srtm_directory)
    pathprof.SrtmConf.set(srtm_dir=srtm_directory)

    if not hdf5_only:
        settings_file = "old_settings/RFI_attenuation_settings.csv"
        check_for_old_run(vars(args), settings_file)

    filtered_transmitters = os.path.abspath(args.transmitters)
    n_freqs = args.n_channels
    freq_start = args.frequency_range[0]
    freq_end = args.frequency_range[1]
    frequency: Union[ndarray, Tuple[ndarray, Optional[float]]] = np.linspace(
        freq_start, freq_end, n_freqs
    )

    outputdir = os.path.abspath(args.output_dir)
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    if not outputdir[-1] == "/":
        outputdir = outputdir + "/"

    zone_t, zone_r = args.zones
    _, rx_lon, rx_lat = args.array_centre

    # load transmitter csv to select licence_no #
    filtered_transm_df = pd.read_csv(
        filtered_transmitters, sep=",", engine="python"
    )
    filtered_transm_df = filtered_transm_df.set_index("Licence Number")

    transmitter_dict = {}
    trans_below = []

    tv_antenna_data = []
    for _, dtv_ant in filtered_transm_df.iterrows():
        # need to set them here again, because when a new frequency
        # range is calculated via calculate_bandwidth_channel_range
        # and that returns Nones, these values are overwritten we want
        # to start with the original values for each transmitter
        n_freqs = args.n_channels
        freq_start = args.frequency_range[0]
        freq_end = args.frequency_range[1]

        tx_name = str(dtv_ant["Callsign"]) + "_" + str(dtv_ant["Site id"])
        LOG.info("\nProcessing transmitter %s", tx_name)

        tx_freq = default_or_antenna_value(
            "central frequency", args.set_freq, args.freq, dtv_ant
        )
        bandwidth = default_or_antenna_value(
            "bandwidth", args.set_bandwidth, args.bandwidth, dtv_ant
        )

        # 0 is default in pycraf
        polarization = 1 if dtv_ant["Polarisation"] == "V" else 0

        height_tx = dtv_ant["Antenna Height"]
        tx_lat = -1 * dtv_ant["Latitude (deg)"]
        tx_lon = dtv_ant["Longitude (deg)"]

        # Calculate azimuth/elevation if requested
        if args.az_calc == "True":
            pprop_fl_ras = pathprof.PathProp(
                tx_freq * u.MHz,
                args.temperature * u.K,
                args.pressure * u.hPa,
                tx_lon * u.deg,
                tx_lat * u.deg,
                rx_lon * u.deg,
                rx_lat * u.deg,
                height_tx * u.m,
                args.height_rg * u.m,
                args.hprof_step * u.m,
                args.timepercent * u.percent,
                zone_t=zone_t,
                zone_r=zone_r,
            )

            az_el = [
                pprop_fl_ras.alpha_rt.value,
                pprop_fl_ras.eps_pr.value,
                pprop_fl_ras.distance.value,
            ]
        else:
            az_el = [0, 0, 0]

        if transmitter_below_horizon(args.non_below_el, az_el[1]):
            trans_below.append(tx_name)
            continue

        # Only caclulate attenuation over specified transmitter bandwidth.
        # If full frequency range is larger than bandwidth, the number
        # of output channels will be those within the bandwidth only.
        if freq_end - freq_start > bandwidth:
            (
                n_freqs,
                freq_start,
                freq_end,
                _,
            ) = calculate_bandwidth_channel_range(
                bandwidth, frequency, tx_freq
            )
            if n_freqs is None:
                LOG.warning(
                    "Cannot obtain frequency range values over bandwidth for "
                    "transmitter %s. Skipping.",
                    tx_name,
                )
                continue
            LOG.info(
                "Calculating attenuation over transmitter bandwidth only "
                "(%.2f, %.2f MHz)",
                freq_start,
                freq_end,
            )
            LOG.info("Attenuation values for %d channels", n_freqs)
        else:
            LOG.info("Calculating attenuation for all %d channels", n_freqs)

        # Calculate propagation attenuation using pycraf
        # + return [az, el, dist] for each ska station
        atten_ant, freqs, apparent_coords, ska_ids = calc_prop_atten(
            tx_freq,
            args.temperature,
            args.pressure,
            args.timepercent,
            height_tx,
            args.height_rg,
            zone_t,
            zone_r,
            n_freqs,
            freq_start,
            freq_end,
            args.hprof_step,
            tx_lon,
            tx_lat,
            args.diam,
            dtv_ant["Antenna Pattern"],
            args.rmax,
            station_skip=args.station_skip,
            locations_file=os.path.abspath(args.antenna_file),
            polarization=polarization,
            srtm_directory=srtm_directory,
        )

        freq_range = freq_end - freq_start
        apparent_power = calculate_apparent_power(
            args.n_time_chunks,
            freq_range,
            dtv_ant["Maximum ERP (W)"],
            atten_ant,
            frequency_variable=args.frequency_variable == "True",
            time_variable=args.time_variable == "True",
        )

        transm_data = {
            "id": tx_name,
            "source_type": "tv_antenna",
            "central_frequency": dtv_ant["Frequency(MHz)"] * 1.0e6,
            "bandwidth": bandwidth * 1.0e6,
            "polarisation": dtv_ant["Polarisation"],
            "height": dtv_ant["Antenna Height"],
            "longitude": dtv_ant["Longitude (deg)"],
            "latitude": dtv_ant["Latitude (deg)"],
        }

        apparent_coords.T[2] = (
            apparent_coords.T[2] * 1.0e3
        )  # pycraf returns the distance in km, we convert it to m

        signal = {
            "source_id": tx_name,
            "time": np.array([None] * args.n_time_chunks),
            "station_id": np.array(ska_ids),
            "frequency": np.array(freqs.value * 1.0e6),
            "apparent_power": np.array(apparent_power),
            "azimuth": np.array([apparent_coords.T[0] * args.n_time_chunks]),
            "elevation": np.array([apparent_coords.T[1] * args.n_time_chunks]),
            "distance": np.array([apparent_coords.T[2] * args.n_time_chunks]),
            "apparent_coordinates": apparent_coords,
        }

        transmitter_data = RFISource(**transm_data)
        rfi_signal = RFISignal(**signal)
        tv_antenna_data.append(
            DataCubePerSource(
                transmitter_data,
                rfi_signal,
            )
        )

        transmitter_dict[tx_name] = {
            "location": [tx_lon, tx_lat],
            "power": dtv_ant["Maximum ERP (W)"],
            "height": height_tx,
            "freq": tx_freq,
            "bw": bandwidth,
            "az": az_el[0],
            "el": az_el[1],
            "dist": az_el[2],
        }

        if args.plot_attenuation == "True":
            plot_attenuation(atten_ant, freqs, outputdir, tx_name)

        if not hdf5_only:
            if save_azel_to_file:
                with open(
                    outputdir + tx_name + "_AzEl.txt", "w", encoding="utf-8"
                ) as fp:
                    print(
                        az_el[0], az_el[1], file=fp
                    )  # save azel to file for use in OSKAR

    if not hdf5_only:
        write_transmitter_data_to_csv(
            args.trans_out, filtered_transmitters, outputdir, transmitter_dict
        )

    LOG.info(
        "%d of %d transmitters below the horizon and removed "
        "from the output list",
        len(trans_below),
        len(filtered_transm_df.index),
    )

    if not hdf5_only:
        write_settings(vars(args), settings_file)

    return tv_antenna_data


def main():
    """Main function"""
    parser = cli_parser()
    args = parser.parse_args()
    rfi_results = rfi_attenuation(args)

    freqs = rfi_results[0].rfi_signal.frequency.tolist()
    station_ids = rfi_results[0].rfi_signal.station_id

    times = [None] * args.n_time_chunks

    cube = DataCube(
        times,
        freqs,
        station_ids,
        rmax=args.rmax,
        station_skip=args.station_skip,
    )
    for source_data in rfi_results:
        source_data.time_samples = np.array(times)
        cube.append_data(source_data)

    if args.output_dir[-1] != "/":
        out_dir = args.output_dir + "/"
    else:
        out_dir = args.output_dir

    cube.export_to_hdf5(f"{out_dir}/tv_transmitter_attenuation_cube.hdf5")


if __name__ == "__main__":
    main()
