#!/usr/bin/env bash

# Common settings for running an end-to-end simulation.
# See the python3 help for more details about these options.
# This is scaled to run on a laptop.

# necessary for imports to work
# we are adding the git directory ska-sim-low to PYTHONPATH
git_root_dir=`git rev-parse --show-toplevel`
export PYTHONPATH="${git_root_dir}:$PYTHONPATH"

# RA
ra=0.0
# Dec
dec=-45.0
# Number of channels per dd
n_chan="8"
# Number of channels to average
ch_avg="1"
# Pre-averaging integration time
t_int="0.2"
# Number of integrations per chunk
n_t=8
# Number of integrations to average
t_avg="1"
# Hour angle range (in hours)
t_range_beg=-0.01
t_range_end=0.01
# Max distance from array centre
rmax=100
station_skip=1

# Number of transmitters
n_trans="100"
freq_range_mhz_low="170.5" #total range -- lower end
freq_range_mhz_high="184.5" #total range -- upper end
freq_range_hz_low="170.5e6"
freq_range_hz_high="184.5e6"

atten_dir="./data/attenuation/"
beamgain_dir="./data/beam_gain/"
pycraf_file="./data/transmitters/Filtered_DTV_list_1.csv"
trans_out_file="./data/attenuation/DTV_above0_1775_${n_trans}T_${n_chan}C.csv"
antenna_workfile="./data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt"
telescope_path='data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_38m_SKALA4_spot_frequencies.tm'
hdf5_file="./data/attenuation/tv_transmitter_attenuation_cube.hdf5"

# Run

echo "--------------------------"
echo " Running pycraf script... "
echo "--------------------------"

python ./pycraf_scripts/SKA_low_RFI_propagation.py --transmitters ${pycraf_file} --output_dir ${atten_dir} \
  --n_channels ${n_chan} --frequency_range ${freq_range_mhz_low} ${freq_range_mhz_high} --trans_out ${trans_out_file} \
  --srtm_directory data/SRTM_data/ --antenna_file ${antenna_workfile} --rmax ${rmax} --station_skip ${station_skip} \
  --n_time_chunks ${n_t} || exit

echo "--------------------------"
echo " Running OSKAR script...  "
echo "--------------------------"

python3 ./oskar_sim_beam_gain_sing.py --transmitters ${trans_out_file}  --outdir ${beamgain_dir} --indir ${atten_dir} \
  --oskar_path ./OSKAR-2.7.6-Python3.sif --N_channels ${n_chan} \
  --frequency_range ${freq_range_mhz_low} ${freq_range_mhz_high} \
  --ra ${ra} --dec ${dec} --telescope_path ${telescope_path}|| exit

echo "--------------------------"
echo " Running RASCIL script... "
echo "--------------------------"

python3 ./rascil_scripts/simulate_low_rfi_visibility_propagation.py --use_dask=True \
  --noise False --ra ${ra} --declination ${dec} --use_beamgain True \
  --beamgain_dir ${beamgain_dir} --input_file ${hdf5_file} --use_antfile True \
  --antenna_file ${antenna_workfile} --time_range ${t_range_beg} ${t_range_end} --channel_average ${ch_avg} \
  --nchannels_per_chunk ${n_chan} --integration_time ${t_int} \
  --time_average ${t_avg} --write_ms True --frequency_range ${freq_range_hz_low} ${freq_range_hz_high} || exit

echo "--------------------------"
echo " Running RASCIL imager... "
echo "--------------------------"

python3 $RASCIL/rascil/apps/rascil_imager.py --ingest_msname simulate_rfi.ms --ingest_dd 0 \
--ingest_vis_nchan 8 --ingest_chan_per_vis 1 --mode invert --imaging_cellsize 0.003 \
--imaging_npixel 768 || exit

echo "--------------------------"
echo " Running RASCIL visualise "
echo "--------------------------"

python3 $RASCIL/rascil/apps/rascil_vis_ms.py --ingest_msname ./simulate_rfi.ms || exit

echo "--------------------------"
echo " Finished.                "
echo "--------------------------"
