"""Common functions"""

import logging
import os
from typing import Tuple

import numpy as np
from astropy import units as u
from astropy.coordinates import EarthLocation
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.configuration.config_create import (
    create_configuration_from_LLAfile,
)

LOG = logging.getLogger("rfi-logger")


def default_or_antenna_value(
    property_name, set_value, default_value, antenna_data
):
    """
    Return either the user defined value or antenna-based value of a property

    :param property_name: property name to get value for.
                          Currently either "bandwidth" or "central frequency"
    :param set_value: if True, use default value,
                      else use one obtained from antenna_data
    :param default_value: value from default arguments
    :param antenna_data: DTV antenna information (pandas.Series)

    :return value: either user defined or antenna-based value
    """
    column_property_dict = {
        "bandwidth": "Bandwidth(MHz)",
        "central frequency": "Frequency(MHz)",
    }

    try:
        column_property_dict[property_name]
    except KeyError as err:
        raise KeyError(
            f"The property you provided ({property_name}) is not valid. "
            f"Currently only the following can be used: "
            f"{column_property_dict.keys()}"
        ) from err

    if set_value == "True":
        value = default_value
        LOG.info("%s is %.2f MHz", property_name.capitalize(), value)

    else:
        try:
            value = antenna_data[column_property_dict[property_name]]
            LOG.info(
                "%s from csv is %.2f MHz", property_name.capitalize(), value
            )

        except KeyError:
            value = default_value
            LOG.info(
                "Cannot find %s in csv file, using default of %.2f MHz",
                property_name,
                value,
            )

    return value


def calculate_bandwidth_channel_range(bandwidth, frequency, tx_freq):
    """
    Calculate the number of channels, the start and end frequency
    based on the given bandwidth

    :param bandwidth: bandwidth to perform calculations for in MHz
    :param frequency: numpy array of frequencies between
                      original start and end value
    :param tx_freq: central frequency of the transmitter

    :return: the new frequency range values: number of channels,
             start and end frequencies, and the array of new frequencies
    """
    freq_start = tx_freq - (bandwidth / 2.0)
    freq_end = tx_freq + (bandwidth / 2.0)
    dtv_range = np.where((frequency <= freq_end) & (frequency >= freq_start))
    new_freq: Tuple = frequency[dtv_range]

    try:
        freq_start = new_freq[0]
    except IndexError:
        LOG.warning(
            "Start and end frequencies calculated from bandwidth "
            "and central frequency of transmitter are out of range "
            "of full frequency range we are simulating. "
            "Cannot obtain bandwidth-truncated frequency range."
        )
        return None, None, None, None

    freq_end = new_freq[-1]
    n_freqs = len(new_freq)

    return n_freqs, freq_start, freq_end, new_freq


def generate_ska_antenna_configuration(
    use_antfile, antenna_file, rmax, log=LOG
):
    """
    Generate antenna configuration.

    :param args: user-provided arguments,
                 must contain: use_antfile, antenna_file
    :param log: logging object also used in Dask
    :param rmax: maximum distance of station from SKA antenna centre
    """
    if use_antfile == "True":
        antenna_file = os.path.abspath(antenna_file)
        log.info("Using antenna file from %s", antenna_file)

        location = EarthLocation(
            lon=116.76444824 * u.deg, lat=-26.824722084 * u.deg, height=300.0
        )
        low = create_configuration_from_LLAfile(
            antfile=antenna_file,
            location=location,
            mount="XY",
            names="LOW_%d",
            vp_type="LOW",
            diameter=38.0,
            alt=300.0,
            rmax=rmax,
            name="LOW",
            ecef=True,
        )
    else:
        log.info("Using RASCIL LOW configuration file")
        low = create_named_configuration("LOWR3", rmax=rmax)

    return low
