# pylint: disable=line-too-long
"""
Generate attenuation data of RFI sources at a given SKA station location, for a given frequency range.
Supported sources: TV antenna

Usage:
    rfi_source_signal_interface.py tv_antenna --transmitters=<transmitter-csv> [--n_time_chunks=<time-samples>] [<n_channels> <freq_start> <freq_end>]
    rfi_source_signal_interface.py aircraft [--n_time_chunks=<time-samples>]
    rfi_source_signal_interface.py (-h | --help)

Arguments:
    # if tv_antenna
    --transmitters=<transmitter-csv>    Location of input CSV file containing TV transmitter properties
    --n_time_chunks=<time-samples>       Number of time samples to run the simulation for; default = 1

Options:
    -h --help           Show this screen.

    # if any of the following is provided, all three has to be provided as a CLI argument
    <n_channels>        Number of frequency channels. Default: 3
    <freq_start>        Start of Frequency range [MHz]. Default: 170.5
    <freq_end>          End of Frequency range [MHz]. Default: 184.5

"""  # noqa: E501
import sys

import numpy as np
from docopt import docopt

from rfi.pycraf_scripts.SKA_low_RFI_propagation import (
    cli_parser,
    rfi_attenuation,
)
from rfi.rfi_interface.rfi_data_cube import DataCube

N_CHANNELS = "n_channels"
FREQ_RANGE = "--frequency_range"
FREQ_START = "<freq_start>"
FREQ_END = "<freq_end>"

ACCEPTED_SOURCES = ["tv_antenna", "aircraft"]
FREQ_KEYS = [f"<{N_CHANNELS}>", FREQ_START, FREQ_END]


def rfi_interface(source_type, **kwargs):
    """
    Call the appropriate RFI signal calculator based on the input source type,
    and format its output to be saveable to HDF5.

    :param source_type: type of the RFI source, either tv_antenna or aircraft
    :param kwargs: arguments for the rfi code to be called based on source type

    :return: DataCube object containing all data from all relevant sources
    """
    if source_type not in ACCEPTED_SOURCES:
        raise ValueError(
            f"RFI source not recognized. "
            f"RFI source has to be one of: {ACCEPTED_SOURCES}"
        )

    if kwargs["--n_time_chunks"] is None:
        kwargs["--n_time_chunks"] = "1"

    if source_type == "tv_antenna":
        args_list = []
        # pylint: disable=expression-not-assigned
        [
            args_list.extend([k, v])
            for k, v in kwargs.items()
            if k != FREQ_RANGE
        ]
        args_list.extend(
            [
                FREQ_RANGE,
                kwargs[FREQ_RANGE][0],
                kwargs[FREQ_RANGE][1],
            ]
        )

        parser = cli_parser()
        args = parser.parse_args(args_list)

        apparent_power_results = rfi_attenuation(
            args, save_azel_to_file=False, hdf5_only=True
        )

        freqs = apparent_power_results[0].rfi_signal.frequency.tolist()
        station_ids = apparent_power_results[0].rfi_signal.station_id
        times = [None] * int(args.n_time_chunks)
        cube = DataCube(
            times,
            freqs,
            station_ids,
            rmax=args.rmax,
            station_skip=args.station_skip,
        )

        for source_data in apparent_power_results:
            source_data.time_samples = np.array(times)
            cube.append_data(source_data)

    elif source_type == "aircraft":
        raise NotImplementedError(
            "Aircraft RFI calculations are not implemented yet."
        )

    return cube


def export_data(attenuation_data, source_type):
    """
    Save RFI attenuation data into HDF5 file.

    :param attenuation_data: DataCube object with RFI source and signal data
    :param source_type: type of the RFI source, either tv_antenna or aircraft
    """
    attenuation_data.export_to_hdf5(f"rfi_signal_{source_type}.hdf5")


def validate_frequency(arguments):
    """Validate frequency-type arguments"""
    validate = [arguments[k] for k in FREQ_KEYS if arguments[k] is not None]
    if len(validate) > 0 and len(validate) != 3:
        raise ValueError(
            f"If you provide one of {FREQ_KEYS}, you need to provide all."
        )

    # pylint: disable=use-a-generator
    if not all([arguments[k] for k in FREQ_KEYS]):
        # default values for SKA Low in SKA_low_RFI_propagation.py
        arguments[f"--{N_CHANNELS}"] = "3"
        arguments[FREQ_RANGE] = ["170.5", "184.5"]

    else:
        arguments[f"--{N_CHANNELS}"] = str(arguments[f"<{N_CHANNELS}>"])
        arguments[FREQ_RANGE] = [
            str(arguments[FREQ_START]),
            str(arguments[FREQ_END]),
        ]

    # remove keys that cannot be used by RFI code
    arguments.pop(f"<{N_CHANNELS}>")
    arguments.pop(FREQ_START)
    arguments.pop(FREQ_END)


def main(argv):
    """Main function"""
    args = docopt(__doc__, argv=argv[1:], help=True)

    source_type = [x for x in ACCEPTED_SOURCES if args[x]][
        0
    ]  # only one can be true

    # remove keys that will not be an input to any
    # of the rfi_attenuation functions
    args.pop("--help", None)
    args.pop("-h", None)
    for source in ACCEPTED_SOURCES:
        args.pop(source, None)

    validate_frequency(args)

    rfi_signal = rfi_interface(source_type, **args)
    export_data(rfi_signal, source_type)


if __name__ == "__main__":
    main(sys.argv)
