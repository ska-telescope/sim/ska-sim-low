#!/usr/bin/env bash
#
# Image simulated data
#
python3 ${RASCIL}/rascil/apps/rascil_imager.py  --clean_nmoment 3 --deconvolve_facets 8 \
  --use_dask True --imaging_context ng --imaging_npixel 10240 --image_pol stokesI --restored_output integrated \
  --imaging_cellsize 1e-5 --imaging_weighting uniform \
  --vis_nchan 100 --image_nchan 1 --chan_per_blockvis 16 \
  --msname scalar_gaussian_beams.ms || exit
