#!/usr/bin/env bash
#
# Image simulated data
#
results_dir=${HOME}/data/ska-sim-low/continuum_imaging/sp_1331/
python3 ${RASCIL}/rascil/apps/rascil_imager.py  --clean_nmoment 3 --clean_facets 2 --clean_overlap 32 \
  --clean_nmajor 10  --clean_threshold 3e-5 --clean_restore_facets 1 \
  --use_dask True --imaging_context ng --imaging_npixel 16384 --imaging_pol stokesI \
  --clean_restored_output taylor \
  --imaging_cellsize 1e-5 --imaging_weighting uniform --imaging_nchan 1 \
  --ingest_vis_nchan 100 --ingest_chan_per_vis 6 \
  --ingest_msname ${results_dir}/scalar_gaussian_beams.ms \
  --performance_file ${results_dir}/performance_rascil_imager.json
