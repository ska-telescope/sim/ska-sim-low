# pylint: disable=too-many-locals
"""
Test RASCIL part of RFI simulations
"""
import glob
import logging
import os
import re
import tempfile
from unittest.mock import Mock, patch

import numpy
import pytest
from rascil.apps.rascil_imager import cli_parser as imager_cli_parser
from rascil.apps.rascil_imager import imager
from rascil.apps.rascil_vis_ms import cli_parser as vis_cli_parser
from rascil.apps.rascil_vis_ms import visualise
from ska_sdp_datamodels.image.image_io_and_convert import (
    import_image_from_fits,
)

from rfi.rascil_scripts.power_spectrum import (
    cli_parser as powerspectrum_cli_parser,
)
from rfi.rascil_scripts.power_spectrum import (
    power_spectrum,
)
from rfi.rascil_scripts.simulate_low_rfi_visibility_propagation import (
    cli_parser,
    rfi_simulation,
)

log = logging.getLogger("rascil-logger")
log.setLevel(logging.WARNING)
log = logging.getLogger("rfi-logger")
log.setLevel(logging.WARNING)

PERSIST = os.environ.get("RASCIL_PERSIST", False)

# These tests exercise the RASCIL part of the RFI simulation code, using the
# RASCIL imager to calculate the dirty images from which we caculate
# the statistics, and check those.

# use_dask Use dask or serial processing
# image_max, image_min, image_rms: statistics as found by qa_image


@pytest.mark.parametrize(
    "use_dask, msout",
    [
        (
            "False",
            "simulate_rfi_skip2_noDask.ms",
        ),
        (
            "True",
            "simulate_rfi_skip2.ms",
        ),
    ],
)
@patch(
    "rfi.rascil_scripts.simulate_low_rfi_visibility"
    "_propagation.check_for_old_run",
    Mock(),
)
@patch(
    "rfi.rascil_scripts.simulate_low_rfi_visibility"
    "_propagation.write_settings",
    Mock(),
)
def test_rfi_simulation(use_dask, msout):
    """Test RASCIL part of RFI simulations"""
    file_path = __file__
    input_file = re.sub(
        "tests/rfi/rascil_scripts/test_regression_rascil_"
        "simulate_low_rfi_visibility_propagation.py",
        "rfi/test_data/tv_transmitter_attenuation_cube.hdf5",
        file_path,
    )
    antenna_file = re.sub(
        "tests/rfi/rascil_scripts/test_regression_rascil_"
        "simulate_low_rfi_visibility_propagation.py",
        "rfi/data/telescope_files/SKA1-LOW_SKO-0000422_Rev3_"
        "38m_SKALA4_spot_frequencies.tm/layout_wgs84.txt",
        file_path,
    )

    expected_image_max = 19048126.008895155
    expected_image_min = -11089147.626079125
    expected_image_rms = 127790.28029560398

    with tempfile.TemporaryDirectory() as tempdirname:
        parser = cli_parser()
        args = parser.parse_args(
            [
                "--use_dask",
                f"{use_dask}",
                "--ra",
                "0.0",
                "--declination",
                "-90.0",
                "--use_beam",
                "False",
                "--time_range",
                "-0.01",
                "0.01",
                "--integration_time",
                "0.2",
                "--nchan",
                "8",
                "--channel_average",
                "1",
                "--time_average",
                "1",
                "--nchannels_per_chunk",
                "8",
                "--write_ms",
                "True",
                "--msout",
                f"{tempdirname}/{msout}",
                "--use_antfile",
                "False",
                "--antenna_file",
                antenna_file,
                "--input_file",
                input_file,
            ]
        )

        rfi_simulation(args)

        parser = vis_cli_parser()
        args = parser.parse_args(["--ingest_msname", f"{tempdirname}/{msout}"])
        visualise(args)

        parser = imager_cli_parser()
        args = parser.parse_args(
            [
                "--mode",
                "invert",
                "--ingest_msname",
                f"{tempdirname}/{msout}",
                "--ingest_vis_nchan",
                "8",
                "--ingest_dd",
                "0",
                "--ingest_chan_per_vis",
                "1",
                "--imaging_npixel",
                "768",
                "--imaging_cellsize",
                "0.003",
            ]
        )

        dirtyname = imager(args)
        dirty = import_image_from_fits(dirtyname)
        qa = dirty.image_acc.qa_image()
        log.info(qa)

        assert dirty["pixels"].shape == (
            1,
            1,
            args.imaging_npixel,
            args.imaging_npixel,
        )

        numpy.testing.assert_allclose(
            qa.data["max"], expected_image_max, atol=1e-7, rtol=1e-7
        )
        numpy.testing.assert_allclose(
            qa.data["min"], expected_image_min, atol=1e-7, rtol=1e-7
        )
        numpy.testing.assert_allclose(
            qa.data["rms"], expected_image_rms, atol=1e-7, rtol=1e-7
        )

        parser = powerspectrum_cli_parser()
        args = parser.parse_args(["--image", dirtyname])

        power_spectrum(args)

        if not PERSIST:
            images = glob.glob("*.png")
            for image in images:
                os.remove(image)

            htmls = glob.glob("*.html")
            for html in htmls:
                os.remove(html)

            os.remove("power_spectrum_channel.csv")
