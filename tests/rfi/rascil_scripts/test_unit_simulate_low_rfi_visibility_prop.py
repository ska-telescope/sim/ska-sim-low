"""Unite tests for RFI simulations with RASCIL"""

import os
import tempfile
from unittest.mock import Mock, patch

import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration.config_create import (
    create_named_configuration,
)
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility

from rfi.rascil_scripts.simulate_low_rfi_visibility_propagation import (
    add_noise,
    get_chunk_start_times,
    load_beam_gain_and_ra_dec,
)
from rfi.rfi_interface.rfi_data_cube import BeamGainDataCube


class DotDict(dict):
    """dot.notation access to dictionary attributes"""

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def test_add_noise():
    """
    Random gaussian noise is added to block visibility data.
    Input data is 0s everywhere. Output is the random noise.
    """
    # rmax = 30 m --> 1 baseline 30 m within SKA LOW centre
    config = create_named_configuration("LOWR3", rmax=30.0)
    times = np.array([-0.0001, 0.0001])  # hour angle in rad
    frequency = np.linspace(160e6, 200e6, 2)
    phasecentre = SkyCoord(
        ra=+15.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
    )
    channel_bandwidth = np.array([1e7, 1e7])

    vis = create_visibility(
        config,
        times,
        frequency,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=PolarisationFrame("stokesI"),
        channel_bandwidth=channel_bandwidth,
    )

    assert (vis["vis"].data == 0).all()

    result = add_noise(vis)
    assert (
        abs(result["vis"].data) > 0
    ).all()  # noise is from a random normal distribution


def test_load_beam_gain_and_ra_dec_dont_use_beam():
    """
    args.use_beamgain = False
    """
    args = DotDict({"use_beamgain": "False", "ra": 0.0, "declination": -45.0})
    result = load_beam_gain_and_ra_dec(args, ["NA"], ["NA"], Mock())

    assert result[0] == 1.0  # beam gain
    assert result[1] == 0.0  # RA
    assert result[2] == -45.0  # DEC


@patch(
    "rfi.rascil_scripts.simulate_low_rfi_visibility_propagation.numpy.loadtxt"
)
def test_load_beam_gain_and_ra_dec_numpy_beam(mock_load):
    """
    args.use_beamgain = True
    args.input_hdf_file = ""
    """
    args = DotDict(
        {
            "use_beamgain": "True",
            "beamgain_hdf_file": "",
            "ra": 0.0,
            "declination": -45.0,
            "beamgain_dir": ".",
        }
    )
    source_ids = np.array(["source1", "source2"])
    frequency_channels = np.array([170.0, 175.0, 180.0])
    mock_load.side_effect = [
        np.array([9, 4, 16]),
        np.array([4, 25, 9]),
    ]  # 2 sources, 3 frequency channels

    expected_gain = np.array([[[9, 4, 16]], [[4, 25, 9]]])
    result = load_beam_gain_and_ra_dec(
        args, source_ids, frequency_channels, Mock()
    )

    assert (result[0] == expected_gain).all()  # beam gain
    assert result[1] == 0.0  # RA
    assert result[2] == -45.0  # DEC


def test_load_beam_gain_and_ra_dec_hdf_beam():
    """
    args.use_beamgain = True
    args.input_hdf_file = "some file
    """
    source_ids = np.array(["source1", "source2"])
    frequency_channels = np.array([170.0, 175.0, 180.0])
    nstations = 5
    ra = 10.2
    dec = 60.9

    # set up test beam gain to be loaded into tmp file
    test_beam_gain_cube = BeamGainDataCube(
        ra,
        dec,
        # time of observation, same as one in OSKAR settings
        "01-01-2000 09:32:00.000",
        frequency_channels,
        source_ids,
        nstations,  # nstations
    )
    beam_gain_array = np.array(
        [
            [[[1.1, 2, 3, 4], [1.2, 2, 3, 4], [1.3, 2, 3, 4]]] * nstations,
            [[[2.1, 3, 4, 5], [2.2, 3, 4, 5], [2.3, 3, 4, 5]]] * nstations,
        ]
    )  # 2 sources, 4 stations, 3 channels, 4 stokes
    test_beam_gain_cube.beam_gain = beam_gain_array

    # contains stokes I only
    expected_beam = np.array(
        [[[1.1, 1.2, 1.3]] * nstations, [[2.1, 2.2, 2.3]] * nstations]
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        hdf_file = os.path.join(tmp_dir, "my-tmp-file.hdf5")
        args = DotDict(
            {
                "use_beamgain": "True",
                "beamgain_hdf_file": "my-tmp-file.hdf5",
                "ra": 0.0,
                "declination": -45.0,
                "beamgain_dir": tmp_dir,
            }
        )
        test_beam_gain_cube.export_to_hdf5(hdf_file)

        result = load_beam_gain_and_ra_dec(
            args, source_ids, frequency_channels, Mock()
        )

        os.remove(hdf_file)

    assert (result[0] == expected_beam).all()  # beam gain
    # RA and DEC read from file and not from args
    assert result[1] == ra
    assert result[2] == dec


def test_get_chunk_start_times():
    """
    One integration is 10s, we want 6 integrations in one chunk,
    with an hour angle range of 0 to 1 hour.

    --> results in 60 chunks, with chunk start times in seconds.
    """
    time_range = [0.0, 1.0]  # [h, h]
    nintegr_per_chunk = 6
    int_time = 10.0  # [s]

    result = get_chunk_start_times(
        time_range, int_time, nintegr_per_chunk, Mock()
    )

    assert len(result) == 60
    # start time of first chunk, 0 seconds
    assert result[0] == np.array([0])
    # start time of last chunk, 3540 seconds,
    # 60 s before end of time_range (1h)
    assert result[-1] == np.array([3540])
