"""Tests for running OSKAR"""

import os
import re
from unittest.mock import Mock, mock_open, patch

import pandas as pd
import pytest

from rfi.oskar_sim_beam_gain_sing import (
    _generate_default_oskar_settings,
    _generate_oskar_settings_from_hdf5,
    cli_parser,
    run_oskar_beam,
    run_with_hdf5,
)

ROOT_PATH = "rfi.oskar_sim_beam_gain_sing"


@pytest.mark.parametrize(
    "singularity_exists, expected_app",
    [
        (False, ["oskar_sim_beam_pattern"]),
        (
            True,
            [
                "singularity",
                "exec",
                "--bind",
                "tel-path-dir:/telescope",
                "--bind",
                "some-input-directory:/in_dir",
                "--bind",
                "some-output-directory:/out_dir",
                "some-fake-path-to-oskar",
                "oskar_sim_beam_pattern",
            ],
        ),
    ],
)
@patch(f"{ROOT_PATH}.check_for_old_run", Mock())
@patch(f"{ROOT_PATH}.write_settings", Mock())
@patch(f"{ROOT_PATH}.subprocess.call")
def test_run_oskar_beam(
    mock_subprocess_call, singularity_exists, expected_app
):
    """
    When oskar_path doesn't exist, subprocess.call is called with the
    non-singularity version of OSKAR.
    When oskar_path exists, subprocess.call is called
    with the arguments for singularity.
    """
    file_path = __file__
    transmitter_file = re.sub(
        "tests/rfi/test_oskar_sim_beam_gain.py",
        "rfi/test_data/DTV_above0_1775_100T_64C_test.csv",
        file_path,
    )

    arg_parser = cli_parser()

    args = arg_parser.parse_args(
        [
            "--transmitters",
            transmitter_file,
            "--telescope_path",
            "tel-path-dir/some-telescope-path",
            "--indir",
            "some-input-directory",
            "--outdir",
            "some-output-directory",
            "--oskar_path",
            "some-fake-path-to-oskar",
            "--input_hdf_file",
            "",
        ]
    )

    # if this is not 0, code will fail complaining that OSKAR run failed.
    mock_subprocess_call.return_value = 0

    # patching os.path.exists:
    #   True: when os.path.exists(out_dir) is checked -->
    #       we don't want to create a directory here, so we fake that it exists
    #   False: when os.path.exists(args.oskar_path) is checked -->
    #       here we test when this path doesn't exist
    with patch(
        f"{ROOT_PATH}.os.path.exists",
        Mock(side_effect=[True, singularity_exists]),
    ):
        # path the part where we are writing into the oskar_settings.ini file;
        # we don't want that from a test
        with patch(f"{ROOT_PATH}.open", mock_open()):
            run_oskar_beam(args)
            mock_subprocess_call.assert_called_with(
                expected_app + ["oskar_settings.ini"]
            )


@pytest.mark.parametrize(
    "choose_range, set_bandwidth, expected_num_channels",
    [("True", "False", 10), ("False", "True", 6)],  # Scenario 1  # Scenario 2
)
def test_generate_default_oskar_settings(
    choose_range, set_bandwidth, expected_num_channels
):
    # pylint: disable=line-too-long
    """
    Test that _generate_default_oskar_settings returns the correct values depending on input args.

    Scenarios:
    1. --choose-range = True will skip the path where freq_start, freq_end, n_freq,
            freq_inc are recalculated;
       --set_bandwidth = False will take the bandwidth value from the provided dtv_antenna Series.
    2. --choose-range = False and --set_bandwidth = True (with the given args) will
            enter the path where freq_start, freq_end, n_freq, freq_inc are recalculated
    """  # noqa: E501
    arg_parser = cli_parser()

    args = arg_parser.parse_args(
        [
            "--N_channels",
            "10",
            "--frequency_range",
            "170.5",
            "184.5",
            "--choose_range",
            choose_range,
            "--set_bandwidth",
            set_bandwidth,
            "--bandwidth",
            "10",
        ]
    )

    dtv_antenna = pd.Series(
        {
            "Name": "My fancy antenna",
            "Frequency(MHz)": 177.5,
            "Bandwidth(MHz)": 30.0,
        }
    )
    input_dir = "path/to-in-dir"
    output_dir = "path/to-out-dir"
    telescope_model_path = "path/"
    telescope_model_name = "tel_model.csv"

    result = _generate_default_oskar_settings(
        args,
        dtv_antenna,
        input_dir,
        output_dir,
        telescope_model_path,
        telescope_model_name,
    )

    assert (
        result["observation"]["num_channels"] == expected_num_channels
    )  # equivalent to n_freq
    assert result["observation"]["frequency_inc_hz"] == 1555555.6
    assert (
        result["beam_pattern"]["sky_model/file"]
        == "path/to-in-dir/My fancy antenna_AzEl.txt"
    )


def test_generate_default_oskar_settings_no_freq_range():
    """
    Test that _generate_default_oskar_settings returns None, when
    the bandwidth-dependent frequency range cannot be calculated.

    --frequency_range arg does not contain the range provided by
        ["Frequency(MHz)" - "Bandwidth(MHz)",
         "Frequency(MHz)" + "Bandwidth(MHz)"]
    """
    arg_parser = cli_parser()

    args = arg_parser.parse_args(
        [
            "--N_channels",
            "10",
            "--frequency_range",
            "170.5",
            "184.5",  # full range we are simulating
            "--choose_range",
            "False",
            "--set_bandwidth",
            "True",
            "--bandwidth",
            "10",
        ]
    )

    dtv_antenna = pd.Series(
        {
            "Name": "My fancy antenna",
            "Frequency(MHz)": 200.0,  # antenna central frequency
            "Bandwidth(MHz)": 10.0,
        }
    )
    input_dir = "path/to-in-dir"
    output_dir = "path/to-out-dir"
    telescope_model_path = "path/"
    telescope_model_name = "tel_model.csv"

    result = _generate_default_oskar_settings(
        args,
        dtv_antenna,
        input_dir,
        output_dir,
        telescope_model_path,
        telescope_model_name,
    )

    assert result is None


def test_generate_oskar_settings_hdf5():
    """
    Correct settings are generated when function is called with
    input data from HDF5.
    """
    arg_parser = cli_parser()
    args = arg_parser.parse_args(["--ra", "0.0", "--dec", "-45.0"])

    freqs = (4, 170.0e6, 0.5e6)
    az_el_dist = [123.0, 0.66, 200e4]  # Hz, Hz, m
    station_idx = 0
    telescope_model_path = "path/"
    telescope_model_name = "tel_model.csv"

    result = _generate_oskar_settings_from_hdf5(
        args,
        freqs,
        az_el_dist,
        station_idx,
        telescope_model_path,
        telescope_model_name,
    )

    assert result["observation"]["num_channels"] == 4
    assert result["observation"]["frequency_inc_hz"] == 0.5e6
    assert result["beam_pattern"]["sky_model/file"] == "tmp_in.txt"
    assert (
        result["beam_pattern"]["station_outputs/text_file/auto_power"] is True
    )


@patch(f"{ROOT_PATH}.BeamGainDataCube.export_to_hdf5", Mock())
@patch(f"{ROOT_PATH}.subprocess.call")
def test_run_with_hdf5(mock_subprocess_call):
    """
    run_with_hdf5 runs without errors when happy path is executed:
        - reads from hdf5 file in rfi/test_data
        - correctly reads from and deletes temporary files
    """
    file_path = __file__
    in_dir = re.sub(
        "tests/rfi/test_oskar_sim_beam_gain.py",
        "rfi/test_data",
        file_path,
    )

    # pylint: disable=unused-argument
    def _create_tmp_out_files(args):
        """
        Mock creating tmp output files by OSKAR.
        These will be automatically removed by code unless test fails.
        *args needed because the func this one replaces takes an input
        """
        to_create = [
            "tmp_out_I.txt",
            "tmp_out_U.txt",
            "tmp_out_V.txt",
            "tmp_out_Q.txt",
        ]
        for name in to_create:
            with open(name, "w", encoding="utf-8") as f:
                f.write("# some comment\n")
                # add one line of "beam gain", because there
                # is one channel in the test hdf5 file.
                f.write("0.0033\n")

        return 0

    arg_parser = cli_parser()
    args = arg_parser.parse_args(
        [
            "--indir",
            in_dir,
            "--outdir",
            in_dir,
            "--ra",
            "0.0",
            "--dec",
            "-45.0",
            "--input_hdf_file",
            "tv_transmitter_attenuation_cube.hdf5",
        ]
    )

    telescope_model_path = "path/"
    telescope_model_name = "tel_model.csv"

    mock_subprocess_call.side_effect = _create_tmp_out_files

    # code runs without any errors
    run_with_hdf5(
        ["oskar"],
        args,
        in_dir,
        in_dir,
        telescope_model_name,
        telescope_model_path,
    )

    os.remove("oskar_settings.ini")
