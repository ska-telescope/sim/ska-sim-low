"""Test code for checking old runs"""

import csv
import logging
import os
import tracemalloc
import unittest
from unittest.mock import call, patch

from rfi.old_run_checker import check_for_old_run, write_settings

tracemalloc.start()

log = logging.getLogger("logger")
log.setLevel(logging.WARNING)


class TestOldRunChecker(unittest.TestCase):
    """Test code for checking old runs"""

    def setUp(self):
        self._settings_file = "test_settings.csv"

        # Mock settings dict containg basic types.
        self._args = {
            "a": "a/path/to/a/directory",
            "b": 123.4,
            "c": False,
            "d": 2,
        }

        self._args_change = {
            "a": "a/path/to/another/directory",
            "b": 432.1,
            "c": True,
            "d": 1,
        }

        self._args_change_order = {
            "a": "a/path/to/a/directory",
            "c": False,
            "b": 123.4,
            "d": 2,
        }

        with open(self._settings_file, "w", encoding="utf-8") as outfile:
            writer = csv.writer(outfile)
            for key, val in self._args.items():
                writer.writerow([key, val])

    def tearDown(self):
        if os.path.isfile("test_write.csv"):
            os.remove("test_write.csv")
        os.remove("test_settings.csv")

    def test_old_run_checker_no_initial_file(self):
        """Missing init file"""
        with patch("logging.Logger.info") as mock_log:
            check_for_old_run(self._args, "")
            mock_log.assert_has_calls([call("No old settings found.")])

    def test_old_run_checker_write_file(self):
        """Write new file"""
        write_settings(self._args, "./test_write.csv")
        assert os.path.isfile("test_write.csv")

    def test_old_run_checker_no_change(self):
        """Old run and new run are the same"""
        self.assertRaises(
            SystemExit, check_for_old_run, self._args, self._settings_file
        )

    def test_old_run_checker_with_change(self):
        """Old run is different from new run"""
        with patch("logging.Logger.info") as mock_log:
            check_for_old_run(self._args_change, self._settings_file)
            mock_log.assert_has_calls(
                [call("Old settings do not match new settings.")]
            )

    def test_old_run_checker_with_change_order(self):
        """??"""
        self.assertRaises(
            SystemExit,
            check_for_old_run,
            self._args_change_order,
            self._settings_file,
        )


if __name__ == "__main__":
    unittest.main()
