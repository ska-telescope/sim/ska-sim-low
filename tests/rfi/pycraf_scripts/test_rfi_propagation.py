# pylint: disable=too-many-locals
"""
Test RFI propagation code (pycraf)
"""
import os
import re
from unittest.mock import Mock, patch
from urllib.error import HTTPError

import numpy as np
import pytest

from rfi.pycraf_scripts.SKA_low_RFI_propagation import (
    calculate_apparent_power,
    cli_parser,
    construct_csv_file_path,
    main,
    rfi_attenuation,
    transmitter_below_horizon,
)

TEXT_TO_REPLACE_IN_PATH = "tests/rfi/pycraf_scripts/test_rfi_propagation.py"
OUTPUTDIR = "/where/file/should/go/"


@pytest.mark.parametrize(
    "check_horizon, elevation, expected_result",
    [("True", 1.0, False), ("True", -1.0, True), ("False", -10.0, False)],
)
def test_transmitter_below_horizon(check_horizon, elevation, expected_result):
    """
    If check_horizon is "True", when the elevation of
    a transmitter is below zero, the function returns False,
    else returns True.
    If check_horizon is "False", function returns False
    regardless of transmitter elevation.

    Note: check_horizon has to be a string.
    """
    result = transmitter_below_horizon(check_horizon, elevation)
    assert result is expected_result


@pytest.mark.parametrize(
    "file_name, filtered_transmitter, expected_csv_name",
    [
        # if file_name is "infile", generate file name
        # from existing filtered_transmitter string
        (
            "infile",
            "transmitter_file_name",
            f"{OUTPUTDIR}transmitter_file__el.csv",
        ),
        ("infile", "dir/tr_files", f"{OUTPUTDIR}tr_f_el.csv"),
        # if file_name also contains path, append that to
        # full path of working directory
        (
            "dir/my-transm-file.csv",
            "existing_transmitter_file",
            os.getcwd() + "/dir/my-transm-file.csv",
        ),
        # if file_name is not a path, the resulting path
        # will include the outputdir value
        (
            "want_my_file_to_be_called_this",
            "some_existing_file_with_transmitters",
            f"{OUTPUTDIR}want_my_file_to_be_called_this.csv",
        ),
    ],
)
def test_construct_csv_file_path(
    file_name, filtered_transmitter, expected_csv_name
):
    """
    Test code to construct file path
    """
    result = construct_csv_file_path(
        file_name, filtered_transmitter, OUTPUTDIR
    )
    assert result == expected_csv_name


def test_calculate_apparent_power():
    """
    Time and frequency variability are turned off.
    """
    ntimes = 2  # two time samples
    frequency_range = 5  # MHz
    transm_power = 4000  # W
    attenuat = np.ones((3, 4))  # 3 antennas, 4 channels

    result = calculate_apparent_power(
        ntimes, frequency_range, transm_power, attenuat
    )

    assert result.shape == (2, 3, 4)  # ntimes x antennas x channels
    # because attenuat is constant at all points,
    # this value is the same everywhere in result
    assert (result.round(5) == 0.01783 + 0j).all()


def test_calculate_apparent_power_time_var():
    """
    Time variability on, but frequency variability off.
    """
    np.random.seed(1805550721)

    ntimes = 2  # two time samples
    frequency_range = 5  # MHz
    transm_power = 4000  # W
    attenuat = np.ones((3, 4))  # 3 antennas, 4 channels

    result = calculate_apparent_power(
        ntimes, frequency_range, transm_power, attenuat, time_variable=True
    )

    assert result.shape == (2, 3, 4)  # ntimes x antennas x channels
    # first time sample, same for every frequency and station
    assert (abs(result[0]).round(5) == 0.00737).all()
    # second time sample, diff from first, but same for
    # every frequency and station
    assert (abs(result[1]).round(5) == 0.01598).all()


def test_calculate_apparent_power_freq_var():
    """
    Frequency variability on, but time variability off.
    """
    np.random.seed(1805550721)

    ntimes = 2  # two time samples
    frequency_range = 5  # MHz
    transm_power = 4000  # W
    attenuat = np.ones((3, 4))  # 3 antennas, 4 channels

    result = calculate_apparent_power(
        ntimes,
        frequency_range,
        transm_power,
        attenuat,
        frequency_variable=True,
    )

    assert result.shape == (2, 3, 4)  # ntimes x antennas x channels
    # first frequency channel, same for every time and station
    assert (abs(result[:, :, 0]).round(5) == 0.01169).all()
    # second frequency channel, same for every time and station
    assert (abs(result[:, :, 1]).round(5) == 0.01808).all()
    # third frequency channel, same for every time and station
    assert (abs(result[:, :, 2]).round(5) == 0.00597).all()
    # fourth frequency channel, same for every time and station
    assert (abs(result[:, :, 3]).round(5) == 0.02721).all()


def test_calculate_apparent_power_time_freq_var():
    """
    Frequency and time variability are both on.
    """
    np.random.seed(1805550721)

    ntimes = 2  # two time samples
    frequency_range = 5  # MHz
    transm_power = 4000  # W
    attenuat = np.ones((3, 4))  # 3 antennas, 4 channels

    result = calculate_apparent_power(
        ntimes,
        frequency_range,
        transm_power,
        attenuat,
        frequency_variable=True,
        time_variable=True,
    )

    assert result.shape == (2, 3, 4)  # ntimes x antennas x channels

    # 0th station (for any station the results are the same)
    # all of the values are time and frequency variable,
    # hence they are all unique the length of unique values in
    # the array matches the length of the flattened array
    assert len(np.unique(result[:, 0, :])) == len(result[:, 0, :].flatten())


@pytest.mark.xfail(raises=HTTPError, reason="SMRT data website is down")
@patch("pandas.DataFrame.to_csv")
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.check_for_old_run", Mock())
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.write_settings", Mock())
def test_rfi_attenuation(mock_to_csv):
    """
    With the given frequency range, and the transmitter
    central frequency from the transmitter file, the simulation
    will produce emitted powers for two frequency channels,
    which we test here.
    """
    file_path = __file__
    workbook_location = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/test_data/SKA_Low_test_REV3.txt",
        file_path,
    )
    srtm_dir = re.sub(TEXT_TO_REPLACE_IN_PATH, "rfi/test_data", file_path)
    transmitter_file = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/data/transmitters/Filtered_DTV_list_1.csv",
        file_path,
    )

    arg_parser = cli_parser()

    args = arg_parser.parse_args(
        [
            "--antenna_file",
            workbook_location,
            "--srtm_directory",
            srtm_dir,
            "--frequency_range",
            "170",
            "180",
            "--hprof_step",
            "10000",  # u.m  # resolution of solution
            "--transmitters",
            transmitter_file,
        ]
    )

    expected_power = np.array(
        [
            [
                [1.63349447e-10 + 0.0j, 1.66074525e-10 + 0.0j],
                [1.63623839e-10 + 0.0j, 1.66414885e-10 + 0.0j],
                [1.63273478e-10 + 0.0j, 1.65984331e-10 + 0.0j],
            ]
        ]
    )

    result = rfi_attenuation(args, save_azel_to_file=False)
    mock_to_csv.assert_called_once()

    result_power = result[0].rfi_signal.apparent_power
    assert round(result_power[0, 0, 0], 3) == round(expected_power[0, 0, 0], 3)
    assert round(result_power[0, 1, 1], 3) == round(expected_power[0, 1, 1], 3)

    result_freq = result[0].rfi_signal.frequency  # in Hz
    assert (
        np.round(result_freq, 3) == np.round(np.array([1.75e08, 1.80e08]), 3)
    ).all()


@pytest.mark.xfail(raises=HTTPError, reason="SMRT data website is down")
@patch("pandas.DataFrame.to_csv")
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.check_for_old_run", Mock())
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.write_settings", Mock())
def test_rfi_attenuation_bug_fix(mock_to_csv):
    """
    Bug Fix:
    When multiple transmitters are used, and the first one is
    skipped due to its central frequency not fitting within
    the simulated frequency range, the second transmitter is
    throwing a TypeError because start and end frequencies of
    the range are not reset to the original value but left as None.

    We have 2 transmitters. One is out of frequency range.
    The second is usable and calculations should be
    performed for it, inc. saving outputs.
    """

    file_path = __file__
    workbook_location = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/test_data/SKA_Low_test_REV3.txt",
        file_path,
    )
    srtm_dir = re.sub(TEXT_TO_REPLACE_IN_PATH, "rfi/test_data", file_path)
    transmitter_file = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/data/transmitters/Filtered_DTV_multi_frequency.csv",
        file_path,
    )

    arg_parser = cli_parser()

    args = arg_parser.parse_args(
        [
            "--freq",
            "180.0",
            "--set_freq",
            "False",
            "--antenna_file",
            workbook_location,
            "--srtm_directory",
            srtm_dir,
            "--frequency_range",
            "170.5",
            "235.0",
            "--transmitters",
            transmitter_file,
            "--n_channels",
            "5",
        ]
    )

    rfi_attenuation(args, save_azel_to_file=False)
    mock_to_csv.assert_called_once()


@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.check_for_old_run", Mock())
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.write_settings", Mock())
@patch(
    "rfi.pycraf_scripts.SKA_low_RFI_propagation.DataCube.export_to_hdf5",
    Mock(),
)
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.cli_parser")
@patch("rfi.pycraf_scripts.SKA_low_RFI_propagation.rfi_attenuation")
def test_rfi_propagation_main(new_rfi_atten, mock_parser):
    """
    main doesn't fail when run with happy path.
    """
    file_path = __file__
    workbook_location = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/test_data/SKA_Low_test_REV3.txt",
        file_path,
    )
    srtm_dir = re.sub(TEXT_TO_REPLACE_IN_PATH, "rfi/test_data", file_path)
    transmitter_file = re.sub(
        TEXT_TO_REPLACE_IN_PATH,
        "rfi/data/transmitters/Filtered_DTV_multi_frequency.csv",
        file_path,
    )

    arg_parser = cli_parser()
    args = arg_parser.parse_args(
        [
            "--antenna_file",
            workbook_location,
            "--srtm_directory",
            srtm_dir,
            "--frequency_range",
            "170.5",
            "235.0",
            "--transmitters",
            transmitter_file,
            "--n_channels",
            "5",
        ]
    )

    mock_parser().parse_args.return_value = args

    # replace the run within main with this.
    # This way the optional arguments can be passed in.
    new_rfi_atten.return_value = rfi_attenuation(
        args, save_azel_to_file=False, hdf5_only=True
    )

    # main doesn't fail when run with above arguments and set up.
    main()
