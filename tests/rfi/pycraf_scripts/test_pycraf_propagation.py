# pylint: disable=too-many-locals

"""
Unit tests for pycraf propagation calculation
"""

import re
import unittest
from urllib.error import HTTPError

import numpy as np
import pytest
from pycraf import pathprof

from rfi.pycraf_scripts.SKA_low_pycraf_propagation import calc_prop_atten


class TestPYprop(unittest.TestCase):
    """
    PyCraf propagation tests
    """

    def setUp(self):
        pass

    @pytest.mark.xfail(raises=HTTPError, reason="SMRT data website is down")
    def test_propagation(self):
        """
        Test RFI propagation code
        """

        # =======================================
        # Parameters for the ITU-R 452-16 model
        # =======================================

        # Units added in function
        freq = 180  # u.MHz
        temperature = 290.0  # u.K
        pressure = 1013.0  # u.hPa
        timepercent = 0.02  # u.percent  # see P.452 for explanation
        h_tg, h_rg = (
            175,
            2.1,
        )  # u.m # height of the receiver and transmitter above gnd

        # clutter type for transmitter/receiver
        zone_t, zone_r = pathprof.CLUTTER.UNKNOWN, pathprof.CLUTTER.UNKNOWN

        diam = 2  # u.m # assumed diameter of transmitter
        direct = "OD"
        polarization = 0  # 0 horizontal, 1 vertical
        file_path = __file__
        antenna_location = re.sub(
            "tests/rfi/pycraf_scripts/test_pycraf_propagation.py",
            "rfi/test_data/SKA_Low_test_REV3.txt",
            file_path,
        )
        srtm_dir = re.sub(
            "tests/rfi/pycraf_scripts/test_pycraf_propagation.py",
            "rfi/test_data",
            file_path,
        )
        nants = 3

        # =======================================
        # Path attenuation additional inputs
        # =======================================

        n_freqs = 3  # no. of channels
        freq_start, freq_end = 170, 180  # frequency range MHz
        hprof_step = 10000  # u.m  # resolution of solution

        # DTV Bickley site Perth 180MHz Seven
        tx_lon, tx_lat = 116.061666667, -32.0127777778

        atten_ant, freqs, apparent_coords, ska_low = calc_prop_atten(
            freq,
            temperature,
            pressure,
            timepercent,
            h_tg,
            h_rg,
            zone_t,
            zone_r,
            n_freqs,
            freq_start,
            freq_end,
            hprof_step,
            tx_lon,
            tx_lat,
            diam,
            direct,
            rmax=100,
            locations_file=antenna_location,
            polarization=polarization,
            srtm_directory=srtm_dir,
        )

        # PyCraf is always striving to improve accuracy. With each
        # version upgrade, there are certain changes. The output_atten here
        # should be changed to values output by PyCraf new version.
        output_atten = np.array(
            [
                [172.66633985, 172.53468004, 172.3689192],
                [172.63280345, 172.49571471, 172.32464957],
                [172.68157136, 172.55188183, 172.38805449],
            ]
        )
        freq_out = np.array([170.0, 174.92855685, 180.0])
        coord_out = np.array(
            [
                [-1.73406931e02, 5.71478668e-04, 5.79084886e02],
                [-1.73393527e02, 2.74913121e-03, 5.78940639e02],
                [-1.73413569e02, -2.63318449e-05, 5.79168256e02],
            ]
        )

        assert atten_ant.shape == (nants, n_freqs)
        assert apparent_coords.shape == (nants, 3)
        assert round(atten_ant[0][0], 3) == round(output_atten[0, 0], 3)
        assert round(atten_ant[1][1], 3) == round(output_atten[1, 1], 3)
        assert round(atten_ant[2][2], 3) == round(output_atten[2, 2], 3)
        assert round(freqs.value[1], 4) == round(freq_out[1], 4)

        # apparent_coords ==> azimuth, elevation, distance
        for i, res in enumerate(apparent_coords[0].tolist()):
            assert round(res, 4) == round(coord_out[0][i], 4)

        assert len(ska_low) == 3  # 3 stations
