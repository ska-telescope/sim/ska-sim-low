"""
Test common functions
"""

import numpy as np
import pandas as pd
import pytest

from rfi.common_tools import (
    calculate_bandwidth_channel_range,
    default_or_antenna_value,
)


def test_calculate_bandwidth_channel_range():
    """
    Function correctly returns the frequency range information
    based on given bandwidth.

    freq_array:
    array([ 50.,  72.22222222,  94.44444444, 116.66666667,
            138.88888889, 161.11111111, 183.33333333, 205.55555556,
            227.77777778, 250.])
    """
    bandwidth = 50
    tx_central_freq = 150
    freq_array = np.linspace(50, 250, 10)

    result = calculate_bandwidth_channel_range(
        bandwidth, freq_array, tx_central_freq
    )

    assert result[0] == 2  # number of channels
    assert round(result[1], 2) == 138.89  # start frequency
    assert round(result[2], 2) == 161.11  # end frequency


def test_calculate_bandwidth_channel_range_index_error():
    """
    IndexError caused by dtv_range calculated
    from bandwidth and central_frequency
    not matching the freq_array range.
    """
    bandwidth = 7.0
    tx_central_freq = 177.50

    n_freqs = 8
    freq_start = 200.0
    freq_end = 210.0
    freq_array = np.linspace(freq_start, freq_end, n_freqs)

    result = calculate_bandwidth_channel_range(
        bandwidth, freq_array, tx_central_freq
    )

    assert result == (None, None, None, None)


@pytest.mark.parametrize(
    "property_name, set_value, default_value, expected_value",
    [
        ("central frequency", "True", 10.0, 10.0),  # return default value
        (
            "central frequency",
            "False",
            10.0,
            180.0,
        ),  # return value from antenna_data
        (
            "bandwidth",
            "False",
            170.0,
            170.0,
        ),  # value does not exist in antenna_data, return default value
    ],
)
def test_default_or_antenna_value(
    property_name, set_value, default_value, expected_value
):
    """Test antenna values"""
    antenna_data = pd.Series({"Frequency(MHz)": 180.0})

    result = default_or_antenna_value(
        property_name, set_value, default_value, antenna_data
    )
    assert result == expected_value


def test_default_or_antenna_value_wrong_property():
    """
    If the provided property is not "bandwidth" or "central frequency",
    raise KeyError with custom log
    """
    property_name = "some-property"
    set_value = False
    default_value = "default"
    antenna_data = pd.Series()

    with pytest.raises(KeyError) as e:
        default_or_antenna_value(
            property_name, set_value, default_value, antenna_data
        )

    assert (
        e.value.args[0]
        == "The property you provided (some-property) is not valid. "
        "Currently only the following can be used: "
        "dict_keys(['bandwidth', 'central frequency'])"
    )
