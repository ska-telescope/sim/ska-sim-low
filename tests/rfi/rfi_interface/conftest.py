"""
Fixtures for RFI tests
"""

import numpy as np
import pytest

from rfi.rfi_interface.rfi_data_cube import (
    DataCubePerSource,
    RFISignal,
    RFISource,
)


@pytest.fixture(name="rfi_source")
def rfi_source_fixt():
    """RFISource object"""
    my_data = {
        "id": "test",
        "central_frequency": 150.0 * 1e6,
        "bandwidth": 7.0 * 1e6,
        "source_type": "tv_antenna",
        "polarisation": "H",
        "height": 15.0,
        "longitude": 33.875,
        "latitude": 121.894722222222,
    }

    rfi_source = RFISource(**my_data)
    return rfi_source


@pytest.fixture(name="rfi_signal")
def rfi_signal_fixt():
    """RFISignal object"""
    my_data = {
        "station_id": np.array(["ska_low_station1", "ska_low_station2"]),
        "source_id": "test",
        "time": np.array(
            [
                "2021-04-11 01:20:55",
                "2021-04-11 01:30:55",
                "2021-04-11 01:40:55",
            ]
        ),
        "frequency": np.array([250.1 * 1e6, 250.2 * 1e6, 250.3 * 1e6]),
        "azimuth": np.array(
            [[12.0, 11.0] * 3]
        ),  # 3 for the three time-samples
        "elevation": np.array(
            [[0.5, 0.6] * 3]
        ),  # 3 for the three time-samples
        "distance": np.array(
            [[150.0 * 1e3, 160.0 * 1e3] * 3]
        ),  # 3 for the three time-samples
        "apparent_coordinates": np.array(
            [  # three time-samples, values for each are the same
                [[12.0, 0.5, 150.0 * 1e3], [11.0, 0.6, 160.0 * 1e3]],
                [[12.0, 0.5, 150.0 * 1e3], [11.0, 0.6, 160.0 * 1e3]],
                [[12.0, 0.5, 150.0 * 1e3], [11.0, 0.6, 160.0 * 1e3]],
            ]
        ),
        "apparent_power": np.array(
            [  # three time-samples, values for each are the same
                [[150.0, 145.0, 147.0], [151.0, 140.0, 144.0]],
                [[150.0, 145.0, 147.0], [151.0, 140.0, 144.0]],
                [[150.0, 145.0, 147.0], [151.0, 140.0, 144.0]],
            ]
        ),
    }

    rfi_signal = RFISignal(**my_data)
    return rfi_signal


@pytest.fixture(name="data_cube_per_source")
def data_cube_per_source_fxt(rfi_source, rfi_signal):
    """A DataCube for a single source"""
    my_data = {
        "rfi_source": rfi_source,
        "rfi_signal": rfi_signal,
    }

    data_cube = DataCubePerSource(**my_data)
    return data_cube
