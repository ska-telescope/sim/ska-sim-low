"""
Test data cube objects
"""

from copy import deepcopy
from dataclasses import fields
from unittest.mock import Mock, patch

import numpy as np
import pytest

from rfi.rfi_interface.rfi_data_cube import (
    BeamGainDataCube,
    DataCube,
    DataCubePerSource,
)


def test_rfi_source(rfi_source):
    """
    RFISource object is created with the correct values from input dict.
    :param rfi_source: RFISource fixture from conftest.py
    """
    assert rfi_source.id == "test"
    assert rfi_source.central_frequency == 150 * 1e6
    assert fields(rfi_source)[-1].name == "longitude"
    assert fields(rfi_source)[-1].metadata["unit"] == "degree"
    assert fields(rfi_source)[2].name == "central_frequency"
    assert fields(rfi_source)[2].metadata["unit"] == "Hz"


def test_rfi_signal(rfi_signal):
    """
    RFISignal object is created with the correct values from input dict.
    :param rfi_signal: RFISignal fixture from conftest.py
    """
    assert (
        np.column_stack(
            (
                rfi_signal.frequency,
                np.column_stack(rfi_signal.apparent_power[0, :]),
            )
        )
        == np.array(
            [
                [250.1 * 1e6, 150.0, 151.0],
                [250.2 * 1e6, 145.0, 140.0],
                [250.3 * 1e6, 147.0, 144.0],
            ]  # there are three time-samples
        )
    ).all()
    assert (
        rfi_signal.azimuth == np.array([[12.0, 11.0] * 3])
    ).all()  # there are three time-samples
    assert (
        rfi_signal.station_id
        == np.array(["ska_low_station1", "ska_low_station2"])
    ).all()


def test_data_cube_per_source(data_cube_per_source, rfi_signal):
    """
    DataCubePerSource object is created with the
    correct values from input dict.

    :param data_cube_per_source: DataCubePerSource fixture from conftest.py
    :param rfi_signal: RFISignal fixture from conftest.py
    :param source_at_ska: RFISourceAtSKA fixture from conftest.py
    """
    assert data_cube_per_source.rfi_source.id == "test"
    assert data_cube_per_source.rfi_signal == rfi_signal


def test_data_cube_per_source_wrong_source_id_in_signal(
    rfi_signal, rfi_source
):
    """
    The id of the RFISource object doesn't match the source id
    in any of the RFISignal objects within the DataCubePerSource object.

    The source id in rfi_source, source_at_ska and rfi_signal
    is "test" by default.

    :param source_at_ska: RFISourceAtSKA fixture from conftest.py
    :param rfi_signal: RFISignal fixture from conftest.py
    :param rfi_source: RFISource fixture from conftest.py
    """
    wrong_signal = deepcopy(rfi_signal)
    wrong_signal.source_id = "new_id"

    my_data = {
        "rfi_source": rfi_source,
        "rfi_signal": wrong_signal,
    }

    expected_message = "Mismatch between input data source ids. "

    with pytest.raises(ValueError) as e:
        DataCubePerSource(**my_data)

    assert expected_message in str(e.value)


def test_data_cube():
    """
    DataCube object is correctly initialized.
    :param data_cube_per_source: fixture from conftest.py
    """
    times = ["2020-01-02 00:00:00", "1987-09-01 16:00:00"]
    freqs = [120.0 * 1e6, 110.0 * 1e6, 154.0 * 1e6]
    stations = ["my-ska-station"]
    result = DataCube(times, freqs, stations)

    assert [t.decode() for t in result.time_samples.tolist()] == times
    assert result.frequency_channels.tolist() == freqs
    assert [s.decode() for s in result.station_id.tolist()] == stations
    # pylint: disable=protected-access
    assert result._number_of_sources == 0


def test_data_cube_append(data_cube_per_source):
    """
    DataCube object has two sources appended to it, which results in the
    correct shape of the rfi_signal, and apparent_source_coords arrays.

    :param data_cube_per_source: fixture from conftest.py
    """
    result = DataCube(
        data_cube_per_source.rfi_signal.time.tolist(),
        data_cube_per_source.rfi_signal.frequency,
        data_cube_per_source.rfi_signal.station_id.tolist(),
    )

    result.append_data(data_cube_per_source)

    data_cube_per_source.source_id = "second_source"
    result.append_data(data_cube_per_source)

    # pylint: disable=protected-access
    assert result._number_of_sources == 2
    assert result.signal.shape == (
        2,
        3,
        2,
        3,
    )  # Nsource x Ntimes x Nstations x Nfreqs
    assert result.coordinates.shape == (
        2,
        3,
        2,
        3,
    )  # Nsource x Ntimes x Nstations x N[ax, el, dist]


@pytest.mark.parametrize("source_id", [None, "", b""])
def test_data_cube_error_missing_source_id(source_id, data_cube_per_source):
    """
    Raise ValueError if the data being appended to
    DataCube does not contain a valid source_id.

    :param data_cube_per_source: fixtures from conftest.py
    """
    data_cube_per_source.rfi_source.id = source_id
    result = DataCube(
        data_cube_per_source.rfi_signal.time.tolist(),
        data_cube_per_source.rfi_signal.frequency,
        data_cube_per_source.rfi_signal.station_id.tolist(),
    )

    with pytest.raises(ValueError) as e:
        result.append_data(data_cube_per_source)

    assert "New data cube cannot have an empty source_id." in str(e.value)


def test_data_cube_error_wrong_times(data_cube_per_source):
    """
    Data appended to DataCube has to refer to the
    same time samples as the DataCube was
    initialized with; otherwise raise ValueError

    :param data_cube_per_source: fixtures from conftest.py
    """
    result = DataCube(
        ["1900-01-01 00:00:00"],
        data_cube_per_source.rfi_signal.frequency,
        data_cube_per_source.rfi_signal.station_id.tolist(),
    )

    with pytest.raises(ValueError) as e:
        result.append_data(data_cube_per_source)

    assert "The time_samples that initialized " in str(e.value)


def test_data_cube_error_wrong_frequency(data_cube_per_source):
    """
    Data appended to DataCube has to refer to the
    same frequency samples as the DataCube was
    initialized with; otherwise raise ValueError

    :param data_cube_per_source: fixtures from conftest.py
    """
    result = DataCube(
        data_cube_per_source.rfi_signal.time.tolist(),
        [1.0, 2.0, 3.0],
        data_cube_per_source.rfi_signal.station_id.tolist(),
    )

    with pytest.raises(ValueError) as e:
        result.append_data(data_cube_per_source)

    assert "The frequency_channels that initialized " in str(e.value)


def test_data_cube_error_wrong_station_id(data_cube_per_source):
    """
    Data appended to DataCube has to refer to the
    same station ids as the DataCube was
    initialized with; otherwise raise ValueError

    :param data_cube_per_source: fixtures from conftest.py
    """
    result = DataCube(
        data_cube_per_source.rfi_signal.time.tolist(),
        data_cube_per_source.rfi_signal.frequency,
        ["stat1", "stat2"],
    )

    with pytest.raises(ValueError) as e:
        result.append_data(data_cube_per_source)

    assert "The station ids that initialized " in str(e.value)


@pytest.fixture(name="beam_gain_cube")
def beam_gain_cube_fxt():
    """
    Fixture to create a BeamGainDataCube for tests.
    It also asserts that the class is correctly initialized.
    """
    ra = 0.0
    dec = -45.0
    obs_time = "2000-01-01 09:32:00.000"
    freq_chans = np.array([170.0, 172.0, 175.0])
    rfi_source_ids = np.array([b"S1", b"S2"])
    nstations = 5

    result = BeamGainDataCube(
        ra, dec, obs_time, freq_chans, rfi_source_ids, nstations
    )

    yield result

    assert (result.freq_channels == freq_chans).all()
    assert result.right_ascension == ra
    assert result.declination == dec
    assert (result.rfi_source_ids == rfi_source_ids).all()
    assert result.num_stations == nstations


def test_set_beam_gain_new_value(beam_gain_cube):
    """
    beam_gain is correctly updated.
    """
    assert beam_gain_cube.beam_gain is None

    beam_gain = np.ones((2, 5, 3, 4))
    beam_gain_cube.beam_gain = beam_gain

    assert (beam_gain_cube.beam_gain == beam_gain).all()


def test_set_beam_gain_new_value_not_array(beam_gain_cube):
    """
    TypeError is raised when the new value of beam_gain is not an np.ndarray
    """
    assert beam_gain_cube.beam_gain is None
    beam_gain = "wrong value"

    with pytest.raises(TypeError):
        beam_gain_cube.beam_gain = beam_gain


def test_set_beam_gain_new_value_wrong_dims(beam_gain_cube):
    """
    ValueError is raised when the dimensions of the new value cannot be derived
    from the dimensions of values that initialized the beam_gain_cube.

    In this case, the accepted dims are:
        2 (n_sources) x 5 (n_stations) x 3 (n_channels) x 4 (n_stokes)
    """
    assert beam_gain_cube.beam_gain is None
    beam_gain = np.zeros((1, 1, 1, 4))

    with pytest.raises(ValueError):
        beam_gain_cube.beam_gain = beam_gain


@patch("rfi.rfi_interface.rfi_data_cube._get_input", Mock(return_value="no"))
def test_set_beam_gain_overwrite_value_false(beam_gain_cube):
    """
    .beam_gain already has a value: Log warning, and abort overwriting.
    """
    # add value the first time
    beam_gain = np.ones((2, 5, 3, 4))
    beam_gain_cube.beam_gain = beam_gain
    assert beam_gain_cube.beam_gain is not None

    # try to overwrite value
    with patch("logging.Logger.warning") as mock_warn:
        with patch("logging.Logger.info") as mock_inf:
            beam_gain_cube.beam_gain = beam_gain

    mock_warn.assert_called_with(
        "Overwriting existing data in %s.beam_gain.", "BeamGainDataCube"
    )
    mock_inf.assert_called_with("Aborting")


@patch("rfi.rfi_interface.rfi_data_cube._get_input", Mock(return_value="yes"))
def test_set_beam_gain_overwrite_value_true(beam_gain_cube):
    """
    .beam_gain already has a value: Log warning, and continue overwriting.
    """
    # add value the first time
    beam_gain = np.ones((2, 5, 3, 4))
    beam_gain_cube.beam_gain = beam_gain
    assert beam_gain_cube.beam_gain is not None

    # try to overwrite value
    with patch("logging.Logger.warning") as mock_warn:
        with patch("logging.Logger.info") as mock_inf:
            beam_gain_cube.beam_gain = beam_gain * 5

    mock_warn.assert_called_with(
        "Overwriting existing data in %s.beam_gain.", "BeamGainDataCube"
    )
    mock_inf.assert_not_called()

    assert (beam_gain_cube.beam_gain == beam_gain * 5).all()
