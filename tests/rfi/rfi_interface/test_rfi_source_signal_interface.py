"""
Test RFI interface
"""

from unittest.mock import MagicMock, patch

import pytest

from rfi.rfi_interface.rfi_source_signal_interface import (
    FREQ_END,
    FREQ_KEYS,
    FREQ_RANGE,
    FREQ_START,
    N_CHANNELS,
    rfi_interface,
    validate_frequency,
)

BASE_PATH = "rfi.rfi_interface.rfi_source_signal_interface"


def test_validate_frequency():
    """
    When the input dictionary doesn't contain frequency-related information,
    the default frequency info is added to the input arguments dictionary.
    """
    input_args = {k: None for k in FREQ_KEYS}
    validate_frequency(input_args)

    assert input_args[f"--{N_CHANNELS}"] == "3"
    assert input_args[FREQ_RANGE] == ["170.5", "184.5"]


def test_initialize_data_cube_error_one_given():
    """
    If at least one but not all of FREQ_KEYS is provided,
    raise ValueError (all has to be provided, if at least one is).
    """
    input_args = {k: None for k in FREQ_KEYS}
    input_args[f"<{N_CHANNELS}>"] = 12

    with pytest.raises(ValueError):
        validate_frequency(input_args)


def test_initialize_data_cube_error_two_given():
    """
    If two but not all of FREQ_KEYS are provided,
    raise ValueError (all has to be provided, if at least one is).
    """
    input_args = {k: None for k in FREQ_KEYS}
    input_args[FREQ_START] = 120.0
    input_args[FREQ_END] = 155.0

    with pytest.raises(ValueError):
        validate_frequency(input_args)


def test_initialize_data_cube():
    """
    If all of FREQ_KEYS are provided, add their
    formatted version to the input_dictionary
    """
    input_args = {
        f"<{N_CHANNELS}>": 5,
        FREQ_START: 120.0,
        FREQ_END: 164.4,
    }

    validate_frequency(input_args)

    assert input_args[f"--{N_CHANNELS}"] == "5"
    assert input_args[FREQ_RANGE] == ["120.0", "164.4"]


def test_initialize_data_cube_remove_unnecessary_keys():
    """
    Function updates the input dictionary in place,
    removing all the keys in FREQ_KEYS, and replacing them
    with --frequency_range and --n_channels
    """
    input_args = {k: None for k in FREQ_KEYS}

    assert FREQ_START in input_args.keys()
    assert FREQ_END in input_args.keys()
    assert f"<{N_CHANNELS}>" in input_args.keys()
    assert FREQ_RANGE not in input_args.keys()
    assert f"--{N_CHANNELS}" not in input_args.keys()

    validate_frequency(input_args)

    assert FREQ_START not in input_args.keys()
    assert FREQ_END not in input_args.keys()
    assert f"<{N_CHANNELS}>" not in input_args.keys()
    assert FREQ_RANGE in input_args.keys()
    assert f"--{N_CHANNELS}" in input_args.keys()


@patch(f"{BASE_PATH}.rfi_attenuation")
@patch(f"{BASE_PATH}.cli_parser")
def test_rfi_interface_tv_antenna(mock_cli_parser, mock_rfi_atten):
    """
    Happy path for rfi_interface, with source_type = tv_antenna
    """
    source_type = "tv_antenna"
    input_args = {
        "--frequency_range": ["170.5", "184.5"],
        "--n_time_chunks": "1",
    }
    mock_cli_parser.return_value = MagicMock(n_time_chunks="1")
    mock_rfi_atten.return_value = MagicMock()

    expected_call_args = [
        "--n_time_chunks",
        "1",
        FREQ_RANGE,
        "170.5",
        "184.5",
    ]
    rfi_interface(source_type, **input_args)

    mock_cli_parser().parse_args.assert_called_once_with(expected_call_args)


def test_rfi_interface_wrong_type():
    """
    If called with a source_type that is not in ACCEPTED_SOURCES,
    raise ValueError
    """
    source_type = "wrong_type"

    with pytest.raises(ValueError):
        rfi_interface(source_type)
