# SKA Low Simulations

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sim-low/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-sim-low/en/latest/?badge=latest)

Collection of scripts used for various SKA-Low simulations. Please refer to the [documentation](https://developer.skatelescope.org/projects/ska-sim-low/en/latest) for more details.


## Contribute to this repository

[Black](https://github.com/psf/black), [isort](https://pycqa.github.io/isort/),
and various linting tools are used to keep the Python code in good shape.
Please check that your code follows the formatting rules before committing it
to the repository. You can apply Black and isort to the code with:

```bash
make python-format
```

and you can run the linting checks locally using:

```bash
make python-lint
```

The linting job in the CI pipeline does the same checks, and it will fail if
the code does not pass all of them. Note: we currently ignore pylint failures.
